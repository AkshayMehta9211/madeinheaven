package com.appicmobile.madeinheaven.ui.common;

public class EVENTDATA {
	
	private String  event_tittle;
	private String event_date;
	private String event_months;
	private String event_start_time;
	private String event_end_time;
	private String latitude;
	private String longitude;
	private String address;
	private String info;
	private String  event_year;
	private String  event_day;
	private String rsvp_option;

	public String getRsvp_option() {
		return rsvp_option;
	}

	public void setRsvp_option(String rsvp_option) {
		this.rsvp_option = rsvp_option;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;
	
	private String event_dmy;
	
	/**
	 * @return the event_dmy
	 */
	public String getEvent_dmy() {
		return event_dmy;
	}
	/**
	 * @param event_dmy the event_dmy to set
	 */
	public void setEvent_dmy(String event_dmy) {
		this.event_dmy = event_dmy;
	}
	/**
	 * @return the event_months
	 */
	public String getEvent_months() {
		return event_months;
	}
	/**
	 * @param event_months the event_months to set
	 */
	public void setEvent_months(String event_months) {
		this.event_months = event_months;
	}
	/**
	 * @return the event_day
	 */
	public String getEvent_day() {
		return event_day;
	}
	/**
	 * @param event_day the event_day to set
	 */
	public void setEvent_day(String event_day) {
		this.event_day = event_day;
	}
	/**
	 * @return the event_year
	 */
	public String getEvent_year() {
		return event_year;
	}
	/**
	 * @param event_year the event_year to set
	 */
	public void setEvent_year(String event_year) {
		this.event_year = event_year;
	}
	/**
	 * @return the event_tittle
	 */
	public String getEvent_tittle() {
		return event_tittle;
	}
	/**
	 * @param event_tittle the event_tittle to set
	 */
	public void setEvent_tittle(String event_tittle) {
		this.event_tittle = event_tittle;
	}
	/**
	 * @return the event_date
	 */
	public String getEvent_date() {
		return event_date;
	}
	/**
	 * @param event_date the event_date to set
	 */
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	/**
	 * @return the event_start_time
	 */
	public String getEvent_start_time() {
		return event_start_time;
	}
	/**
	 * @param event_start_time the event_start_time to set
	 */
	public void setEvent_start_time(String event_start_time) {
		this.event_start_time = event_start_time;
	}
	/**
	 * @return the event_end_time
	 */
	public String getEvent_end_time() {
		return event_end_time;
	}
	/**
	 * @param event_end_time the event_end_time to set
	 */
	public void setEvent_end_time(String event_end_time) {
		this.event_end_time = event_end_time;
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}
	/**
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}
}
