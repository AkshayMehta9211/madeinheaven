package com.appicmobile.madeinheaven.ui.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SERVICEDATA implements Serializable {

    private String postDatestring;

    public static class POST {
        public String commentatorName;
        public String comments_data;


        /**
         * @return the commentatorName
         */
        public String getCommentatorName() {
            return commentatorName;
        }

        /**
         * @param commentatorName the commentatorName to set
         */
        public void setCommentatorName(String commentatorName) {
            this.commentatorName = commentatorName;
        }

        /**
         * @return the comments_data
         */
        public String getComments_data() {
            return comments_data;
        }

        /**
         * @param comments_data the comments_data to set
         */
        public void setComments_data(String comments_data) {
            this.comments_data = comments_data;
        }
    }

    private int comments_count;
    private String post_id;
    private String created_time;
    private String likes;
    private String likes_count;
    private String images;
    private String images_thumbnail_url;
    private String weddingCode;
    private String users;
    private String users_name;
    private String users_profile_pics;
    private String images_standard_resolution;
    private String posttype;
    private String answer;
    private String videolink;

    public String getPostDatestring() {
        return postDatestring;
    }

    public void setPostDatestring(String postDatestring) {
        this.postDatestring = postDatestring;
    }

    
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    private String questionId;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    private String question;


    private List<String> postImage = new ArrayList<String>();

    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }


    /**
     * @return the postImage
     */
    public List<String> getPostImage() {
        return postImage;
    }

    /**
     * @param postImage the postImage to set
     */
    public void setPostImage(List<String> postImage) {
        this.postImage = postImage;
    }

    private String type;
    private List<POST> comments = new ArrayList<POST>();


    private List<POLLS> polls = new ArrayList<POLLS>();

    private List<String> image = new ArrayList<String>();
    private String users_id;
    private String users_fullname;

    public String getOptionTypeClicked() {
        return optionTypeClicked;
    }

    public void setOptionTypeClicked(String optionTypeClicked) {
        this.optionTypeClicked = optionTypeClicked;
    }

    public String optionTypeClicked;

    /**
     * @return the image
     */
    public List<String> getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(List<String> image) {
        this.image = image;
    }


    public List<POLLS> getPolls() {
        return polls;
    }

    public void setPolls(List<POLLS> polls) {
        this.polls = polls;
    }

    /**
     * @return the com
     */
    public List<POST> getComments() {
        return comments;
    }

    /**
     * @param comments the com to set
     */
    public void setComments(List<POST> comments) {
        this.comments = comments;
    }

    /**
     * @return the weddingCode
     */
    public String getWeddingCode() {
        return weddingCode;
    }

    /**
     * @param weddingCode the weddingCode to set
     */
    public void setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
    }

    /**
     * @return the images_standard_resolution
     */
    public String getImages_standard_resolution() {
        return images_standard_resolution;
    }

    /**
     * @param images_standard_resolution the images_standard_resolution to set
     */
    public void setImages_standard_resolution(String images_standard_resolution) {
        this.images_standard_resolution = images_standard_resolution;
    }

    /**
     * @return the post_id
     */
    public String getPost_id() {
        return post_id;
    }

    /**
     * @param post_id the post_id to set
     */
    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the comments_count
     */
    public int getComments_count() {
        return comments_count;
    }

    /**
     * @param comments_count the comments_count to set
     */
    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }


    /**
     * @return the created_time
     */
    public String getCreated_time() {
        return created_time;
    }

    /**
     * @param created_time the created_time to set
     */
    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    /**
     * @return the likes
     */
    public String getLikes() {
        return likes;
    }

    /**
     * @param likes the likes to set
     */
    public void setLikes(String likes) {
        this.likes = likes;
    }

    /**
     * @return the likes_count
     */
    public String getLikes_count() {
        return likes_count;
    }

    /**
     * @param likes_count the likes_count to set
     */
    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    /**
     * @return the images
     */
    public String getImages() {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(String images) {
        this.images = images;
    }

    /**
     * @return the images_thumbnail_url
     */
    public String getImages_thumbnail_url() {
        return images_thumbnail_url;
    }

    /**
     * @param images_thumbnail_url the images_thumbnail_url to set
     */
    public void setImages_thumbnail_url(String images_thumbnail_url) {
        this.images_thumbnail_url = images_thumbnail_url;
    }

    /**
     * @return the users
     */
    public String getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(String users) {
        this.users = users;
    }

    /**
     * @return the users_name
     */
    public String getUsers_name() {
        return users_name;
    }

    /**
     * @param users_name the users_name to set
     */
    public void setUsers_name(String users_name) {
        this.users_name = users_name;
    }

    /**
     * @return the users_profile_pics
     */
    public String getUsers_profile_pics() {
        return users_profile_pics;
    }

    /**
     * @param users_profile_pics the users_profile_pics to set
     */
    public void setUsers_profile_pics(String users_profile_pics) {
        this.users_profile_pics = users_profile_pics;
    }

    /**
     * @return the users_id
     */
    public String getUsers_id() {
        return users_id;
    }

    /**
     * @param users_id the users_id to set
     */
    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    /**
     * @return the users_fullname
     */
    public String getUsers_fullname() {
        return users_fullname;
    }

    /**
     * @param users_fullname the users_fullname to set
     */
    public void setUsers_fullname(String users_fullname) {
        this.users_fullname = users_fullname;
    }

    public void setVideolink(String videolink) {
        this.videolink = videolink;
    }

    public String getVideolink() {
        return videolink;
    }

    public static class POLLS {

        public String option1;
        public String optiontype;
        public String vote_a;


        public String getOption1() {
            return option1;
        }

        public void setOption1(String option1) {
            if(option1.length()>7){
                this.option1 = option1.substring(0, 7);
            }else{
                this.option1 = option1;
            }

        }

        public String getVote_a() {
            return vote_a;
        }

        public void setVote_a(String vote_a) {
            this.vote_a = vote_a;
        }

        public String getOptiontype() {
            return optiontype;
        }

        public void setOptiontype(String optiontype) {
            this.optiontype = optiontype;
        }


    }
}
