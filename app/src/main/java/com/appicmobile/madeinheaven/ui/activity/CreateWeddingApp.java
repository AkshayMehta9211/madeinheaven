package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.RegistrationDO;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.pojo.UserRegister;
import com.google.gson.Gson;

import org.json.JSONObject;

public class CreateWeddingApp extends Fragment {

    private static final String TAG = CreateWeddingApp.class.getSimpleName();
    Button login;
	TextView tvRegister, tvCancel;
	EditText etName, etEmail, etPassword, etConfirmPassword;
	String name, email, password;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootview = inflater.inflate(R.layout.create_wedding_app_layout, container, false);
		setUserVisibleHint(true);
		InitializeControls(rootview);
		return rootview;
	}

	private void InitializeControls(View rootview) {

		etName = (EditText) rootview.findViewById(R.id.etName);
		etEmail = (EditText) rootview.findViewById(R.id.etEmail);
		etPassword = (EditText) rootview.findViewById(R.id.etPassword);
		etConfirmPassword = (EditText) rootview.findViewById(R.id.etConfirmpassword);
		tvRegister = (TextView) rootview.findViewById(R.id.tvRegister);
		tvCancel = (TextView) rootview.findViewById(R.id.tvCancel);

		tvRegister.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				registerThroughServer();
			}
		});
		tvCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Fragment canclewedding = new Joinwdding();

				FragmentManager fragmentManager = getActivity()
						.getSupportFragmentManager();

				FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				fragmentTransaction.replace(R.id.main_container, canclewedding);
				fragmentTransaction.commit();

			}
		});
	}
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}

	protected void registerThroughServer() {
		name = etName.getText().toString().trim();
		email = etEmail.getText().toString().trim();
		password = etPassword.getText().toString().trim();

		if (name.isEmpty()) {
			Toast.makeText(getActivity(), "please Enter Name", Toast.LENGTH_SHORT).show();
		} else if (email.isEmpty()) {
			Toast.makeText(getActivity(), "please Enter email", Toast.LENGTH_SHORT).show();
		} else if (password.isEmpty()) {
			Toast.makeText(getActivity(), "please Enter Password", Toast.LENGTH_SHORT).show();
		} else {
			RegistrationDO registrationDO = new RegistrationDO();
			registrationDO.name = name;
			registrationDO.email_id = email;
			registrationDO.password = password;
			registrationDO.type = "create_wedding";

			final Gson gson = new Gson();
			final String jsonString = gson.toJson(registrationDO);
			Log.i("jsonString",jsonString);
			RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_REGISTER_USER, jsonString, true,
					new com.appicmobile.madeinheaven.ui.network.ResponseListener() {
						@Override
						public void onSuccess(String response) {
							try {
                                Log.e(TAG,response);
								UserRegister userRegister = gson.fromJson(response, UserRegister.class);
								if ("Wedding Created Successfully".equals(userRegister.getMsg())) {
									if (UserRegister.saveUser(getActivity(), userRegister)) {

										Toast.makeText(getActivity(), "Registration Successfull", Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getActivity(), "Failed to save data,Please try again",
												Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getActivity(), userRegister.getMsg() == null ? "Please try again"
											: userRegister.getMsg(), Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								System.out.println("e---->>" + e.toString());
								Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onError(VolleyError error) {
							// TODO Auto-generated method stub
                            if(error!=null){
                                if(error.networkResponse!=null){
                                    if(error.networkResponse.data!=null){
                                        Log.e(TAG,error.networkResponse.data.toString());
                                    }
                                }
                            }
							Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();

						}

						@Override
						public void onSuccess_one(JSONObject response) {
							// TODO Auto-generated method stub

						}
					});

		}
	}

}