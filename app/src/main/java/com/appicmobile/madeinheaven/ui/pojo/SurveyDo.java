package com.appicmobile.madeinheaven.ui.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 21-Dec-15.
 */
public class SurveyDo {


    public String getWeddingCode() {
        return weddingCode;
    }

    public void setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
    }

    public   String weddingCode;
    public  String question;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String userId;
    private List<OPTION> option = new ArrayList<OPTION>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;

    public List<OPTION> getOption() {
        return option;
    }

    public void setOption(List<OPTION> option) {
        this.option = option;
    }




    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }





    public static class OPTION {

        private String text;


        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }


    }
}
