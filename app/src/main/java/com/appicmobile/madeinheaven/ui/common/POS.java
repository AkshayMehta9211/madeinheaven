package com.appicmobile.madeinheaven.ui.common;

public class POS {
	public String pos;

	/**
	 * @return the pos
	 */
	public String getPos() {
		return pos;
	}

	/**
	 * @param pos the pos to set
	 */
	public void setPos(String pos) {
		this.pos = pos;
	}

}
