package com.appicmobile.madeinheaven.ui.pojo;

/**
 * Created by Ashvini on 09-Jan-16.
 */
public class CHATDO {

    public String weddingCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeddingCode() {
        return weddingCode;
    }

    public void setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
    }

    public String userId;
}
