package com.appicmobile.madeinheaven.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.NotificationAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.Notiarray;
import com.appicmobile.madeinheaven.ui.pojo.Notification;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    private TextView tvBackButton;
    private RecyclerView recyclerView;
    private ArrayList<Notiarray> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        loadNotificationFromServer();
        tvBackButton = (TextView) findViewById(R.id.tvBackButton);
        tvBackButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    private void loadNotificationFromServer() {

        JSONObject object = new JSONObject();
        try {
            object.put("weddingCode", String.valueOf(Appconstant.WEDDING_CODE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestGenerator.makePostRequest(NotificationActivity.this, MadeInHeavenServiceURL.URL_NOTIFICATION, object.toString(), true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        Gson gson = new Gson();
                        Notification notification = gson.fromJson(response, Notification.class);
                        if (notification != null) {
                            if (notification.getNotiarray().size() > 0) {
                                arrayList.addAll(notification.getNotiarray());
                                initDataSet(arrayList);
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                    }
                });
    }

    private void initDataSet(ArrayList<Notiarray> arrayList) {
        NotificationAdapter adapter = new NotificationAdapter(NotificationActivity.this, arrayList);
        recyclerView.setAdapter(adapter);
    }

}
