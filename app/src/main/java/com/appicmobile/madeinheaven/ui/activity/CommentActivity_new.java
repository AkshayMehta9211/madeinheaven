package com.appicmobile.madeinheaven.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.CommentsAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.CommentDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.utils.Utils;
import com.appicmobile.madeinheaven.ui.view.SendCommentButton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 28-Dec-15.
 */
public class CommentActivity_new extends Activity implements SendCommentButton.OnSendClickListener{

    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";

    SendCommentButton btnSendComment;
    LinearLayout contentRoot;
    RecyclerView rvComments;
    LinearLayout llAddComment;
    EditText etComment;
    ImageView ivBackButton;
    String comm;
    String value;
    List<SERVICEDATA.POST> questions = new ArrayList<SERVICEDATA.POST>();
    private ArrayList<String> arrayList;

    private CommentsAdapter commentsAdapter;
    private int drawingStartLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_activity_new);
        rvComments = (RecyclerView) findViewById(R.id.rvComments);
        btnSendComment = (SendCommentButton) findViewById(R.id.btnSendComment);
        contentRoot = (LinearLayout) findViewById(R.id.contentRoot);
        llAddComment = (LinearLayout) findViewById(R.id.llAddComment);
        etComment = (EditText) findViewById(R.id.etComment);
        ivBackButton = (ImageView) findViewById(R.id.ivBackButton);

        arrayList = new ArrayList<String>();


        value = getIntent().getStringExtra("PostId");
        questions = Appconstant.COMMENTS;

        setupComments();
        setupSendCommentButton();

        drawingStartLocation = getIntent().getIntExtra(ARG_DRAWING_START_LOCATION, 0);
        if (savedInstanceState == null) {
            contentRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    contentRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    startIntroAnimation();
                    return true;
                }
            });
        }
    }

    private void setupSendCommentButton() {
        btnSendComment.setOnSendClickListener(this);
    }

    private void setupComments() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setHasFixedSize(true);

        commentsAdapter = new CommentsAdapter(this, questions);
        rvComments.setAdapter(commentsAdapter);
        rvComments.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rvComments.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    commentsAdapter.setAnimationsLocked(true);
                }
            }
        });
    }

    private void startIntroAnimation() {
        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);
        llAddComment.setTranslationY(200);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
//                        ViewCompat.setElevation(getToolBar(), Utils.dpToPx(8));
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
        commentsAdapter.updateItems();
        llAddComment.animate().translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(200)
                .start();
    }


    @Override
    public void onSendClickListener(View v) {

        comm = etComment.getText().toString().trim();
        if (!comm.equalsIgnoreCase("")) {
            if (validateComment()) {
                commentsAdapter.addItem();
                commentsAdapter.setAnimationsLocked(false);
                commentsAdapter.setDelayEnterAnimation(false);
                if (commentsAdapter.getItemCount() > 0)
                    rvComments.smoothScrollBy(0, rvComments.getChildAt(0).getHeight() * commentsAdapter.getItemCount());
                etComment.setText(null);
                btnSendComment.setCurrentState(SendCommentButton.STATE_DONE);
            }
            uploadOnServer(comm);
        }
    }

    private void uploadOnServer(String com) {
        CommentDo comment = new CommentDo();
        comment.commentatorId = String.valueOf(Appconstant.USER_ID);
        comment.postId = value;
        comment.comment = com;
        final Gson gson = new Gson();
        String jsonString = gson.toJson(comment);
        RequestGenerator.makePostRequest(CommentActivity_new.this, MadeInHeavenServiceURL.URL_POST_COMMENTS, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            CommentDo comment = gson.fromJson(response, CommentDo.class);
                            if (comment.getStatus().contains("true")) {
                                Toast.makeText(CommentActivity_new.this, comment.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(CommentActivity_new.this, "please_try_again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        Toast.makeText(CommentActivity_new.this, "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub

                        Toast.makeText(CommentActivity_new.this, "successOne", Toast.LENGTH_SHORT).show();
                    }
                });
        SERVICEDATA.POST post = new SERVICEDATA.POST();
        post.setCommentatorName((Appconstant.USERNAME));
        post.setComments_data(comm);
        questions.add(post);
        commentsAdapter.notifyDataSetChanged();
        btnSendComment.setCurrentState(SendCommentButton.STATE_DONE);

    }

    private boolean validateComment() {
        if (TextUtils.isEmpty(etComment.getText().toString().trim())) {
            btnSendComment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        contentRoot.animate()
                .translationY(Utils.getScreenHeight(this))
                .setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        overridePendingTransition(0, 0);
                    finish();
                    }
                })
                .start();
    }

   /* @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton:
                contentRoot.animate()
                        .translationY(Utils.getScreenHeight(this))
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                overridePendingTransition(0, 0);
                                Intent i = new Intent(CommentActivity_new.this,NavDrawer.class);
                                startActivity(i);
                            }
                        })
                        .start();
                break;
        }
    }*/
}
