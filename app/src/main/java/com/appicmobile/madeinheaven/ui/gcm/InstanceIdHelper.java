package com.appicmobile.madeinheaven.ui.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by Ashvini on 8/4/2015.
 */
public class InstanceIdHelper {

    private final Context mContext;
    private PreferenceUtils preferenceManager;
    SharedPreferences spref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    public InstanceIdHelper(Context context) {
        mContext = context;
        preferenceManager=new PreferenceUtils(context);
    }

    /**
     * Register for GCM
     *
     * @param senderId the project id used by the app's server
     */
    public void getGcmTokenInBackground(final String senderId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    spref = mContext.getSharedPreferences(preferenceManager.PREF_NAME, Context.MODE_PRIVATE);
                    editor = spref.edit();
                    String token =InstanceID.getInstance(mContext).getToken(senderId,
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    editor.putString(Appconstant.GCM_TOKEN,token);
                    editor.commit();
                    //preferenceManager.saveStringInPreference(Appconstant.GCM_TOKEN, token);
                    // Save the token in the address book
                } catch (final IOException e) {
                   e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * Unregister by deleting the token
     *
     * @param senderId the project id used by the app's server
     */
    public void deleteGcmTokeInBackground(final String senderId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    InstanceID.getInstance(mContext).deleteToken(senderId,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                } catch (final IOException e) {

                }
                return null;
            }
        }.execute();
    }
}
