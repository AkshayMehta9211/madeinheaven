package com.appicmobile.madeinheaven.ui.network;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ankit on 9/11/2015.
 */
public class RequestGenerator {

    private static final int MY_SOCKET_TIMEOUT_MS = 72000;
    private static ProgressDialog progressDialog;
    private static int COUNT;

    public static void showProgressDialog(final Activity context) {
        try {
            cancelProgressDialog();
            context.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    COUNT++;
                }
            });
        }catch (Exception e){

        }
    }

    public static void cancelProgressDialog() {
        try{
            if (progressDialog != null) {
                if (COUNT <= 1) {
                    progressDialog.cancel();
                    progressDialog = null;
                }
                COUNT--;
            }
        }catch (Exception e){

        }
    }

    public static void makePostRequest(final Activity context, String url, String body, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }

            JSONObject jsonObject = new JSONObject(body);
            Log.i("jsonString", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,url,jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            responseListener.onSuccess(response.toString());
                            cancelProgressDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            responseListener.onError(error);
                            cancelProgressDialog();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                /*@Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }*/

            };
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void makeGetRequest(final Activity context, String url, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            responseListener.onSuccess_one(response);
                            cancelProgressDialog();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    responseListener.onError(error);
                    error.printStackTrace();
                    cancelProgressDialog();
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
