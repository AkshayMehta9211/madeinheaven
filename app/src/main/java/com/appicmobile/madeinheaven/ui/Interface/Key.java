package com.appicmobile.madeinheaven.ui.Interface;

public interface Key {
	public static final String TAG_ID = "id";
	public static final String TAG_META = "meta";
	public static final String TAG_DATA = "data";
	public  static final String TAG_TAG = "tags";
	public  static final String TAG_TYPE = "type";
	public  static final String TAG_LOCATIONS = "location";
	public  static final String TAG_COMMENT = "comments";
	public  static final String TAG_COMMENT_COUNT = "count";
	public  static final String TAG_COMMENT_DATA = "data";
	public  static final String TAG_FILTER = "filter";
	public  static final String TAG_LINK = "link";
	public  static final String TAG_LIKE = "likes";
	public  static final String TAG_LIKE_COUNT = "count";
	public  static final String TAG_LIKE_DATA = "data";
	public  static final String TAG_LIKE_DATA_USERNAME = "username";
	public  static final String TAG_LIKE_DATA_PROFILE_PICTURES = "profile_picture";
	public  static final String TAG_LIKE_DATA_ID = "id";
	public  static final String TAG_LIKE_DATA_FULL_NAME = "full_name";
	public  static final String TAG_IMAGES = "images";
	public  static final String TAG_IMAGES_LOW_RESOLUTION = "low_resolution";
	public  static final String TAG_IMAGES_THUMBNAIL = "thumbnail";
	public  static final String TAG_IMAGES_STANDARD_RESOLUTION = "standard_resolution";
	public  static final String TAG_IMAGES_STANDARD_RESOLUTION_URL = "url";
	public  static final String TAG_USERS_PHOTO = "users_in_photo";
	public  static final String TAG_CAPTIONS = "caption";
	public  static final String TAG_USER = "user";
	public  static final String TAG_CREATED_TIME = "created_time";
	public  static final String TAG_IMAGES_THUMBNAIL_URL = "url";
	public  static final String TAG_USER_NAME = "username";
	public  static final String TAG_USER_PROFILE_PICS = "profile_picture";
	public  static final String TAG_USER_FULL_NAME = "full_name";
}
