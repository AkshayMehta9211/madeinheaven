package com.appicmobile.madeinheaven.ui.multipleimages;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.PostActivity;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.utils.Utils;
import com.facebook.android.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * 多图选择 Created by Nereo on 2015/4/7.
 */
public class MultiImageSelectorActivity extends Fragment implements MultiImageSelectorFragment.Callback {

	/** 最大图片选择次数，int类型，默认9 */
	public static final String EXTRA_SELECT_COUNT = "max_select_count";
	/** 图片选择模�?，默认多选 */
	public static final String EXTRA_SELECT_MODE = "select_count_mode";
	/** 是�?�显示相机，默认显示 */
	public static final String EXTRA_SHOW_CAMERA = "show_camera";
	/** 选择结果，返回为 ArrayList&lt;String&gt; 图片路径集�?� */
	public static final String EXTRA_RESULT = "select_result";
	/** 默认选择集 */
	public static final String EXTRA_DEFAULT_SELECTED_LIST = "default_list";

	/** �?�选 */
	public static final int MODE_SINGLE = 0;
	/** 多选 */
	public static final int MODE_MULTI = 1;

	private ArrayList<String> resultList = new ArrayList<String>();

	ArrayList<ImageData> imageUrl = new ArrayList<>();
	private Button mSubmitButton, btn_back;
	private int mDefaultCount;
	Toolbar toolbar;
	TextView toolbar_title;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onResume() {
		super.onResume();
//		resultList.clear();

//		FragmentTransaction ft = getFragmentManager().beginTransaction();
//		ft.detach(this).attach(this).commit();
	}

	/*
         * @Override protected void onCreate(Bundle savedInstanceState) {
         * super.onCreate(savedInstanceState);
         * setContentView(R.layout.activity_default);
         */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootview = inflater.inflate(R.layout.activity_default, container, false);
		AppCompatActivity activity = (AppCompatActivity) getActivity();
		
		((NavDrawer)getActivity()).setActionBarTitle("CAMERA ROLL");
		/*toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
		toolbar_title = (TextView) rootview.findViewById(R.id.toolbar_title);
		toolbar_title.setText("Library");
		activity.getSupportActionBar().setHomeButtonEnabled(true);
		toolbar.setNavigationIcon(R.drawable.ic_drawer);

//		activity.setSupportActionBar(toolbar);
		activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

		// btn_back = (Button)rootview.findViewById(R.id.btn_back);

		Intent intent = getActivity().getIntent();
		mDefaultCount = intent.getIntExtra(EXTRA_SELECT_COUNT, 4);
		int mode = intent.getIntExtra(EXTRA_SELECT_MODE, MODE_MULTI);
		boolean isShow = intent.getBooleanExtra(EXTRA_SHOW_CAMERA, false);
		if (mode == MODE_MULTI && intent.hasExtra(EXTRA_DEFAULT_SELECTED_LIST)) {
			resultList = intent.getStringArrayListExtra(EXTRA_DEFAULT_SELECTED_LIST);
		}

		Bundle bundle = new Bundle();
		bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_COUNT, mDefaultCount);
		bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_MODE, mode);
		bundle.putBoolean(MultiImageSelectorFragment.EXTRA_SHOW_CAMERA, isShow);
		bundle.putStringArrayList(MultiImageSelectorFragment.EXTRA_DEFAULT_SELECTED_LIST, resultList);

		getActivity().getSupportFragmentManager().beginTransaction()
				.add(R.id.image_grid,
						Fragment.instantiate(getActivity(), MultiImageSelectorFragment.class.getName(), bundle))
				.commit();

		// 返回按钮
		/*
		 * btn_back.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View view) {
		 * getActivity().setResult(getActivity().RESULT_CANCELED);
		 * getActivity().finish(); } });
		 */

		// 完�?按钮
		mSubmitButton = (Button) rootview.findViewById(R.id.commit);
		if (resultList == null || resultList.size() <= 0) {
			mSubmitButton.setText("select Image");
			mSubmitButton.setEnabled(false);
		} else {
			mSubmitButton.setText("selected(" + resultList.size() + "/" + mDefaultCount + ")");
			mSubmitButton.setEnabled(true);
		}
		mSubmitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (resultList != null && resultList.size() > 0) {
					// 返回已选择的图片数�?�
					Intent data = new Intent();
					data.putStringArrayListExtra(EXTRA_RESULT, resultList);
					getActivity().setResult(getActivity().RESULT_OK, data);
					getActivity().finish();
				}
			}
		});

		return rootview;
	}

	private void setToolBar() {
		// set toolbar appearance

	}

	@Override
	public void onSingleImageSelected(String path) {
		Intent data = new Intent();
		resultList.add(path);
		data.putStringArrayListExtra(EXTRA_RESULT, resultList);
		getActivity().setResult(getActivity().RESULT_OK, data);
		getActivity().finish();
	}

	@Override
	public void onImageSelected(String path) {
		if (!resultList.contains(path)) {
			resultList.add(path);
		}
		// 有图片之�?�，改�?�按钮状�?
		if (resultList.size() > 0) {
			mSubmitButton.setText("selected(" + resultList.size() + "/" + mDefaultCount + ")");
			if (!mSubmitButton.isEnabled()) {
				mSubmitButton.setEnabled(true);
			}

//			ArrayList<ImageData> imageUrl = new ArrayList<>();
			String url = null;
			if (resultList != null && resultList.size() > 0) {
				for (int i = 0; i < resultList.size(); i++) {
					url = resultList.get(i).toString();
					ImageData imageData = new ImageData();
					imageData.setImagePath(url);
					imageUrl.add(imageData);
				}
			}

//			Intent i = new Intent(getActivity(), PostActivity.class);
			Appconstant.image = imageUrl;
		}
	}

	@Override
	public void onImageUnselected(String path) {
		if (resultList.contains(path)) {
			resultList.remove(path);
			mSubmitButton.setText("selected(" + resultList.size() + "/" + mDefaultCount + ")");
			ArrayList<ImageData> imageUrl = new ArrayList<>();
			String url = null;
			if (resultList != null && resultList.size() > 0) {
				for (int i = 0; i < resultList.size(); i++) {
					url = resultList.get(i).toString();
					ImageData imageData = new ImageData();
					imageData.setImagePath(url);
					imageUrl.add(imageData);
				}
			}

//			Intent i = new Intent(getActivity(), PostActivity.class);
//			Appconstant.image = imageUrl;
//			getActivity().startActivity(i);

		} else {
			mSubmitButton.setText("selected(" + resultList.size() + "/" + mDefaultCount + ")");
		}
		// 当为选择图片时候的状�?
		if (resultList.size() == 0) {
			mSubmitButton.setText("No image selected");
			mSubmitButton.setEnabled(false);
		}
	}

	@Override
	public void onCameraShot(File imageFile) {
		// TODO Auto-generated method stub

	}

	/*
	 * @Override public void onCameraShot(File imageFile) { if(imageFile !=
	 * null) { Intent data = new Intent();
	 * resultList.add(imageFile.getAbsolutePath());
	 * data.putStringArrayListExtra(EXTRA_RESULT, resultList);
	 * setResult(RESULT_OK, data); finish(); } }
	 */

	public static Fragment newInstance() {
		MultiImageSelectorActivity activeFragment = new MultiImageSelectorActivity();
		Bundle bundle = new Bundle();
		activeFragment.setArguments(bundle);
		return activeFragment;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.library_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			/*case R.id.:
				// Not implemented here
				return false;*/
			case R.id.action_add:
                if(Appconstant.image!=null) {
                    Intent i = new Intent(getActivity(), PostActivity.class);
                    getActivity().startActivity(i);
                }else{
                    Utils.showToast(getActivity(),getString(R.string.please_select_images));
                }

				return true;
			default:
				break;
		}

		return false;
	}




}
