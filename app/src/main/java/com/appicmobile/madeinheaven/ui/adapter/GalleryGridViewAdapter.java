package com.appicmobile.madeinheaven.ui.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.USERPOSTDO;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class GalleryGridViewAdapter extends BaseAdapter {


    private Context context;
    private int[] imageid;
    ViewHolderItem viewHolder;
    FragmentActivity activity;
    USERPOSTDO result;

    ArrayList<USERPOSTDO> allPostData;

    public GalleryGridViewAdapter(FragmentActivity activity, ArrayList<USERPOSTDO> allPostData) {
        this.activity = activity;
        this.allPostData = allPostData;
       // inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {

        if (allPostData != null && allPostData.size() > 0)
            return allPostData.size();
        return 0;
    }

    public Object getItem(int position) {
        return allPostData.size();
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        result = allPostData.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.grid_cell, null);
            viewHolder = new ViewHolderItem();
            viewHolder.img = (ImageView) convertView.findViewById(R.id.grid_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        String url = result.getPostImg();
        if(!TextUtils.isEmpty(result.getType())){
            if (result.getType().equalsIgnoreCase("Image")) {
                Glide.with(activity).load(url).placeholder(R.drawable.loadingimage).centerCrop().into(viewHolder.img);
            } else {
                Glide.with(activity).load(R.drawable.play_image).placeholder(R.drawable.play_image).centerCrop().into(viewHolder.img);
            }
        }


        return convertView;
    }

    public class ViewHolderItem {
        ImageView img;
    }

}
