package com.appicmobile.madeinheaven.ui.common;

public class EventList {
	public long userId;
	public String weddingCode;
	public String status;
	
	
	
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the weddingCode
	 */
	public String getWeddingCode() {
		return weddingCode;
	}
	/**
	 * @param weddingCode the weddingCode to set
	 */
	public void setWeddingCode(String weddingCode) {
		this.weddingCode = weddingCode;
	}
}
