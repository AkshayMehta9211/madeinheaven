package com.appicmobile.madeinheaven.ui.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.events.AddressActivity;
import com.appicmobile.madeinheaven.ui.events.InfoActivity;
import com.appicmobile.madeinheaven.ui.events.MapActivity;
import com.appicmobile.madeinheaven.ui.rsvpevent.RSVPActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class EventActivity extends Fragment implements OnClickListener{

	private TextView mapdetail,addressdetail,infodetail,tvEventTitle ,tvWeek,tvMonth,tvDate,tvYear,addToCalender,tvEventTime;
	private Button marriage,gallery,homepage,event,chat;
	Context context;
	private LinearLayout llAddToCalender;
	String EventLatitude;
	String EventLongitude;
	boolean isPressed = true;
	String dateInString = null;
	Date date = null;
	Calendar c ;
	 String EventDate;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}


	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootview=inflater.inflate(R.layout.eventlayout,container,false);
		 String EventTitle = getArguments().getString("EventTitle");  
		 String EventAddress = getArguments().getString("EventAddress");  
		 String EventInfo = getArguments().getString("EventInfo"); 
		 String EventLatitude = getArguments().getString("Lattitude");  
		 String EventLongitude = getArguments().getString("Longitude");
		 EventDate = getArguments().getString("Event_dmy");
		 
		
		/* SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateInString = Appconstant.EVENT_START_TIME;
			Date date = null;
			try {
				date = formatter.parse(dateInString);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

		/*	c = Calendar.getInstance();
			c.setTime(date);*/
		 

		 Appconstant.EVENT_TITLE = EventTitle;
		 Appconstant.EVENT_ADDRESS = EventAddress;
		 Appconstant.EVENT_INFO = EventInfo;
		 
		 
//		 Toast.makeText(getActivity(), EventTitle +":"+EventAddress +":"+EventInfo, Toast.LENGTH_SHORT).show();
		
		
		mapdetail=(TextView)rootview.findViewById(R.id.tvMap);
		addressdetail=(TextView)rootview.findViewById(R.id.tvAddress);
		infodetail=(TextView)rootview.findViewById(R.id.tvInfo);
		tvEventTitle = (TextView)rootview.findViewById(R.id.tvEventTitle);
		marriage=(Button)rootview.findViewById(R.id.ibMarriage);
        gallery=(Button)rootview.findViewById(R.id.ibGallery);
        homepage=(Button)rootview.findViewById(R.id.ibHome);
        event=(Button)rootview.findViewById(R.id.ibEvent);
        chat=(Button)rootview.findViewById(R.id.ibChat);
        addToCalender = (TextView)rootview.findViewById(R.id.tvAdd_to_cal);
		tvEventTime = (TextView)rootview.findViewById(R.id.tvEventTime);
		llAddToCalender = (LinearLayout)rootview.findViewById(R.id.llAddToCalender);

        
        tvWeek = (TextView)rootview.findViewById(R.id.tvWeek);
        tvMonth=(TextView)rootview.findViewById(R.id.tvMonth);
        tvDate= (TextView)rootview.findViewById(R.id.tvDate);
        tvYear = (TextView)rootview.findViewById(R.id.tvYear);
        
        
        tvWeek.setText(Appconstant.EVENT_DAY);
        tvMonth.setText(Appconstant.EVENT_MONTHS_NAME);
        tvDate.setText(Appconstant.EVENT_DATE);
        tvYear.setText(Appconstant.EVENT_YEAR);
		tvEventTime.setText(Appconstant.EVENT_TIME_START +" to "+Appconstant.EVENT_TIME_END);

        mapdetail.setTextColor(getResources().getColor(R.color.purple_dark));
        addressdetail.setTextColor(Color.parseColor("#CCCCCC"));
        infodetail.setTextColor(Color.parseColor("#CCCCCC"));

        if(EventTitle !=null)
        	tvEventTitle.setText(EventTitle);
        else{
    	tvEventTitle.setText(Appconstant.EVENT_TITLE);
        }
		
		displayView1(0, mapdetail);
		setOnClick();
		
		return rootview;
	}
	
	 /* public static Calendar defaultCalendar() {
          Calendar currentDate = Calendar.getInstance();
          currentDate.set(currentDate.get(Integer.parseInt(Appconstant.EVENT_YEAR)),Integer.parseInt(Appconstant.EVENT_MONTHS),Integer.parseInt(Appconstant.EVENT_DAY));
          return currentDate;
      }*/
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}
	
	private void setOnClick() {
		mapdetail.setOnClickListener(this);
		addressdetail.setOnClickListener(this);
		infodetail.setOnClickListener(this);
		marriage.setOnClickListener(this);
		gallery.setOnClickListener(this);
		homepage.setOnClickListener(this);
		event.setOnClickListener(this);
		chat.setOnClickListener(this);
		llAddToCalender.setOnClickListener(this);
	}

	public static Fragment newInstance() {
		EventActivity activeFragment = new EventActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
        case R.id.tvMap:
            displayView1(0, mapdetail);
            mapdetail.setTextColor(getResources().getColor(R.color.purple_dark));
            addressdetail.setTextColor(Color.parseColor("#CCCCCC"));
            infodetail.setTextColor(Color.parseColor("#CCCCCC"));
            break;
        case R.id.tvAddress:
            addressdetail.setTextColor(getResources().getColor(R.color.purple_dark));
            mapdetail.setTextColor(Color.parseColor("#CCCCCC"));
            infodetail.setTextColor(Color.parseColor("#CCCCCC"));
            displayView1(1, addressdetail);
            break;
        case R.id.tvInfo:
            infodetail.setTextColor(getResources().getColor(R.color.purple_dark));
            mapdetail.setTextColor(Color.parseColor("#CCCCCC"));
            addressdetail.setTextColor(Color.parseColor("#CCCCCC"));
            displayView1(2, infodetail);
            break;
            
        case R.id.ibMarriage:
            displayView(0, marriage);
            marriage.setBackgroundResource(R.color.Dark);
			event.setBackgroundResource(R.color.lightDark);
            break;
        case R.id.ibGallery:
        	gallery.setBackgroundResource(R.color.Dark);
            displayView(1, gallery);
            break;
        case R.id.ibHome:
        	homepage.setBackgroundResource(R.color.Dark);
            displayView(2, homepage);
            break;    
        case R.id.ibEvent:
        	event.setBackgroundResource(R.color.Dark);
            displayView(3, event);
            break;
        case R.id.ibChat:
        	chat.setBackgroundResource(R.color.Dark);
            displayView(4, chat);
            break;
            
        case R.id.llAddToCalender:
        	Calendar cal = Calendar.getInstance();   
        	
        	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
 			String dateInString = EventDate;
 			Date date = null;
 			try {
 				date = formatter.parse(dateInString);
 			} catch (ParseException e) {
 				e.printStackTrace();
 			} catch (java.text.ParseException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
 			cal.setTime(date);
        	
        	Intent intent = new Intent(Intent.ACTION_EDIT);
        	intent.setType("vnd.android.cursor.item/event");
        	intent.putExtra("beginTime",cal.getTimeInMillis());
        	intent.putExtra("allDay", true);
        	intent.putExtra("rrule", "FREQ=YEARLY");
        	intent.putExtra("endTime",  cal.getTimeInMillis()+60*60*1000);
        	intent.putExtra("title", Appconstant.EVENT_TITLE);
        	
        	startActivity(intent);
        	break;
            
    }
		
	}

	private void displayView1(int index, TextView addressdetail2) {
        Fragment fragment = null;
        switch (index) {
            case 0:
            	 Bundle args = new Bundle();
            	 args.putString("Latitude", EventLatitude);
            	 args.putString("Longitude", EventLongitude);
                fragment = MapActivity.newInstance();
                event.setBackgroundResource(R.color.Dark);
                break;
            case 1:
                fragment = AddressActivity.newInstance();
                break;
            case 2:
                fragment = InfoActivity.newInstance();
                break;
            
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_Event_fragment, fragment, fragment.getClass().getSimpleName()).commit();
//        if (MapActivity.newInstance() != null && !MapActivity.newInstance().isDetached()) {
//            FragmentTransaction ft2 = getFragmentManager()
//                    .beginTransaction();
//            ft2.detach(MapActivity.newInstance());
//            ft2.commit();
//        }
    }

	@SuppressWarnings("deprecation")
	private void setActiveTab(TextView activeTab, TextView inactiveTab1,TextView inactiveTab2) {
		// TODO Auto-generated method stub
		
//		activeTab.setBackgroundDrawable(context.getDrawable(R.drawable.textview_pressed));
//        inactiveTab1.setBackgroundDrawable(context.getDrawable(R.drawable.bottomtab_press));
//        inactiveTab2.setBackgroundDrawable(context.getDrawable(R.drawable.bottomtab_press));
		
	}

	private void displayView(int index, Button marriage2) {
        Fragment fragment1 = null;
        switch (index) {
            case 0:
                fragment1 = NavDrawer.newInstance();
               
                if (isPressed) {
    				marriage.setBackgroundResource(R.color.Dark);
    				gallery.setBackgroundResource(R.color.lightDark);
    				event.setBackgroundResource(R.color.lightDark);
    				chat.setBackgroundResource(R.color.lightDark);
    				isPressed = !isPressed;
    			} else {
    				marriage.setBackgroundResource(R.color.Dark);
    			}

//    			isPressed = !isPressed; // reverse

                break;
            case 1:
                fragment1 =GalleryActivity.newInstance();
    			if (isPressed) {
    				gallery.setBackgroundResource(R.color.Dark);
    				marriage.setBackgroundResource(R.color.lightDark);
    				event.setBackgroundResource(R.color.lightDark);
    				chat.setBackgroundResource(R.color.lightDark);
    				isPressed = !isPressed;
    			} else {
    				gallery.setBackgroundResource(R.color.Dark);
    			}

//    			isPressed = !isPressed; // reverse
                break;
            case 2:
                fragment1 = UploadActivity.newInstance();
                if (isPressed) {
    				gallery.setBackgroundResource(R.color.lightDark);
    				marriage.setBackgroundResource(R.color.lightDark);
    				event.setBackgroundResource(R.color.lightDark);
    				chat.setBackgroundResource(R.color.lightDark);
    				isPressed = !isPressed;
    			} else {
    				homepage.setBackgroundResource(R.color.Dark);
    			}

    			isPressed = !isPressed; // reverse
                break;
            case 3:
                fragment1 = RSVPActivity.newInstance();
//                event.setBackgroundResource(R.color.Dark);
                if (isPressed) {
    				event.setBackgroundResource(R.color.Dark);
    				gallery.setBackgroundResource(R.color.lightDark);
    				marriage.setBackgroundResource(R.color.lightDark);
    				chat.setBackgroundResource(R.color.lightDark);
    				isPressed = !isPressed;
    			} else {
    				event.setBackgroundResource(R.color.Dark);
    			}

    			isPressed = !isPressed; // reverse
                break;
            case 4:
                fragment1 = ChatActivity.newInstance();
                if (isPressed) {
    				chat.setBackgroundResource(R.color.Dark);
    				event.setBackgroundResource(R.color.lightDark);
    				gallery.setBackgroundResource(R.color.lightDark);
    				marriage.setBackgroundResource(R.color.lightDark);
    				isPressed = !isPressed;
    			} else {
    				chat.setBackgroundResource(R.color.Dark);
    			}

    			isPressed = !isPressed; // reverse
                break;
            
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        ft.replace(R.id.main_fragment_container, fragment1, fragment1.getClass().getName()).commit();
        
    }

	
	@Override
	public void onResume() {
		super.onResume();
		
		
		
	}
	@SuppressWarnings("deprecation")
	private void setActiveTab(Button activeTab, Button inactiveTab1, Button inactive2,Button inactive3,Button inactive4 ) {
		// TODO Auto-generated method stub
		
		
//		 activeTab.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.bottomtab_color));
//	        inactiveTab1.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.bottomtab_press));
//	        inactive2.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.bottomtab_press));
//	        inactive3.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.bottomtab_press));
//	        inactive4.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.bottomtab_press));
	    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.gallery_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.gallery_notification:
				return true;
			default:
				break;
		}

		return false;
	}




}