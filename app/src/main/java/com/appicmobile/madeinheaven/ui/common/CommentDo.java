package com.appicmobile.madeinheaven.ui.common;

public class CommentDo {

	public String postId;
	public String commentatorId;
	public String comment;
	public String status;
	public String msg;
	/**
	 * @return the postId
	 */
	public String getPostId() {
		return postId;
	}
	/**
	 * @param postId the postId to set
	 */
	public void setPostId(String postId) {
		this.postId = postId;
	}
	/**
	 * @return the commentatorId
	 */
	public String getCommentatorId() {
		return commentatorId;
	}
	/**
	 * @param commentatorId the commentatorId to set
	 */
	public void setCommentatorId(String commentatorId) {
		this.commentatorId = commentatorId;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
