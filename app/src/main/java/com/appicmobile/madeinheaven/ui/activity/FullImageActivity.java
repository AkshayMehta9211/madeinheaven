package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.FullScreenImageAdapter;
import com.appicmobile.madeinheaven.ui.utils.ExtendedViewPager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ashvini on 08-Jan-16.
 */
public class FullImageActivity extends Activity{

    private ExtendedViewPager mViewPager;
    private FullScreenImageAdapter mAdapter;
    private ArrayList<String> imageList= new ArrayList<String>();
    private int position=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);

        ImageView ivBack = (ImageView)findViewById(R.id.ivBackButton);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mViewPager = (ExtendedViewPager) findViewById(R.id.pager);
        Intent i = getIntent();

        if(i.getExtras().containsKey("ImageUrl")){
            position = 0;
            imageList.add(i.getExtras().getString("ImageUrl"));
        }else{
            position = i.getIntExtra("position", 0);
            imageList = i.getStringArrayListExtra("images");
        }


        Log.e("imageList",imageList.size()+"");
        mAdapter = new FullScreenImageAdapter(FullImageActivity.this,imageList);

        mViewPager.setAdapter(mAdapter);

        // displaying selected image first
        mViewPager.setCurrentItem(position);


    }
}
