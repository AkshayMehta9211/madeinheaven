package com.appicmobile.madeinheaven.ui.uploadTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.SurveyDo;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class SurveyActivity extends Fragment implements View.OnClickListener {

    private static final String TAG = SurveyActivity.class.getSimpleName();
    EditText etOption1, etOption2, etOption3, etOption4, etQuestion;
    LinearLayout llAdd2, llAdd3, llAddMore;

    String opt1, opt2, opt3, opt4, ques;

    static int count = 0;

    ArrayList<String> allQues = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.gallery_filter_layout, container, false);
        ((NavDrawer) getActivity()).setActionBarTitle("POLLS");

        etOption1 = (EditText) rootview.findViewById(R.id.etOption1);
        etOption2 = (EditText) rootview.findViewById(R.id.etOption2);
        etOption3 = (EditText) rootview.findViewById(R.id.etOption3);
        etOption4 = (EditText) rootview.findViewById(R.id.etOption4);
        etQuestion = (EditText) rootview.findViewById(R.id.etQuestion);
        llAdd2 = (LinearLayout) rootview.findViewById(R.id.llAdd2);
        llAdd3 = (LinearLayout) rootview.findViewById(R.id.llAdd3);
        llAddMore = (LinearLayout) rootview.findViewById(R.id.llAddMore);
        llAddMore.setOnClickListener(this);

        llAddMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                count++;
                if (count == 1)
                    llAdd2.setVisibility(v.VISIBLE);
                if (count == 2) {
                    llAdd2.setVisibility(v.VISIBLE);
                    llAdd3.setVisibility(v.VISIBLE);
                    llAddMore.setVisibility(v.GONE);
                    count = 0;
                }

            }
        });
        return rootview;

    }

    public static Fragment newInstance() {
        SurveyActivity activeFragment = new SurveyActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.polls_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.:
                // Not implemented here
				return false;*/
            case R.id.action_polls:

                ArrayList<SurveyDo.OPTION> list = new ArrayList<SurveyDo.OPTION>();


                ques = etQuestion.getText().toString().trim();
                opt1 = etOption1.getText().toString().trim();
                opt2 = etOption2.getText().toString().trim();
                opt3 = etOption3.getText().toString().trim();
                opt4 = etOption4.getText().toString().trim();

                if (ques.isEmpty() && opt1.isEmpty() && opt2.isEmpty())
                    Toast.makeText(getActivity(), "Please enter Minimum two options", Toast.LENGTH_SHORT).show();
                else {
                    for (int i = 0; i < 4; i++) {
                        SurveyDo.OPTION option = new SurveyDo.OPTION();
                        if (i == 0)
                            option.setText(opt1);
                        if (i == 1)
                            option.setText(opt2);
                        if (i == 2)
                            option.setText(opt3);
                        if (i == 3)
                            option.setText(opt4);

                        list.add(option);
                    }

                    SurveyDo add_poll = new SurveyDo();
                    add_poll.setWeddingCode(Appconstant.WEDDING_CODE);
                    add_poll.setQuestion(ques);
                    add_poll.setOption(list);
                    add_poll.setUserId(String.valueOf(Appconstant.USER_ID));

                    final Gson gson = new Gson();
                    final String jsonString = gson.toJson(add_poll);
                    Log.i("survey", jsonString);

                    RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_POST_POLLS, jsonString,
                            true, new ResponseListener() {
                                @Override
                                public void onSuccess(String response) {
                                    try {
                                        Log.d(TAG,response);
                                        SurveyDo res = gson.fromJson(response, SurveyDo.class);
                                        if (res.getStatus().contains("true")) {
                                            Toast.makeText(getActivity(), "uploaded successfull", Toast.LENGTH_SHORT).show();
                                            Intent home = new Intent(getActivity(), NavDrawer.class);
                                            startActivity(home);
                                        } else {
                                            Toast.makeText(getActivity(), "uploaded not successfull", Toast.LENGTH_SHORT)
                                                    .show();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Intent home = new Intent(getActivity(), NavDrawer.class);
                                        startActivity(home);
                                       // Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(VolleyError error) {
                                    Intent home = new Intent(getActivity(), NavDrawer.class);
                                    startActivity(home);
                                    // TODO Auto-generated method stub
                                   // Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onSuccess_one(JSONObject response) {
                                    // TODO Auto-generated method stub
                                  //  Toast.makeText(getActivity(), "try_again", Toast.LENGTH_SHORT).show();
                                }
                            });
                }

                return true;
            default:
                break;
        }

        return false;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llAddMore:
                count++;
                if (count == 1)
                    llAdd2.setVisibility(v.VISIBLE);
                if (count == 2) {
                    llAdd2.setVisibility(v.VISIBLE);
                    llAdd3.setVisibility(v.VISIBLE);
                    llAddMore.setVisibility(v.GONE);
                    count = 0;
                }
                break;
        }
    }
}
