package com.appicmobile.madeinheaven.ui.common;

import java.util.List;


public class serverDo {
	
	private String userID;
	private List<Likes> likes;
	private List<Data> like_data;

	// get and set

	public class Likes {
		private String count;
		private String data;
		/**
		 * @return the count
		 */
		public String getCount() {
			return count;
		}
		/**
		 * @param count the count to set
		 */
		public void setCount(String count) {
			this.count = count;
		}
		/**
		 * @return the data
		 */
		public String getData() {
			return data;
		}
		/**
		 * @param data the data to set
		 */
		public void setData(String data) {
			this.data = data;
		}
	}

	public class Data {
		private String username;
		private String profilePic;
		private String postId;
		private String postImg;
		private String postDate;
		private String type;
		/**
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}
		/**
		 * @param username the username to set
		 */
		public void setUsername(String username) {
			this.username = username;
		}
		/**
		 * @return the profilePic
		 */
		public String getProfilePic() {
			return profilePic;
		}
		/**
		 * @param profilePic the profilePic to set
		 */
		public void setProfilePic(String profilePic) {
			this.profilePic = profilePic;
		}
		/**
		 * @return the postId
		 */
		public String getPostId() {
			return postId;
		}
		/**
		 * @param postId the postId to set
		 */
		public void setPostId(String postId) {
			this.postId = postId;
		}
		/**
		 * @return the postImg
		 */
		public String getPostImg() {
			return postImg;
		}
		/**
		 * @param postImg the postImg to set
		 */
		public void setPostImg(String postImg) {
			this.postImg = postImg;
		}
		/**
		 * @return the postDate
		 */
		public String getPostDate() {
			return postDate;
		}
		/**
		 * @param postDate the postDate to set
		 */
		public void setPostDate(String postDate) {
			this.postDate = postDate;
		}
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}
		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}
		
	}
	
	

	

	
}
