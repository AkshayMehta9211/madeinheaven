package com.appicmobile.madeinheaven.ui.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashwini on 2/8/2016.
 */
public class Message {

    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("stringtodate ")
    @Expose
    private int stringtodate;
    @SerializedName("receiver")
    @Expose
    private Receiver receiver;

    /**
     *
     * @return
     * The senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     *
     * @param senderId
     * The sender_id
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     *
     * @return
     * The receiverId
     */
    public String getReceiverId() {
        return receiverId;
    }

    /**
     *
     * @param receiverId
     * The receiver_id
     */
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The stringtodate
     */
    public int getStringtodate() {
        return stringtodate;
    }

    /**
     *
     * @param stringtodate
     * The stringtodate
     */
    public void setStringtodate(int stringtodate) {
        this.stringtodate = stringtodate;
    }

    /**
     *
     * @return
     * The receiver
     */
    public Receiver getReceiver() {
        return receiver;
    }

    /**
     *
     * @param receiver
     * The receiver
     */
    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

}