package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.LoginDo;
import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.android.Facebook;
import com.facebook.model.GraphUser;

import java.util.ArrayList;
import java.util.List;

public class fb extends Activity {
	private String App_ID ="534750026676472";
	
	 private Session.StatusCallback statusCallback;
	
	 private SharedPreferences mPrefs;
	 private Facebook facebook ;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		facebook = new Facebook(App_ID);	
//		loginWithFacebook();
	}
	protected void loginWithFacebook() {
		statusCallback = new SessionStatusCallback();
		Session session = Session.getActiveSession();
		
		if(session == null)
//			session = new Session(this);
		session = Session.openActiveSessionFromCache(this);
    
		if (session!=null && !session.isOpened() && !session.isClosed()) 
		{
			OpenRequest op = new Session.OpenRequest(this);
			op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
			op.setCallback(statusCallback);
			op.setPermissions(getPermissions());
			session = new Builder(this).build();
			Session.setActiveSession(session);
			session.openForRead(op);
//			getFacebookUserInfo();

		} else {
			
			Session.openActiveSession(this, true, statusCallback);
		}

	}
	
	public List<String> getPermissions()
	{
		List<String> PERMISSIONS = new ArrayList<String>();
		//	    	PERMISSIONS.add("publish_stream");
		//	    	PERMISSIONS.add("publish_actions");
		PERMISSIONS.add("email");
		PERMISSIONS.add("public_profile");
		PERMISSIONS.add("user_friends");
		return PERMISSIONS;
	}
	
	

	public class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
//			showLoader("Logging User through Facebook");
			
			if(session!=null && session.isOpened())
			{
				getFacebookUserInfo();
				
			}
			else
			{
				if(Appconstant.WEDDING_CODE !=null)
				{
					
				}
				else
				{
					
					
				}
//				onConnectedToFacebook();
//				getFacebookUserInfo();
			}
		}

	}
	
//	protected void onConnectedToFacebook(){
//		Intent i = new Intent(this,NavDrawer.class);
//		startActivity(i);
//	};

	private void getFacebookUserInfo() 
	{
		if(Session.getActiveSession()!=null && Session.getActiveSession().isOpened()){
			
			
			Request request = Request.newMeRequest(Session.getActiveSession(), new GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {

					if(Appconstant.DEBUG)
					{
						Log.i("User","Id : "+user.getId());
						Log.i("User","UserName : "+user.getName());
						Log.i("User","FirstName : "+user.getFirstName());
						Log.i("User","LastName : "+user.getLastName());
						Log.i("User","Email : "+user.getProperty("email"));
						Log.i("User","Birthday : "+user.getBirthday());
						Log.i("User","Gender : "+user.getProperty("gender"));
					}
					
					 String userId = user.getId();
					
					Appconstant.USER_ID = Integer.parseInt(userId);
					final String userName = user.getName();
					final String firstName = user.getFirstName();
					final String lastName = user.getLastName();
					final String userProfilePicUrl = "http://graph.facebook.com/"+ user.getId()+ "/picture?type=large";
					final String emailId =  user.getProperty("email") != null ? user.getProperty("email").toString(): "NA";
					final String genderString = user.getProperty("gender") != null ? user.getProperty("gender").toString(): "NA";
					final int gender = !genderString.equalsIgnoreCase("male") ?(!genderString.equalsIgnoreCase("female") ? -1 : 0): 1;
					
//					preference.saveStringInPreference(PreferenceUtils.USER_NAME, userName);
//					preference.saveStringInPreference(PreferenceUtils.FIRST_NAME, firstName);
//					preference.saveStringInPreference(PreferenceUtils.LAST_NAME, lastName);
//					preference.saveStringInPreference(PreferenceUtils.EMAIL_ID, emailId);
//					preference.saveStringInPreference(PreferenceUtils.PROFILE_PIC_URL, userProfilePicUrl);
//					preference.saveIntInPreference(PreferenceUtils.GENDER, gender);
//					preference.commitPreference();
					
					LoginDo userDO = new LoginDo();
					userDO.UserId = userId;
					userDO.FirstName = firstName;
					userDO.LastName = lastName;
					userDO.EmailId = emailId;
					userDO.Gender = gender;
					userDO.ImgUrl = userProfilePicUrl;
					userDO.SocialId = user.getId();
					
					
					
//					validateLogin2Server();
				}
			});
		}
	}
}
 
 
 
  
