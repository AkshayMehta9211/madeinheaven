package com.appicmobile.madeinheaven.ui.common;

public class USERPOSTDO {
	
	public String postId;
	public String userName;
	public String type;
	public String postData;
	public String postImg;
	public String postDate;


	/**
	 * @return the postId
	 */
	public String getPostId() {
		return postId;
	}
	/**
	 * @param postId the postId to set
	 */
	public void setPostId(String postId) {
		this.postId = postId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the postDate
	 */
	public String getPostData() {
		return postData;
	}
	/**
	 * @param postDate the postDate to set
	 */
	public void setPostDate(String postData) {
		this.postData = postData;
	}
	/**
	 * @return the postImg
	 */
	public String getPostImg() {
		return postImg;
	}
	/**
	 * @param postImg the postImg to set
	 */
	public void setPostImg(String postImg) {
		this.postImg = postImg;
	}
}
