package com.appicmobile.madeinheaven.ui.uploadTab;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.PostActivity;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PhotosActivity extends Fragment
        implements SurfaceHolder.Callback, Camera.ShutterCallback, Camera.PictureCallback, View.OnClickListener {

    Button cancel;
    ImageView takepic;
    Camera mCamera = null;
    SurfaceView mPreview;
    Context context;

    public boolean clickOn = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ArrayList<ImageData> imageUrl = new ArrayList<ImageData>();
    List<Camera.Size> mSupportedPreviewSizes = null;
    SharedPreferences sPrefs;
    SharedPreferences.Editor sEdit;
    Camera.Size finalSize;

    public static final int IMAGE_SELECTION_LIMIT = 5;
    public static final int INTENT_REQUEST_GET_IMAGES = 2123;
    private int currentCameraId;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.photocapture, container, false);
        releaseCameraAndPreview();

        ((NavDrawer) getActivity()).setActionBarTitle("PHOTO");

//		getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        takepic = (ImageView) rootview.findViewById(R.id.btnTakePic);
        mPreview = (SurfaceView) rootview.findViewById(R.id.svSurfaceView);
        mPreview.getHolder().addCallback(this);
        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
        mCamera = Camera.open();
        View switchCamera = rootview.findViewById(R.id.ibSwitchCamera);
        if(Camera.getNumberOfCameras() == 1){
            switchCamera.setVisibility(View.GONE);
        }else {
            switchCamera.setOnClickListener(this);
        }

        Camera.Parameters params = mCamera.getParameters();
        // Check what resolutions are supported by your camera
        List<Camera.Size> sizes = params.getSupportedPictureSizes();


        // Iterate through all available resolutions and choose one.
        // The chosen resolution will be stored in mSize.
        Camera.Size mSize;
        for (Camera.Size size : sizes) {
           // Log.i("resolution", "Available resolution: " + size.width + " " + size.height);
//		    if (wantToUseThisResolution(size)) {
//		        mSize = size;

           // Toast.makeText(getActivity(), "Size" + size, Toast.LENGTH_SHORT).show();
            break;
//		    }
        }

//		Log.i("Resolution", "Chosen resolution: "+mSize.width+" "+mSize.height);
        //params.setPictureSize(640, 640);
        //	params.setPreviewSize(640,480);

        //New Code***********



		/*mSupportedPreviewSizes=mCamera.getParameters().getSupportedPreviewSizes();
        if (mSupportedPreviewSizes != null) {
			 finalSize = getOptimalPreviewSize(mSupportedPreviewSizes, 640, 640);

		*//*	//Camera.Size selected = sizes.get(0);
            params.setPreviewSize(finalSize.width, finalSize.height);
			mCamera.setParameters(params);
			mCamera.setDisplayOrientation(90);
			mCamera.startPreview();*//*


			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewSize(finalSize.width, finalSize.height);
			mCamera.setParameters(parameters);
			mCamera.setDisplayOrientation(90);
			mCamera.startPreview();
		}*/

//***************
        takepic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onsnap();
            }
        });
        return rootview;
    }


    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCamera.stopPreview();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCamera.release();
       // Log.d("CAMERA", "Destroy");
    }

    public void onCancelClick(View v) {
        // finish();
    }

    private void onsnap() {
        // TODO Auto-generated method stub
        mCamera.takePicture(this, null, null, this);
    }

    @Override
    public void onShutter() {
      //  Toast.makeText(getActivity(), "Click!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        // change 1: Fixing the file name at the begining.
        long cT = System.currentTimeMillis();
        String fileName = String.format("%d.jpg", cT);

        // Major Change: Use this file copying mechanism.
        FileOutputStream fop = null;
        File file = null;
        try {

            file = new File(getActivity().getFilesDir(), fileName);
            fop = new FileOutputStream(file);


//			Intent i = new Intent(getActivity(),PostActivity.class);

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = data;

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            ExifInterface exif = new ExifInterface(file.getPath());
            exif.setAttribute(ExifInterface.TAG_ORIENTATION,String.valueOf(ExifInterface.ORIENTATION_ROTATE_180));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Picasso.with(getActivity()).load("file://"+file.getAbsolutePath()).into(imgPreview);
        // Step 1: Converting File to byte[]
		/*Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); // bm is the bitmap
		byte[] b = baos.toByteArray();
		// Step 2 : converting byte[] to Base64 encoded String
		String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);*/

        sPrefs = getActivity().getSharedPreferences("ImageData", Context.MODE_PRIVATE);
        sEdit = sPrefs.edit();


        ArrayList<String> myList = new ArrayList<String>();
        myList.add(file.toString());

        ImageData imageData = new ImageData();
        imageData.setImagePath(file.toString());
        if(currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
            imageData.setRotation(90);
        }else {
            imageData.setRotation(270);
            imageData.setFlip(true);
        }
        imageUrl.add(imageData);


        Appconstant.image = imageUrl;

        Intent i = new Intent(getActivity(), PostActivity.class);
        startActivity(i);
    }

	/*public static Bitmap rotateImage(Bitmap source, float angle) {
		Bitmap retVal;

		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

		return retVal;
	}*/
		

	/*	UPLOADIMAGE.Image image = new UPLOADIMAGE.Image();
		image.setFileextension(".jpg");
		image.setPath(encodedImage);

		ArrayList<UPLOADIMAGE.Image> base64ImageList = new ArrayList<UPLOADIMAGE.Image>();
		base64ImageList.add(image);

		UPLOADIMAGE uploadimage = new UPLOADIMAGE();
		uploadimage.setUserId(Appconstant.LOGIN_USER_ID);
		uploadimage.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));
		uploadimage.setImage(base64ImageList);

		Toast.makeText(getActivity(), "Picture Saved", 2000).show();

		final Gson gson = new Gson();
		final String jsonString = gson.toJson(uploadimage);
		Log.i("uploadimage", jsonString);

		RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_POST_IMAGE, jsonString, true,
				new ResponseListener() {
					@Override
					public void onSuccess(String response) {
						try {
							UPLOADIMAGE prescription1 = gson.fromJson(response, UPLOADIMAGE.class);

							if (prescription1.getStatus() == true) {

								Toast.makeText(getActivity(), "uploaded successfull", Toast.LENGTH_SHORT).show();

							}

					else {
								Toast.makeText(getActivity(), "uploaded not successfull", Toast.LENGTH_SHORT).show();
								// onBackPressed();
							}

						} catch (Exception e) {
							System.out.println("e---->>" + e.toString());
							Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onError(VolleyError error) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onSuccess_one(JSONObject response) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "JSONObject response_try_again", Toast.LENGTH_SHORT).show();
					}
				});
	}*/

    @SuppressWarnings("deprecation")
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		/*	Camera.Parameters params = mCamera.getParameters();
		List<Camera.Size> sizes = params.getSupportedPreviewSizes();
		Camera.Size selected = sizes.get(0);
		params.setPreviewSize(selected.width, selected.height);
		mCamera.setParameters(params);

		mCamera.setDisplayOrientation(90);
		mCamera.startPreview();*/
        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        if (mSupportedPreviewSizes != null) {
            finalSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);

            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewSize(finalSize.width, finalSize.height);
//            parameters.set("orientation", "portrait");
//            parameters.setRotation(90);
            mCamera.setParameters(parameters);
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i("PREVIEW", "surfaceDestroyed");
    }

    public static Fragment newInstance() {
        PhotosActivity activeFragment = new PhotosActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.blank, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibSwitchCamera:
                switchCamera();
                break;
        }
    }

    private void switchCamera() {

//        if (inPreview) {
        mCamera.stopPreview();
//        }
//NB: if you don't release the current camera before switching, you app will crash
        mCamera.release();

//swap the id of the camera to be used

        if(currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        else {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        mCamera = Camera.open(currentCameraId);

        // setCameraDisplayOrientation(getActivity(), currentCameraId, mCamera);
        try {

            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }
}
