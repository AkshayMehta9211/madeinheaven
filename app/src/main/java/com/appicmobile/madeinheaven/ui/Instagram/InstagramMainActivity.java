package com.appicmobile.madeinheaven.ui.Instagram;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.ui.InstagramSupport.Instagram;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramRequest;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramSession;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramUser;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.InstagramDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.UserRegister;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Instagram authentication.
 * 
 * @author Lorensius W. L. T
 *
 */
public class InstagramMainActivity extends Activity {
    private static final String TAG = InstagramMainActivity.class.getSimpleName();
    private InstagramSession mInstagramSession;
	private Instagram mInstagram;

	private ProgressBar mLoadingPb;
	private GridView mGridView;
	private PreferenceUtils preference;
	private static final String CLIENT_ID = "09fce637a1cd40239cd9ed79856fc7fa";
	private static final String CLIENT_SECRET = "252691b554684be8848c170fd52581b0";
	private static final String REDIRECT_URI = "http://davaaii.com/made_in_heaven1.1";
	private static String ACCESS_TOKEN = null;
	int PRIVATE_MODE = 0;
	SharedPreferences spref;

	// Editor for Shared preferences
	SharedPreferences.Editor editor;
	private static final String PREF_NAME = "LOGIN_DATA";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	// User name (make variable public to access from outside)
	public static final String LOGIN_USER_ID = "userId";

	// Email address (make variable public to access from outside)
	public static final String LOGIN_WEDDING_CODE = "wedding";

	public static final String LOGIN_HASHTAG = "hashtag";
	public static final String LOGIN_PROFILE_PICS = "profilepics";
	public static final String LOGIN_USERNAME = "username";

	SharedPreferences pref;
	SharedPreferences.Editor edior;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mInstagram = new Instagram(this, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);
		
		spref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		edior = spref.edit();
		preference=new PreferenceUtils(InstagramMainActivity.this);
		mInstagramSession = mInstagram.getSession();

		if (mInstagramSession.isActive()) {

			registerToTheServer();

			} else {
			mInstagram.authorize(mAuthListener);
		}
	}
	private void registerToTheServer() {

		if (!Appconstant.WEDDING_CODE.equals("")) {
			InstagramDo login = new InstagramDo();
			login.weddingCode = String.valueOf(Appconstant.WEDDING_CODE);
			login.token = preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");
			login.loginType = "instagram";
			login.userName = mInstagramSession.getUser().fullName;
			login.profilePic = mInstagramSession.getUser().profilPicture;
			// InstagramDo user = new InstagramDo();
			login.type = "user";
			login.deviceType="android";
			login.deviceToken=preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");
			final Gson gson = new Gson();
			String jsonString = gson.toJson(login);
			RequestGenerator.makePostRequest(this, MadeInHeavenServiceURL.URL_REGISTER_INSTGRAM, jsonString, true,
					new ResponseListener() {
						@Override
						public void onSuccess(String response) {
							try {
                                Log.e(TAG,response);
                                UserRegister userLogin1 = gson.fromJson(response, UserRegister.class);
								if ("successoosss".equals(userLogin1.getStatus())) {
									edior.putInt(LOGIN_USER_ID, userLogin1.getId());
									edior.putString(LOGIN_WEDDING_CODE, userLogin1.getWeddingCode());
									edior.putString(LOGIN_HASHTAG, userLogin1.getHashtag());
									edior.putString(LOGIN_PROFILE_PICS, userLogin1.getImageUrl());
									edior.putString(LOGIN_USERNAME, userLogin1.getUserName());
									edior.commit();
									Appconstant.USER_ID = userLogin1.getId();
									Appconstant.WEDDING_CODE = userLogin1.getWeddingCode();
									Appconstant.HASHTAG = userLogin1.getHashtag();
									Appconstant.PROFILE_PICS = userLogin1.getImageUrl();
									Appconstant.USERNAME = userLogin1.getUserName();
									Toast.makeText(InstagramMainActivity.this, userLogin1.getMsg(), Toast.LENGTH_SHORT)
											.show();
									startDrawerActivity();

								} else {
									Toast.makeText(InstagramMainActivity.this, userLogin1.getMsg(), Toast.LENGTH_SHORT)
											.show();
								}
							} catch (Exception e) {
								Toast.makeText(InstagramMainActivity.this, "please_try_again", Toast.LENGTH_SHORT)
										.show();
								e.printStackTrace();
							}
						}

						private void startDrawerActivity() {
							Intent i = new Intent(InstagramMainActivity.this, NavDrawer.class);
							startActivity(i);
						}

						@Override
						public void onError(VolleyError error) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onSuccess_one(JSONObject response) {
							// TODO Auto-generated method stub

						}

					});

		} else {
			InstagramDo login = new InstagramDo();
			login.emailId = mInstagramSession.getAccessToken();
			login.deviceType="android";
			login.deviceToken=preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");
			final Gson gson = new Gson();
			String jsonString = gson.toJson(login);
			RequestGenerator.makePostRequest(this, MadeInHeavenServiceURL.URL_LOGIN_USER_2, jsonString, true,
					new ResponseListener() {
						@Override
						public void onSuccess(String response) {
							try {
								UserRegister userLogin1 = gson.fromJson(response, UserRegister.class);
								if ("false".equals(userLogin1.getStatus())) {

									new AlertDialog.Builder(InstagramMainActivity.this)
											.setMessage("Please Join or Create a weeding")
											.setPositiveButton(android.R.string.yes,
													new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											
										}
									})

							.setIcon(android.R.drawable.ic_dialog_alert).show();

								} else {
									edior.putInt(LOGIN_USER_ID, userLogin1.getId());
									edior.putString(LOGIN_WEDDING_CODE,userLogin1.getWeddingCode());
									edior.putString(LOGIN_HASHTAG, userLogin1.getHashtag());
									edior.putString(LOGIN_PROFILE_PICS,userLogin1.getImageUrl());
									edior.putString(LOGIN_USERNAME,userLogin1.getUserName());
									edior.commit();
									Appconstant.USER_ID = userLogin1.getId();
									Appconstant.WEDDING_CODE = userLogin1.getWeddingCode();
									Appconstant.HASHTAG = userLogin1.getHashtag();
									Appconstant.PROFILE_PICS = userLogin1.getImageUrl();
									Appconstant.USERNAME = userLogin1.getUserName();
									Toast.makeText(InstagramMainActivity.this, userLogin1.getMsg(), Toast.LENGTH_SHORT)
											.show();
									startDrawerActivity();
									Toast.makeText(InstagramMainActivity.this, userLogin1.getMsg(), Toast.LENGTH_SHORT)
											.show();
								}
							} catch (Exception e) {
								Toast.makeText(InstagramMainActivity.this, "please_try_again", Toast.LENGTH_SHORT)
										.show();
								e.printStackTrace();
							}
						}

						private void startDrawerActivity() {
							Intent i = new Intent(InstagramMainActivity.this, NavDrawer.class);
							startActivity(i);
						}

						@Override
						public void onError(VolleyError error) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onSuccess_one(JSONObject response) {
							// TODO Auto-generated method stub

						}

					});
		}

	}

	private void showToast(String text) {
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
	}

	private Instagram.InstagramAuthListener mAuthListener = new Instagram.InstagramAuthListener() {
		@Override
		public void onSuccess(InstagramUser user) {
			finish();

			startActivity(new Intent(InstagramMainActivity.this, InstagramMainActivity.class));
		}

		@Override
		public void onError(String error) {
			showToast(error);
		}

		@Override
		public void onCancel() {
			//showToast("OK. Maybe later?");
			finish();

		}
	};

	public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	public class DownloadTask extends AsyncTask<URL, Integer, Long> {
		ArrayList<String> photoList;

		protected void onCancelled() {

		}

		protected void onPreExecute() {

		}

		protected Long doInBackground(URL... urls) {
			long result = 0;
			try {
				List<NameValuePair> params = new ArrayList<NameValuePair>(1);

				params.add(new BasicNameValuePair("count", "10"));

				ACCESS_TOKEN = mInstagramSession.getAccessToken();

				InstagramMainActivity.this.runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(InstagramMainActivity.this, "ACCESS_TOKEN = " + ACCESS_TOKEN, Toast.LENGTH_SHORT)
								.show();
					}
				});

				InstagramRequest request = new InstagramRequest(mInstagramSession.getAccessToken());
				String response = request.createRequest("GET", "/users/self/feed", params);

				if (!response.equals("")) {
					JSONObject jsonObj = (JSONObject) new JSONTokener(response).nextValue();
					JSONArray jsonData = jsonObj.getJSONArray("data");

					int length = jsonData.length();

					if (length > 0) {
						photoList = new ArrayList<String>();
						for (int i = 0; i < length; i++) {
							JSONObject jsonPhoto = jsonData.getJSONObject(i).getJSONObject("images")
									.getJSONObject("low_resolution");
							photoList.add(jsonPhoto.getString("url"));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		protected void onProgressUpdate(Integer... progress) {
		}

		protected void onPostExecute(Long result) {
			try {
				// mLoadingPb.setVisibility(View.GONE);

				if (photoList == null) {
					// Toast.makeText(getApplicationContext(), "No Photos
					// Available", Toast.LENGTH_LONG).show();
				} else {
					DisplayMetrics dm = new DisplayMetrics();

					getWindowManager().getDefaultDisplay().getMetrics(dm);

					int width = (int) Math.ceil((double) dm.widthPixels / 2);
					width = width - 50;
					int height = width;

					PhotoListAdapter adapter = new PhotoListAdapter(InstagramMainActivity.this);

					adapter.setData(photoList);
					adapter.setLayoutParam(width, height);

					mGridView.setAdapter(adapter);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}