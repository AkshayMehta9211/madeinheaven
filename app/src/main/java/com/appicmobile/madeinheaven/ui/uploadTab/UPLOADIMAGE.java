package com.appicmobile.madeinheaven.ui.uploadTab;

import java.util.ArrayList;
import java.util.List;


public class UPLOADIMAGE {
	
	private long userId;
	private Boolean status;
	private String weddingCode;
	private List<Image> image = new ArrayList<Image>();
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the weddingCode
	 */
	public String getWeddingCode() {
		return weddingCode;
	}
	/**
	 * @param weddingCode the weddingCode to set
	 */
	public void setWeddingCode(String weddingCode) {
		this.weddingCode = weddingCode;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
		 /**
	 * @return the status
	 */
	public boolean getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
		/**
	 * @return the userId
	 */
	
		/**
	 * @return the images
	 */
	public List<Image> getImage() {
		return image;
	}

	public void setImage(List<Image> image) {
		this.image = image;
	}
		public static class Image {

		        private String fileextension;
		        private String path;


		        /**
				 * @return the path
				 */
				public String getPath() {
					return path;
				}

				/**
				 * @param path the path to set
				 */
				public void setPath(String path) {
					this.path = path;
				}

				/**
				 * @return the fileextension
				 */
				public String getFileextension() {
					return fileextension;
				}

				/**
				 * @param fileextension the fileextension to set
				 */
				public void setFileextension(String fileextension) {
					this.fileextension = fileextension;
				}

	}

}
