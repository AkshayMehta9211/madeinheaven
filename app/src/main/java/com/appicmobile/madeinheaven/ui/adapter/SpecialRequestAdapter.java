package com.appicmobile.madeinheaven.ui.adapter;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.pojo.Images;

import java.io.File;
import java.util.ArrayList;

import nl.changer.polypicker.utils.ImageInternalFetcher;

/**
 * Created by Ashvini on 8/31/2015.
 */
public class SpecialRequestAdapter extends PagerAdapter {
    private Activity activity;
    private ArrayList<Images> imageList;
    private LayoutInflater inflater;
    ImageView media_image;
    EditText editText;

    public SpecialRequestAdapter(Activity activity, ArrayList<Images> imageList) {
        this.activity = activity;
        this.imageList = imageList;

    }


    @Override
    public int getCount() {
        return imageList.size();
    }

    public View instantiateItem(ViewGroup container, final int position) {
        final Images images = imageList.get(position);
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.media_layout, null);
        media_image = (ImageView) convertView.findViewById(R.id.media_image);
        editText = (EditText) convertView.findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                images.setContentValue(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editText.setText(images.getContentValue()==null?"":images.getContentValue());
        ImageInternalFetcher imageFetcher = new ImageInternalFetcher(activity, 500);
        Uri uri = Uri.parse(images.getBase64Value());
        if (!uri.toString().contains("content://")) {
            uri = Uri.fromFile(new File(uri.toString()));
        }
        imageFetcher.loadImage(uri, media_image);
        ((ViewPager) container).addView(convertView);
        return convertView;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void finishUpdate(View arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Object instantiateItem(View arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Parcelable saveState() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void startUpdate(View arg0) {
		// TODO Auto-generated method stub
		
	}

}