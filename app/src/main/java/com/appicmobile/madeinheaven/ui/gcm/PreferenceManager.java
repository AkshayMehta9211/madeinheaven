package com.appicmobile.madeinheaven.ui.gcm;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ashwini on 2/9/2016.
 */
public class PreferenceManager {


    private final String MY_PREFS_NAME = "myPreference";

    private Context context;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    public PreferenceManager(Context context) {
        this.context = context;
        this.editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        this.prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
    }

    public void putPreferenceBoolValues(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putPreferenceValues(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void putPreferenceIntValues(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public boolean getPreferenceBoolValues(String key) {
        return prefs.getBoolean(key, false);
    }

    public int getPreferenceIntValues(String key) {
        return prefs.getInt(key, 0);
    }

    public String getPreferenceValues(String key) {
        return prefs.getString(key, "");
    }

    public void clearSharedPreferance() {
        prefs.edit().clear().commit();
    }

    public void putPreferencDoubleValues(String key, double value) {
        editor.putLong(key, Double.doubleToRawLongBits(value));
        editor.commit();
    }

    public double getPreferencDoubleValues(String key, double defaultValue) {
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }
}