package com.appicmobile.madeinheaven.ui.pojo;

/**
 * Created by Ashvini on 30-Dec-15.
 */
public class ReportErrorDo {

    public String status;
    private String weddingCode;



    public int userId  ;
    public String postId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWeddingCode() {
        return weddingCode;
    }

    public void setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


}
