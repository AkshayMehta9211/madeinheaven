package com.appicmobile.madeinheaven.ui.adapter;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.util.StringUtil;
import com.appicmobile.madeinheaven.ui.activity.CommentActivity_new;
import com.appicmobile.madeinheaven.ui.activity.FullImageActivity;
import com.appicmobile.madeinheaven.ui.activity.VideoViewActivity;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.LikesDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.POS;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.network.ShareImageAsynctask;
import com.appicmobile.madeinheaven.ui.pojo.ReportErrorDo;
import com.appicmobile.madeinheaven.ui.pojo.VoteDO;
import com.appicmobile.madeinheaven.ui.utils.DateUtil;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenu;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;
import com.bumptech.glide.Glide;
import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.Line;
import com.facebook.android.Util;
import com.facebook.internal.Utility;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Home_cell_Adapter extends BaseAdapter {

    private final SwipeRefreshLayout.OnRefreshListener onRefreshListener;
    Context context;
    private Activity act;
    private ArrayList<SERVICEDATA> allData;
    List<POS> data;
    boolean isPressed;
    //
    HashMap<String, String> map = new HashMap<String, String>();
    HashMap<String, String> umap = new HashMap<String, String>();
    private ArrayList<String> likeData;
    HashMap<Integer, Boolean> but = new HashMap<Integer, Boolean>();

    ArrayList<SERVICEDATA.POLLS> val = new ArrayList<SERVICEDATA.POLLS>();


    private FeedAdapter.OnFeedItemClickListener onFeedItemClickListener;


    private final Map<RecyclerView.ViewHolder, AnimatorSet> likeAnimations = new HashMap<>();
    ImageLoader imageLoader;

    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    int count = 0;
    static int updateCount = 0;

    File file = null;
    private ProgressDialog progressDialog;
    private static int COUNT;

    public Home_cell_Adapter(Activity act, ArrayList<SERVICEDATA> allData, SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        this.act = act;
        this.allData = allData;
        this.onRefreshListener = onRefreshListener;
    }

    @Override
    public int getCount() {
        if (allData != null && allData.size() >= 0)
            return allData.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;
        convertView = null;

        final SERVICEDATA result = allData.get(position);

        if (convertView == null) {
            viewHolder = new ViewHolderItem();
            if (result.getPosttype().equalsIgnoreCase("Image"))
                convertView = LayoutInflater.from(act).inflate(R.layout.activity_home_cell, null);
            else if (result.getPosttype().equalsIgnoreCase("video"))
                convertView = LayoutInflater.from(act).inflate(R.layout.activity_home_cell, null);
            else
                convertView = LayoutInflater.from(act).inflate(R.layout.list_item_barchart, null);

            viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
            viewHolder.tvComments = (ImageView) convertView.findViewById(R.id.tvComments);
//            viewHolder.tvShare = (TextView) convertView.findViewById(R.id.tvShare);
            viewHolder.tvUploadTime = (TextView) convertView.findViewById(R.id.tvUploadTime);
            viewHolder.ivUserPics = (ImageView) convertView.findViewById(R.id.ivUserPics);
            viewHolder.ivImages = (ImageView) convertView.findViewById(R.id.ivImages);

            viewHolder.tvViewComments = (TextView) convertView.findViewById(R.id.tvViewComments);
            viewHolder.tvCommentsData = (TextView) convertView.findViewById(R.id.tvCommentsData);
            viewHolder.chart = (BarGraph) convertView.findViewById(R.id.bargraph);
            viewHolder.tvQuestion = (TextView) convertView.findViewById(R.id.tvQuestion);
            viewHolder.llButton = (LinearLayout) convertView.findViewById(R.id.llButton);
            viewHolder.llTextView = (LinearLayout) convertView.findViewById(R.id.llTextView);
            viewHolder.tvNoOfCommentsData = (TextView) convertView.findViewById(R.id.tvNoOfCommentsData);
            viewHolder.llLike = (LinearLayout) convertView.findViewById(R.id.llLike);
            viewHolder.llShare = (LinearLayout) convertView.findViewById(R.id.llShare);
            viewHolder.llComments = (LinearLayout) convertView.findViewById(R.id.llComments);
            viewHolder.tsLikesCounter = (TextSwitcher) convertView.findViewById(R.id.tsLikesCounter);
            viewHolder.btnMore = (ImageView) convertView.findViewById(R.id.btnMore);

            viewHolder.singleImageViewLayout = (RelativeLayout) convertView.findViewById(R.id.singleImageViewLayout);
            viewHolder.twoImageViewLayout = (LinearLayout) convertView.findViewById(R.id.twoImageViewLayout);
            viewHolder.threeImageViewLayout = (LinearLayout) convertView.findViewById(R.id.threeImageViewLayout);


            viewHolder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
            viewHolder.imageView2 = (ImageView) convertView.findViewById(R.id.imageView2);
            viewHolder.imageView3 = (ImageView) convertView.findViewById(R.id.imageView3);
            viewHolder.imageView4 = (ImageView) convertView.findViewById(R.id.imageView4);
            viewHolder.imageView5 = (ImageView) convertView.findViewById(R.id.imageView5);

            viewHolder.extraImageCountTextView = (TextView) convertView.findViewById(R.id.extraImageCount);

            convertView.setTag(viewHolder);
            convertView.setTag(getItemId(position));

        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        if (result.getPosttype().equalsIgnoreCase("video") || result.getPosttype().equalsIgnoreCase("polls")) {
            viewHolder.llShare.setVisibility(View.GONE);
        } else {
            viewHolder.llShare.setVisibility(View.VISIBLE);
        }
        isPressed = true;
        viewHolder.tvNumber_Of_Likes = (TextView) convertView.findViewById(R.id.tvNumber_Of_Likes);
        viewHolder.lvComments = (LinearLayout) convertView.findViewById(R.id.llComments);
        viewHolder.llCommentsValue = (LinearLayout) convertView.findViewById(R.id.llCommentsValue);
//        viewHolder.lvCommentCell =(ListView)convertView.findViewById(R.id.lvCommentCell);
        viewHolder.tvComment1 = (TextView) convertView.findViewById(R.id.tvComment1);
        viewHolder.commentrator2 = (TextView) convertView.findViewById(R.id.commentrator2);
        if (result.getPosttype().equalsIgnoreCase("polls"))
//            viewHolder.chart = (BarGraph) convertView.findViewById(R.id.bargraph);
            viewHolder.tvLikes = (TextView) convertView.findViewById(R.id.tvLikes);
        viewHolder.ivlikes = (ImageView) convertView.findViewById(R.id.ivLikes);
        viewHolder.ivUnLikes = (ImageView) convertView.findViewById(R.id.ivUnLikes);

        viewHolder.v1 = (Button) convertView.findViewById(R.id.btnOne);
        viewHolder.v2 = (Button) convertView.findViewById(R.id.btnTwo);
        viewHolder.v3 = (Button) convertView.findViewById(R.id.btnThree);
        viewHolder.v4 = (Button) convertView.findViewById(R.id.btnFour);

        viewHolder.llShare.setTag(position);
        viewHolder.llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getPosttype().equalsIgnoreCase("Image")) {
                    new ShareImageAsynctask(act,result).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        viewHolder.btnMore.setTag(position);
        final boolean showHideMenu = String.valueOf(result.getUsers_name()).equalsIgnoreCase(Appconstant.USERNAME);

        viewHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int btnPos = (Integer) v.getTag();
                FeedContextMenuManager.getInstance().toggleContextMenuFromView(v, btnPos, showHideMenu, new FeedContextMenu.OnFeedContextMenuItemClickListener() {
                    @Override
                    public void onReportClick(int feedItem) {
                        FeedContextMenuManager.getInstance().hideContextMenu();

                        ReportErrorDo error = new ReportErrorDo();
                        error.setUserId(Appconstant.USER_ID);
                        error.setPostId(result.getPost_id());
                        error.setWeddingCode(Appconstant.WEDDING_CODE);
                        if (Appconstant.TYPE.equals("create_wedding"))
                            error.setStatus("hidden");
                        else
                            error.setStatus("adminReport");

                        final Gson gson = new Gson();
                        String jsonString = gson.toJson(error);
                        RequestGenerator.makePostRequest(act, MadeInHeavenServiceURL.URL_REPORT_ADMIN,
                                jsonString, true, new ResponseListener() {
                                    @Override
                                    public void onSuccess(String response) {
                                        try {
                                            ReportErrorDo res = gson.fromJson(response, ReportErrorDo.class);
                                            if ("true".equals(res.getStatus())) {
                                                String message = "Thanks for your feedback report send to admin";
                                                AlertDialogBox(message);

                                            } else {
                                                Toast.makeText(act, "Not Successfull", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(act, "please_try_again", Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onError(VolleyError error) {
                                        Toast.makeText(act, "please_try_again", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onSuccess_one(JSONObject response) {
                                        // TODO Auto-generated method stub
                                    }
                                });
//                        Toast.makeText(act, "Report to admin is clicked", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSharePhotoClick(int feedItem) {
                        FeedContextMenuManager.getInstance().hideContextMenu();

                    }

                    @Override
                    public void onCopyShareUrlClick(int feedItem) {
                        FeedContextMenuManager.getInstance().hideContextMenu();
                    }

                    @Override
                    public void onCancelClick(int feedItem) {
                        FeedContextMenuManager.getInstance().hideContextMenu();
                    }

                    @Override
                    public void onHideClick(int feedItem) {
                        FeedContextMenuManager.getInstance().hideContextMenu();
                        ReportErrorDo error = new ReportErrorDo();
                        error.setUserId(Appconstant.USER_ID);
                        error.setPostId(result.getPost_id());
                        error.setWeddingCode(Appconstant.WEDDING_CODE);
                        error.setStatus("hidden");

                        final Gson gson = new Gson();
                        String jsonString = gson.toJson(error);
                        RequestGenerator.makePostRequest(act, MadeInHeavenServiceURL.URL_REPORT_ADMIN,
                                jsonString, true, new ResponseListener() {
                                    @Override
                                    public void onSuccess(String response) {
                                        try {
                                            ReportErrorDo res = gson.fromJson(response, ReportErrorDo.class);
                                            if ("true".equals(res.getStatus())) {
                                                String message = "Post sucessfully hidden";
                                                AlertDialogBox(message);
                                                if (onRefreshListener != null) {
                                                    onRefreshListener.onRefresh();
                                                }

                                            } else {
                                                Toast.makeText(act, "Not Successfull", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onError(VolleyError error) {
                                    }

                                    @Override
                                    public void onSuccess_one(JSONObject response) {
                                        // TODO Auto-generated method stub
                                    }
                                });
                    }
                });
            }
        });
        String id = result.getPost_id();
        String posi = String.valueOf(position);

        Iterator myVeryOwnIterator = map.keySet().iterator();
        Iterator unSelected = umap.keySet().iterator();
        String like = result.getLikes();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            String value = (String) map.get(key);

            if ((Integer.parseInt(key) == Integer.parseInt(posi)) || (like.equalsIgnoreCase("true"))) {
                viewHolder.ivlikes.setVisibility(View.VISIBLE);
                viewHolder.ivUnLikes.setVisibility(View.GONE);
                isPressed = false;
            } else {
                viewHolder.ivlikes.setVisibility(View.GONE);
                viewHolder.ivUnLikes.setVisibility(View.VISIBLE);
                updateHeartButton(viewHolder, true);
                isPressed = true;
            }
        }
        while (unSelected.hasNext()) {
            String ukey = (String) unSelected.next();
            String uvalue = (String) map.get(ukey);

            if ((Integer.parseInt(ukey) == Integer.parseInt(posi))) {
                viewHolder.ivlikes.setVisibility(View.VISIBLE);
                viewHolder.ivUnLikes.setVisibility(View.GONE);
                updateHeartButton(viewHolder, true);
                isPressed = true;
            } else {
                viewHolder.ivlikes.setVisibility(View.GONE);
                viewHolder.ivUnLikes.setVisibility(View.VISIBLE);
                isPressed = false;
            }
        }

        if (like.equalsIgnoreCase("true")) {
            viewHolder.ivlikes.setVisibility(View.VISIBLE);
            viewHolder.ivUnLikes.setVisibility(View.GONE);
        } else if (like.equalsIgnoreCase("false")) {
            viewHolder.ivUnLikes.setVisibility(View.VISIBLE);
            viewHolder.ivlikes.setVisibility(View.GONE);
        }

        viewHolder.tvUserName.setText(result.getUsers_name());

        final int com = result.getComments_count();


//        textView.setText(text);

//        Toast.makeText(act,text,Toast.LENGTH_SHORT).show();


        if (com > 0) {
            viewHolder.llCommentsValue.setVisibility(View.VISIBLE);
            if (com < 2) {
                for (int i = 0; i < com; i++) {
                    if (i == 0) {
                        viewHolder.tvComment1.setVisibility(View.VISIBLE);
                        viewHolder.tvComment1.setText(Html.fromHtml("<b>" + result.getComments().get(i).getCommentatorName() + "</b>" + ": " + result.getComments().get(i).getComments_data()));
                    } else if (i == 1) {
                        viewHolder.commentrator2.setVisibility(View.VISIBLE);
                        viewHolder.commentrator2.setText(Html.fromHtml("<b>" + result.getComments().get(i).getCommentatorName() + "</b>" + ": " + result.getComments().get(i).getComments_data()));
                    }
                }
            } else {
                viewHolder.tvComment1.setVisibility(View.VISIBLE);
                viewHolder.commentrator2.setVisibility(View.VISIBLE);
                for (int j = (com - 2); j < com; j++) {
                    if (j == (com - 2))
                        viewHolder.tvComment1.setText(Html.fromHtml("<b>" + result.getComments().get(j).getCommentatorName() + "</b>" + ": " + result.getComments().get(j).getComments_data()));
                    else if (j == (com - 1))
                        viewHolder.commentrator2.setText(Html.fromHtml("<b>" + result.getComments().get(j).getCommentatorName() + "</b>" + ": " + result.getComments().get(j).getComments_data()));
                }
            }
        }

        viewHolder.tvNoOfCommentsData.setText(String.valueOf(com) + " Comment");

        String createdTime = result.getCreated_time();

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date past = null;
        try {
            past = format.parse(createdTime);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Date now = new Date();

        long diff = now.getTime() - past.getTime();
        Log.e("diff",diff+"");
        String time = null;
        long days = diff / (24 * 60 * 60 * 1000);

        if (days == 0) {
            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diff);
            long seconds = diffInSec % 60;
            diffInSec/= 60;
            long minutes =diffInSec % 60;
            diffInSec /= 60;
            long hours = diffInSec % 24;
            diffInSec /= 24;

            if(hours==0){
                if(minutes==0){
                    time = seconds + " seconds ago";
                }else{
                    time = minutes + " minutes ago";
                }
            }else{
                if(minutes>60){
                    hours = minutes/60;
                    time = hours + " hours ago";
                }
                time = minutes + " minutes ago";
            }

        } else if (days == 1)
            time = "Yesterday";
        else if (days <= 7) {
            time = days + " days ago";
        } else if (days > 30) {
            long _days = days / 30;
            time = String.valueOf(_days) + " month ago";
        } else {
            long _days = days / 7;
            time = String.valueOf(_days) + " weeks ago";
        }

        viewHolder.tvUploadTime.setText(time);

    /*    String postDateString = result.getPostDatestring();
      //  viewHolder.tvUploadTime.setText(postDateString);
        Log.e("DateTime",past.getTime()+"");
        viewHolder.tvUploadTime.setText(DateUtil.getPastRelativeTimeSpanString(Long.parseLong(postDateString)*1000, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL));
      //  viewHolder.tvUploadTime.setText(DateUtil.dateDiff(DateUtil.getDate(result.getCreated_time())));
       */
        String likeCounts = result.getLikes_count();

        viewHolder.tvNumber_Of_Likes.setText(result.getLikes_count() + " Like");
        String url = null;
        String str = null;

        viewHolder.llLike.setTag(position);
        viewHolder.llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = (Integer) v.getTag();

                final String pos = String.valueOf(position);
//                Toast.makeText(act, "likes position" + position, Toast.LENGTH_SHORT).show();
                final String postId = result.getPost_id();
                String userId = String.valueOf(Appconstant.USER_ID);
                final String userhaslike = result.getLikes();
                LikesDo likeDo = new LikesDo();
                likeDo.postId = postId;
                likeDo.userId = userId;
                final Gson gson = new Gson();
                String jsonString = gson.toJson(likeDo);
                count = Integer.parseInt(result.getLikes_count());

                RequestGenerator.makePostRequest(act, MadeInHeavenServiceURL.URL_POST_LIKES, jsonString, true,
                        new ResponseListener() {
                            @Override
                            public void onSuccess(String response) {
                                try {
                                    LikesDo likes = gson.fromJson(response, LikesDo.class);
                                    if (likes.getStatus().contains("true")) {
                                        if (userhaslike.equals("true")) // trun into false i.e like to unlike
                                        {
                                            result.setLikes("false");
                                            count--;
                                            updateCount = count;
                                            if (updateCount < 0) {
                                                updateCount = 0;
                                                count = 0;
                                            }

                                            viewHolder.tsLikesCounter.setText(String.valueOf(updateCount) + " like");
                                            result.setLikes_count(String.valueOf(updateCount));
                                            /*viewHolder.tvNumber_Of_Likes.setText("" + updateCount + " like");*/
                                            viewHolder.ivlikes.setVisibility(View.GONE);
                                            updateHeartButton(viewHolder, true);
                                            viewHolder.ivUnLikes.setVisibility(View.VISIBLE);
//                                            Toast.makeText(act, likes.getMsg(), Toast.LENGTH_SHORT).show();

                                        } else { // trun into false i.e unlike  to like
                                            result.setLikes("true");
                                            count++;
                                            updateCount = count;

                                            viewHolder.tsLikesCounter.setText(String.valueOf(updateCount) + " like");
                                            result.setLikes_count(String.valueOf(updateCount));
                                           /* viewHolder.tvNumber_Of_Likes.setText("" + updateCount + " like");*/
                                            map.put(pos, pos);
                                            viewHolder.ivlikes.setVisibility(View.VISIBLE);
                                            viewHolder.ivUnLikes.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(act, "please_try_again", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                Toast.makeText(act, "please_try_again", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess_one(JSONObject response) {
                                // TODO Auto-generated method stub
                            }
                        });
                viewHolder.ivlikes.setVisibility(View.VISIBLE);
                viewHolder.ivUnLikes.setVisibility(View.GONE);
            }
        });

        final ArrayList<SERVICEDATA.POST> comdata = new ArrayList<SERVICEDATA.POST>();

        for (int i = 0; i < result.getComments().size(); i++) {
            SERVICEDATA.POST con = new SERVICEDATA.POST();
            String com_data = result.getComments().get(i).getComments_data();
            String com_name = result.getComments().get(i).getCommentatorName();
            con.setCommentatorName(com_name);
            con.setComments_data(com_data);
            comdata.add(con);
        }
        viewHolder.llComments.setTag(position);
        viewHolder.llComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int element = (Integer) v.getTag();
                String username = result.getUsers_name();
                Intent i = new Intent(act, CommentActivity_new.class);
                i.putExtra("PostId", result.getPost_id());
                Appconstant.COMMENTS = comdata;
                act.startActivity(i);
            }
        });

        viewHolder.llCommentsValue.setTag(position);
        viewHolder.llCommentsValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int element = (Integer) v.getTag();
                String username = result.getUsers_name();
                Intent i = new Intent(act, CommentActivity_new.class);
                i.putExtra("PostId", result.getPost_id());
                Appconstant.COMMENTS = comdata;
                act.startActivity(i);
            }
        });

        String url_img = null;

        final ArrayList<SERVICEDATA> imgData = new ArrayList<SERVICEDATA>();

        if (result.getPosttype().equalsIgnoreCase("Image")) {

            final List<String> imageUrlList = result.getPostImage();

//            for (int i = 0; i < imageUrlList.size(); i++) {
//                SERVICEDATA con = new SERVICEDATA();
//                url_img = imageUrlList.get(i).toString();
//            }


            viewHolder.ivImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String onClickUrl = result.getPostImage().get(0).toString();
                    Intent i = new Intent(act, FullImageActivity.class);
                    i.putStringArrayListExtra("images",new ArrayList<String>(imageUrlList));
                    i.putExtra("ImageUrl", onClickUrl);
                    i.putExtra("position", 0);
                    act.startActivity(i);

                }
            });


            if(imageUrlList.size()>0 && imageUrlList.size()<=1){

                viewHolder.singleImageViewLayout.setVisibility(View.VISIBLE);
                viewHolder.twoImageViewLayout.setVisibility(View.GONE);
                viewHolder.threeImageViewLayout.setVisibility(View.GONE);

                url_img = imageUrlList.get(0);

                if (imageUrlList.get(0) != null) {
                    Picasso.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.ivImages);
                  //  Glide.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.ivImages);
                } else {
                    viewHolder.ivImages.setImageResource(R.drawable.loadingimage);
                }

                viewHolder.singleImageViewLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(act, FullImageActivity.class);
                        i.putStringArrayListExtra("images", new ArrayList<String>(imageUrlList));
                        i.putExtra("position", 0);
                        act.startActivity(i);
                    }
                });


            }else if (imageUrlList.size()>1 && imageUrlList.size()<=2){

                viewHolder.singleImageViewLayout.setVisibility(View.GONE);
                viewHolder.twoImageViewLayout.setVisibility(View.VISIBLE);
                viewHolder.threeImageViewLayout.setVisibility(View.GONE);


                viewHolder.twoImageViewLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(act, FullImageActivity.class);
                        i.putStringArrayListExtra("images", new ArrayList<String>(imageUrlList));
                        i.putExtra("position", 0);
                        act.startActivity(i);
                    }
                });

                if (imageUrlList.get(0) != null) {
                    Picasso.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView1);
          //          Glide.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView1);
                } else {
                    viewHolder.imageView1.setImageResource(R.drawable.loadingimage);
                }

                if (imageUrlList.get(1) != null) {
                    Picasso.with(act).load(imageUrlList.get(1)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView2);
//                    Glide.with(act).load(imageUrlList.get(1)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView2);
                } else {
                    viewHolder.imageView2.setImageResource(R.drawable.loadingimage);
                }


            }else if (imageUrlList.size()>2 && imageUrlList.size()<=8){

                viewHolder.singleImageViewLayout.setVisibility(View.GONE);
                viewHolder.twoImageViewLayout.setVisibility(View.GONE);
                viewHolder.threeImageViewLayout.setVisibility(View.VISIBLE);

                viewHolder.threeImageViewLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(act, FullImageActivity.class);
                        i.putStringArrayListExtra("images", new ArrayList<String>(imageUrlList));
                        i.putExtra("position", 0);
                        act.startActivity(i);
                    }
                });
                if (imageUrlList.get(0) != null) {
                    Log.e("imageUrlList", imageUrlList.get(0));
                    Picasso.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView3);
                  //  Glide.with(act).load(imageUrlList.get(0)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView1);
                } else {
                    viewHolder.imageView3.setImageResource(R.drawable.loadingimage);
                }

                if (imageUrlList.get(1) != null) {
                    Log.e("imageUrlList", imageUrlList.get(1));
                    Picasso.with(act).load(imageUrlList.get(1)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView4);
                 //   Glide.with(act).load(imageUrlList.get(1)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView2);
                } else {
                    viewHolder.imageView4.setImageResource(R.drawable.loadingimage);
                }

                if (imageUrlList.get(2) != null) {
                    Picasso.with(act).load(imageUrlList.get(2)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView5);
                  //  Glide.with(act).load(imageUrlList.get(2)).placeholder(R.drawable.loadingimage).error(R.drawable.loadingimage).into(viewHolder.imageView5);
                } else {
                    viewHolder.imageView5.setImageResource(R.drawable.loadingimage);
                }

                if(imageUrlList.size()>3){
                    viewHolder.extraImageCountTextView.setVisibility(View.VISIBLE);
                    viewHolder.extraImageCountTextView.setText("+"+(imageUrlList.size()-3)+"");
                }else{
                    viewHolder.extraImageCountTextView.setVisibility(View.GONE);
                }

            }


        }
        if (result.getPosttype().equalsIgnoreCase("video")) {


            final String link = result.getVideolink();
            viewHolder.ivImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(act, VideoViewActivity.class);
                    i.putExtra("VideoUrl", link);
                    act.startActivity(i);

                }
            });


            if (!TextUtils.isEmpty(link)) {
                Glide.with(act).load(link).placeholder(R.drawable.play_image).error(R.drawable.play_image).into(viewHolder.ivImages);
            } else {
                viewHolder.ivImages.setImageResource(R.drawable.loadingimage);
            }

            viewHolder.singleImageViewLayout.setVisibility(View.VISIBLE);
            viewHolder.twoImageViewLayout.setVisibility(View.GONE);
            viewHolder.threeImageViewLayout.setVisibility(View.GONE);
        }
        str = result.getUsers_profile_pics();
        if (str != null) {
            Glide.with(act).load(str).placeholder(R.drawable.loadingimage).error(R.drawable.profile_image).into(viewHolder.ivUserPics);
        } else {
            viewHolder.ivUserPics.setImageResource(R.drawable.icon);
        }

        if (result.getPosttype().equalsIgnoreCase("polls")) {
            String option = null;
            String optiontype = null;
            String vote = null;
            String quesID = null;
            String clickOpt = null;

            int op = 0;

            viewHolder.tvQuestion.setText(result.getQuestion() + "\n" + "#" + Appconstant.HASHTAG);

            final ArrayList<SERVICEDATA.POLLS> poll = new ArrayList<SERVICEDATA.POLLS>();
            ArrayList<Bar> points = new ArrayList<Bar>();

            quesID = result.getQuestionId();
            clickOpt = result.getOptionTypeClicked();

            if (!clickOpt.equals("")) {
                if (clickOpt.equals("a")) {
                    viewHolder.v1.setTextColor(Color.parseColor("#ffffff"));
                    viewHolder.v1.setBackgroundResource(R.drawable.rounded_colour_button_vote);

                } else if (clickOpt.equals("b")) {
                    viewHolder.v2.setTextColor(Color.parseColor("#ffffff"));
                    viewHolder.v2.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                } else if (clickOpt.equals("c")) {
                    viewHolder.v3.setTextColor(Color.parseColor("#ffffff"));
                    viewHolder.v3.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                } else if (clickOpt.equals("d")) {
                    viewHolder.v4.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                    viewHolder.v4.setTextColor(Color.parseColor("#ffffff"));
                }
            } else
                viewHolder.v1.setBackgroundResource(R.drawable.rounded_button_vote);


            //polls
            for (int i = 0; i < result.getPolls().size(); i++) {
                SERVICEDATA.POLLS polll = new SERVICEDATA.POLLS();
                option = result.getPolls().get(i).getOption1();
                optiontype = result.getPolls().get(i).getOptiontype();
                String v = result.getPolls().get(i).getVote_a();

                polll.setOption1(option);
                polll.setOptiontype(optiontype);
                polll.setVote_a(v);
                val.add(polll);

                String ans = result.getAnswer();

                if (ans != null) {
                    if (ans.contains("a")) {
                        Appconstant.ans = 0;
                        viewHolder.v1.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                        viewHolder.v1.setTextColor(Color.parseColor("#ffffff"));
                    } else if (ans.contains("b")) {
                        Appconstant.ans = 1;
                        viewHolder.v2.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                        viewHolder.v2.setTextColor(Color.parseColor("#ffffff"));
                    } else if (ans.contains("c")) {
                        Appconstant.ans = 2;
                        viewHolder.v3.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                        viewHolder.v3.setTextColor(Color.parseColor("#ffffff"));
                    } else if (ans.contains("d")) {
                        Appconstant.ans = 3;
                        viewHolder.v4.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                        viewHolder.v4.setTextColor(Color.parseColor("#ffffff"));
                    }
                }
                int vot = 0;
                vote = result.getPolls().get(i).getVote_a();
                vot = (int) Math.round(Float.parseFloat(vote));

                Float fl = Float.parseFloat(vote);

                Bar d = new Bar();
                d.setColor(Color.parseColor("#FFDC73"));
                d.setName(option);

                d.setValue(vot);
                points.add(d);

                if (i == 0) {
                    viewHolder.v1.setVisibility(View.VISIBLE);
                }
                if (i == 1) {
                    viewHolder.v2.setVisibility(View.VISIBLE);
                }
                if (i == 2) {
                    viewHolder.v3.setVisibility(View.VISIBLE);
                }
                if (i == 3) {
                    viewHolder.v4.setVisibility(View.VISIBLE);
                }
                final int userId = Appconstant.USER_ID;

                final String opt = optiontype;
                final int qustId = Integer.parseInt(quesID);
                final String cl = null;

                viewHolder.v1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (result.getAnswer().equals("a")) {
                            String msg = "you have already voted";
                            AlertDialogBox(msg);
                            Toast.makeText(act, "already voted", Toast.LENGTH_SHORT).show();
                        } else {
                            String gloabledecl = result.getAnswer();
                            switch (gloabledecl) {
                                case "b": {
                                    Float vp = Float.valueOf(result.getPolls().get(1).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(1).setVote_a(vs);
                                    break;
                                }
                                case "c": {
                                    Float vp = Float.valueOf(result.getPolls().get(2).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(2).setVote_a(vs);
                                    break;
                                }
                                case "d": {
                                    Float vp = Float.valueOf(result.getPolls().get(3).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(3).setVote_a(vs);
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }

                            Float vp = Float.valueOf(result.getPolls().get(0).getVote_a());
                            int vi = (int) Math.round(vp);
                            String vs = String.valueOf(vi + 1);
                            result.getPolls().get(0).setVote_a(vs);
                            result.setAnswer("a");

                            Appconstant.opti = "a";
                            voteUpdate(userId, qustId, Appconstant.opti);
                            viewHolder.v1.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                            viewHolder.v1.setTextColor(Color.parseColor("#ffffff"));

                            viewHolder.v2.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v2.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v3.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v3.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v4.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v4.setTextColor(Color.parseColor("#959595"));

                            Appconstant.ans = 0;
                            notifyDataSetChanged();
                        }
                    }
                });
                viewHolder.v2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (result.getAnswer().equals("b")) {
                            String msg = "you have already voted";
                            AlertDialogBox(msg);
                            Toast.makeText(act, "already voted", Toast.LENGTH_SHORT).show();
                        } else {
                            String gloabledecl = result.getAnswer();
                            switch (gloabledecl) {
                                case "a": {
                                    Float vp = Float.valueOf(result.getPolls().get(0).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(0).setVote_a(vs);
                                    break;
                                }
                                case "c": {
                                    Float vp = Float.valueOf(result.getPolls().get(2).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(2).setVote_a(vs);
                                    break;
                                }
                                case "d": {
                                    Float vp = Float.valueOf(result.getPolls().get(3).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(3).setVote_a(vs);
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }

                            Float vp = Float.valueOf(result.getPolls().get(1).getVote_a());
                            int vi = (int) Math.round(vp);
                            String vs = String.valueOf(vi + 1);
                            result.getPolls().get(1).setVote_a(vs);
                            result.setAnswer("b");

                            Appconstant.opti = "b";
                            voteUpdate(userId, qustId, Appconstant.opti);
                            viewHolder.v2.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                            viewHolder.v2.setTextColor(Color.parseColor("#ffffff"));

                            viewHolder.v1.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v1.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v3.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v3.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v4.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v4.setTextColor(Color.parseColor("#959595"));

                            Appconstant.ans = 1;
                            notifyDataSetChanged();
                        }

                    }
                });
                viewHolder.v3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (result.getAnswer().equals("c")) {
                            String msg = "you have already voted";
                            AlertDialogBox(msg);
                            Toast.makeText(act, "already voted", Toast.LENGTH_SHORT).show();
                        } else {
                            String gloabledecl = result.getAnswer();
                            switch (gloabledecl) {
                                case "a": {
                                    Float vp = Float.valueOf(result.getPolls().get(0).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(0).setVote_a(vs);
                                    break;
                                }
                                case "b": {
                                    Float vp = Float.valueOf(result.getPolls().get(1).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(1).setVote_a(vs);
                                    break;
                                }
                                case "d": {
                                    Float vp = Float.valueOf(result.getPolls().get(3).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(3).setVote_a(vs);
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }

                            Float vp = Float.valueOf(result.getPolls().get(2).getVote_a());
                            int vi = (int) Math.round(vp);
                            String vs = String.valueOf(vi + 1);
                            result.getPolls().get(2).setVote_a(vs);
                            result.setAnswer("c");

                            Appconstant.opti = "c";
                            voteUpdate(userId, qustId, Appconstant.opti);
                            viewHolder.v3.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                            viewHolder.v3.setTextColor(Color.parseColor("#ffffff"));

                            viewHolder.v1.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v1.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v2.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v2.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v4.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v4.setTextColor(Color.parseColor("#959595"));

                            Appconstant.ans = 2;
                            notifyDataSetChanged();
                        }
                    }
                });
                viewHolder.v4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (result.getAnswer().equals("d")) {
                            String msg = "you have already voted";
                            AlertDialogBox(msg);
                            Toast.makeText(act, "already voted", Toast.LENGTH_SHORT).show();
                        } else {
                            String gloabledecl = result.getAnswer();
                            switch (gloabledecl) {
                                case "a": {
                                    Float vp = Float.valueOf(result.getPolls().get(0).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(0).setVote_a(vs);
                                    break;
                                }
                                case "b": {
                                    Float vp = Float.valueOf(result.getPolls().get(1).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(1).setVote_a(vs);
                                    break;
                                }
                                case "c": {
                                    Float vp = Float.valueOf(result.getPolls().get(2).getVote_a());
                                    int vi = (int) Math.round(vp);
                                    String vs = String.valueOf(vi - 1);
                                    result.getPolls().get(2).setVote_a(vs);
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }

                            Float vp = Float.valueOf(result.getPolls().get(3).getVote_a());
                            int vi = (int) Math.round(vp);
                            String vs = String.valueOf(vi + 1);
                            result.getPolls().get(3).setVote_a(vs);
                            result.setAnswer("d");

                            Appconstant.opti = "d";
                            voteUpdate(userId, qustId, Appconstant.opti);
                            viewHolder.v4.setBackgroundResource(R.drawable.rounded_colour_button_vote);
                            viewHolder.v4.setTextColor(Color.parseColor("#ffffff"));

                            viewHolder.v1.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v1.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v2.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v2.setTextColor(Color.parseColor("#959595"));

                            viewHolder.v3.setBackgroundResource(R.drawable.rounded_button_vote);
                            viewHolder.v3.setTextColor(Color.parseColor("#959595"));

                            Appconstant.ans = 3;
                            notifyDataSetChanged();
                        }
                    }
                });


                int white = R.color.white;

                TextView tv = new TextView(act);
                tv.setText(option);
                tv.setTextColor(act.getResources().getColor(R.color.Dark));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(140, 45);
                if (result.getPolls().size() < 4) {
                    layoutParams.weight = 0f;
                    layoutParams.setMargins(60,0,0,0);
                } else {
                    layoutParams.weight = 1f;
                    layoutParams.setMargins(35, 0, 0, 0);// left, top, right, bottom
                }
                tv.setLayoutParams(layoutParams);
                viewHolder.llTextView.addView(tv);
            }
            if (viewHolder.chart != null) {
                assert viewHolder.chart != null;
                viewHolder.chart.setUnit(" vote");
                viewHolder.chart.appendUnit(true);
                viewHolder.chart.setBars(points);
            }
        }
        return convertView;
    }

    private void showProgressDialog(Activity act) {
        try {
            progressDialog = new ProgressDialog(act);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            COUNT++;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void cancelProgressDialog() {
        try{
            if (progressDialog != null) {
                if (COUNT <= 1) {
                    progressDialog.cancel();
                    progressDialog = null;
                }
                COUNT--;
            }
        }catch (Exception e){

        }
    }
    public void AlertDialogBox(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(act);
        builder1.setMessage(msg);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void voteUpdate(int id, int qId, String option) {
        VoteDO vote = new VoteDO();
        vote.setUser_id(id);
        vote.setOption(option);
        vote.setQuestion_id(qId);

        final Gson gson = new Gson();
        final String jsonString = gson.toJson(vote);
        Log.i("uploadimage", jsonString);

        RequestGenerator.makePostRequest(act, MadeInHeavenServiceURL.URL_POST_VOTE, jsonString,
                true, new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            VoteDO v = gson.fromJson(response, VoteDO.class);
                        } catch (Exception e) {
                            System.out.println("e---->>" + e.toString());
                            Toast.makeText(act, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        // TODO Auto-generated method stub
                        Toast.makeText(act, "toast_please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                        Toast.makeText(act, "JSONObject response_try_again", Toast.LENGTH_SHORT).show();
                    }
                });

    }


    public void setOnFeedItemClickListener(FeedAdapter.OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    private void updateHeartButton(final ViewHolderItem holder, boolean animated) {
        if (animated) {
            if (!likeAnimations.containsKey(holder)) {
                AnimatorSet animatorSet = new AnimatorSet();
//				likeAnimations.put(position, animatorSet);

                ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.ivlikes, "rotation", 0f, 360f);
                rotationAnim.setDuration(300);
                rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.ivlikes, "scaleX", 0.2f, 1f);
                bounceAnimX.setDuration(300);
                bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

                ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.ivlikes, "scaleY", 0.2f, 1f);
                bounceAnimY.setDuration(300);
                bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
                bounceAnimY.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        holder.ivlikes.setImageResource(R.drawable.ic_heart_red);
                    }
                });

                animatorSet.play(rotationAnim);
                animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);

                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                    }
                });
                animatorSet.start();
            }
        }

    }


    public class ViewHolderItem {
        TextView tvUserName;
        TextView tvLikes;
        ImageView tvComments;
        TextView tvShare;
        TextView tvNumber_Of_Likes;
        TextView tvUploadTime;
        ImageView ivUserPics;
        ImageView ivImages;
        ImageView ivUnLikes;
        ImageView ivlikes;
        LinearLayout lvComments;
        TextView tvCommentsData;
        TextView tvViewComments;
        BarGraph chart;
        TextView tvQuestion;
        LinearLayout llButton;
        LinearLayout llTextView;
        LinearLayout llLike;
        LinearLayout llCommentsValue;
        TextSwitcher tsLikesCounter;
        TextView tvNoOfCommentsData;
        TextView tvComment1;
        LinearLayout llShare;
        LinearLayout llComments;
        ListView lvCommentCell;
        TextView commentrator2;
        ImageView btnMore;
        Button v1;
        Button v2;
        Button v3;
        Button v4;


        RelativeLayout singleImageViewLayout;
        LinearLayout twoImageViewLayout,threeImageViewLayout;
        ImageView imageView1,imageView2,imageView3,imageView4,imageView5;
        TextView extraImageCountTextView;
    }


}
