package com.appicmobile.madeinheaven.ui.drawer;

public class ItemObject {

	private String name,weddingCode;
	
	 public ItemObject(String name) {
	        this.name = name;
	        
	    }

	public ItemObject(String name,String weddingCode) {
		this.name = name;
		this.weddingCode = weddingCode;

	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getWeddingCode() {
		return weddingCode;
	}

	public void setWeddingCode(String weddingCode) {
		this.weddingCode = weddingCode;
	}
	
}
