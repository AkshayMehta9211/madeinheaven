package com.appicmobile.madeinheaven.ui.activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramSession;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.gcm.InstanceIdHelper;
import com.viewpagerindicator.CirclePageIndicator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends FragmentActivity {

    private static final String SENDER_ID = "967469277907";
    private TextView login, getsatart;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.src1, R.drawable.src2, R.drawable.src3, R.drawable.src4};
    private InstagramSession mInstagramSession;
    private SharedPreferences spref;

    public PreferenceUtils preference;


    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    private static final String PREF_NAME = "LOGIN_DATA";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String USER_ID = "userId";

    // Email address (make variable public to access from outside)
    public static final String LOGIN_WEDDING_CODE = "wedding";

    public static final String LOGIN_HASHTAG = "hashtag";
    public static final String LOGIN_PROFILE_PICS = "profilepics";
    public static final String LOGIN_USERNAME = "username";
    public static final String LOGIN_TYPE = "userType";


    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    Button btnTryApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        spref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Appconstant.USER_ID = spref.getInt(USER_ID, 0);
        Appconstant.WEDDING_CODE = spref.getString(LOGIN_WEDDING_CODE, "");
        Appconstant.HASHTAG = spref.getString(LOGIN_HASHTAG, "");
        Appconstant.PROFILE_PICS = spref.getString(LOGIN_PROFILE_PICS, "");
        Appconstant.USERNAME = spref.getString(LOGIN_USERNAME, "");
        Appconstant.TYPE = spref.getString(LOGIN_TYPE, "");
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.appicmobile.madeinheaven",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        if (Appconstant.USER_ID != 0) {
            Intent i = new Intent(getApplicationContext(), NavDrawer.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }


        login = (TextView) findViewById(R.id.login);
        btnTryApp = (Button) findViewById(R.id.btnTryApp);

        getsatart = (TextView) findViewById(R.id.getstart);
//       Appconstant.TOKEN 	 = preference.getStringFromPreference(Appconstant.TOKEN,"");
//	   Appconstant.USER_ID 	 = preference.getIntFromPreference(Appconstant.LOGIN_USER_ID, 0);
//	   Appconstant.WEDDING_CODE = preference.getIntFromPreference(Appconstant.LOGIN_WEDDING_CODE,0);
//	   Appconstant.HASHTAG		 = preference.getStringFromPreference(Appconstant.HASHTAG, "");
//	   Appconstant.PROFILE_PICS = preference.getStringFromPreference(Appconstant.LOGIN_PROFILE_PICS,"");
//	   Appconstant.USERNAME     = preference.getStringFromPreference(Appconstant.LOGIN_USERNAME,"");

        init();
         GCM();
        btnTryApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Coming Soon")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String flag = "1";
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.putExtra(Appconstant.FLAG, flag);
                startActivity(i);
            }
        });

        getsatart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String flag = "1";
                Intent getstarted = new Intent(MainActivity.this, GetActivity.class);
                getstarted.putExtra(Appconstant.FLAG, flag);
                startActivity(getstarted);
            }

        });
        //}
    }

    private void GCM() {
        InstanceIdHelper instanceIdHelper = new InstanceIdHelper(MainActivity.this);
        instanceIdHelper.getGcmTokenInBackground(SENDER_ID);
    }

    // Image Viewer
    private void init() {
        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);

        mPager.setAdapter(new SlidingImage_Adapter(MainActivity.this, ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
//        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2000, 2000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }




}

