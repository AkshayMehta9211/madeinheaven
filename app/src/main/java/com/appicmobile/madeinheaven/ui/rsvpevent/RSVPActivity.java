package com.appicmobile.madeinheaven.ui.rsvpevent;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ParseException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.NotificationActivity;
import com.appicmobile.madeinheaven.ui.adapter.RsvpCellAdapter;
import com.appicmobile.madeinheaven.ui.adapter.RsvpEventAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.EVENTDATA;
import com.appicmobile.madeinheaven.ui.common.EventList;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.RSVPEVENTDO;
import com.appicmobile.madeinheaven.ui.pojo.RSVP_HELP_DO;
import com.appicmobile.madeinheaven.ui.tabs.EventActivity;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class RSVPActivity extends Fragment implements View.OnClickListener {

    private static final String TAG = RSVPActivity.class.getSimpleName();
    TextView etPhoneNumber, etEmailId;
    TextView etQuestion;
    ListView listevent;
    Button rsvpevent, rsvphelp;
    private ImageButton marriage, gallery, homepage, event, chat;
    ArrayList<EVENTDATA> allEventData = new ArrayList<>();
    EVENTDATA result;
    String month_Name = null;
    Toolbar toolbar;
    ArrayList<RSVPEVENTDO> data = new ArrayList<RSVPEVENTDO>();

    TextView toolbar_title;
    SeekBar snapBarControl;
    RsvpCellAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        onRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        allEventData.clear();
        adapter = null;
        eventDataFromServer();

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.rsvp_layout, container, false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);


        ((NavDrawer) getActivity()).setActionBarTitle("RSVP");

        FeedContextMenuManager man = new FeedContextMenuManager();
        man.getInstance().dialogDismiss();

        rsvpevent = (Button) rootview.findViewById(R.id.btnRsvpEvent);
        rsvphelp = (Button) rootview.findViewById(R.id.btnRsvpHelp);
        listevent = (ListView) rootview.findViewById(R.id.lvEventList);

        rsvpevent.setOnClickListener(this);
        rsvphelp.setOnClickListener(this);
//        eventDataFromServer();

        return rootview;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }


    private void eventDataFromServer() {
        String s = "test";
        final EventList event = new EventList();
        event.userId = Appconstant.USER_ID;
        event.weddingCode = String.valueOf(Appconstant.WEDDING_CODE);
        final Gson gson = new Gson();
        String jsonString = gson.toJson(event);
        RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_EVENT_LIST, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Log.e(TAG,response);
                            EventList eventList = gson.fromJson(response, EventList.class);
                            if ("true".equals(eventList.getStatus())) {
                                JSONObject jObject = new JSONObject(response);
                                if (jObject != null) {
                                    try {
                                        if (jObject.has("EventsList")) {

                                            JSONArray arrData = jObject.getJSONArray("EventsList");

                                            for (int i = 0; i < arrData.length(); i++) {

                                                String id = null;
                                                String event_tittle = null;
                                                String event_date = null;
                                                String event_day = null;
                                                String event_start_time = null;
                                                String event_end_time = null;
                                                String latitude = null;
                                                String longitude = null;
                                                String address = null;
                                                String info = null;
                                                String months = null;
                                                String rsvp_option = null;

                                                Date dateTime = null;

                                                JSONObject data = arrData.getJSONObject(i);
                                                // comment_count =
                                                // data.getInt(Key.TAG_COMMENT_COUNT);

                                                event_tittle = data.getString("event_tittle");
                                                rsvp_option = data.getString("rsvp_option");
                                                event_date = data.getString("event_date");
                                                event_start_time = data.getString("event_start_time");

//												Appconstant.EVENT_START_TIME = event_date;
//												Appconstant.EVENT_END_TIME = event_end_time;


                                                id = data.getString("id");
                                                event_end_time = data.getString("event_end_time");
                                                latitude = data.getString("latitude");
                                                longitude = data.getString("longitude");
                                                address = data.getString("address");
                                                info = data.getString("info");

                                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                                String dateInString = event_date;
                                                Date date = null;
                                                try {

                                                    date = formatter.parse(dateInString);

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                Calendar c = Calendar.getInstance();
                                                c.setTime(date); // yourdate
                                                // is a object of type Date
                                                int year = c.get(Calendar.YEAR);

                                                int day = c.get(Calendar.DAY_OF_MONTH);
                                                int dat = c.get(Calendar.DATE);

                                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                                switch (dayOfWeek) {
                                                    case 1:
                                                        event_day = "SUN";
                                                        break;
                                                    case 2:
                                                        event_day = "MON";
                                                        break;
                                                    case 3:
                                                        event_day = "TUE";
                                                        break;
                                                    case 4:
                                                        event_day = "WED";
                                                        break;
                                                    case 5:
                                                        event_day = "THU";
                                                        break;
                                                    case 6:
                                                        event_day = "FRI";
                                                        break;
                                                    case 7:
                                                        event_day = "SAT";
                                                        break;

                                                    default:
                                                        break;
                                                }

                                                int month = c.get(Calendar.MONTH);

                                                switch (month) {
                                                    case 0:
                                                        months = "JAN";
                                                        month_Name = "JANUARY";
                                                        break;
                                                    case 1:
                                                        months = "FEB";
                                                        month_Name = "FEBURARY";
                                                        break;
                                                    case 2:
                                                        months = "MARCH";
                                                        month_Name = "MARCH";
                                                        break;
                                                    case 3:
                                                        months = "APRIL";
                                                        month_Name = "APRIL";
                                                        break;
                                                    case 4:
                                                        months = "MAY";
                                                        month_Name = "MAY";
                                                        break;
                                                    case 5:
                                                        months = "JUNE";
                                                        month_Name = "JUNE";
                                                        break;
                                                    case 6:
                                                        months = "JULY";
                                                        month_Name = "JULY";
                                                        break;
                                                    case 7:
                                                        months = "AUG";
                                                        month_Name = "AUGUST";
                                                        break;
                                                    case 8:
                                                        months = "SEPT";
                                                        month_Name = "SEPTEMBER";
                                                        break;
                                                    case 9:
                                                        months = "OCT";
                                                        month_Name = "OCTOBER";
                                                        break;
                                                    case 10:
                                                        months = "NOV";
                                                        month_Name = "NOVEMBER";
                                                        break;
                                                    case 11:
                                                        months = "DEC";
                                                        month_Name = "DECEMBER";
                                                        break;

                                                    default:
                                                        break;
                                                }

                                                EVENTDATA eventValue = new EVENTDATA();
                                                eventValue.setEvent_tittle(event_tittle);

                                                eventValue.setEvent_date(months);
                                                eventValue.setEvent_day(event_day);
                                                eventValue.setId(id);
                                                eventValue.setRsvp_option(rsvp_option);

                                                eventValue.setEvent_date(event_date);
                                                eventValue.setEvent_dmy(event_date);
                                                eventValue.setAddress(address);
                                                eventValue.setInfo(info);
                                                eventValue.setEvent_start_time(event_start_time);
                                                eventValue.setEvent_end_time(event_end_time);
                                                eventValue.setLatitude(latitude);
                                                eventValue.setLongitude(longitude);
                                                eventValue.setEvent_months(months);
                                                eventValue.setEvent_date(String.valueOf(dat));
                                                eventValue.setEvent_year(String.valueOf(year));
                                                allEventData.add(eventValue);
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                Toast.makeText(getActivity(), "response unSuccessfull", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            // Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                        if (!allEventData.isEmpty())
                            adapter = new RsvpCellAdapter(getActivity(), allEventData);


                        if (adapter != null) {
                            listevent.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                            //listevent.setAdapter(adapter);
                            //adapter.notifyDataSetChanged();
                        }

                        listevent.setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                result = allEventData.get(position);

                                // Toast.makeText(getActivity(), "postition" +
                                // position, Toast.LENGTH_SHORT).show();
                                Fragment event = new EventActivity();
                                FragmentManager fragmentManager = getFragmentManager();
                                Bundle args = new Bundle();
                                args.putString("EventTitle", allEventData.get(position).getEvent_tittle());
                                args.putString("EventAddress", allEventData.get(position).getAddress());
                                args.putString("EventInfo", allEventData.get(position).getInfo());
                                args.putString("EventMonths", allEventData.get(position).getInfo());
                                args.putString("EventYear", allEventData.get(position).getInfo());
                                args.putString("EventDate", allEventData.get(position).getEvent_date());
                                args.putString("EventDay", allEventData.get(position).getEvent_day());
                                args.putString("Event_dmy", allEventData.get(position).getEvent_dmy());

                                Appconstant.EVENT_START_TIME = allEventData.get(position).getEvent_date();

                                Appconstant.EVENT_MONTHS = allEventData.get(position).getEvent_months();
                                Appconstant.EVENT_YEAR = allEventData.get(position).getEvent_year();
                                Appconstant.EVENT_DAY = allEventData.get(position).getEvent_day();
                                Appconstant.EVENT_DATE = allEventData.get(position).getEvent_date();
                                Appconstant.EVENT_MONTHS_NAME = month_Name;
                                Appconstant.EVENT_ID = allEventData.get(position).getId();

                                Appconstant.EVENT_TIME_START = allEventData.get(position).getEvent_start_time();
                                Appconstant.EVENT_TIME_END = allEventData.get(position).getEvent_end_time();

                                Appconstant.LATTITUDE = allEventData.get(position).getLatitude();
                                Appconstant.LONGITUDE = allEventData.get(position).getLongitude();
                                Appconstant.ADDRESS = allEventData.get(position).getAddress();
                                Appconstant.INFO = allEventData.get(position).getInfo();

                                args.putString("Lattitude", allEventData.get(position).getLatitude());
                                args.putString("Longitude", allEventData.get(position).getLongitude());
                                event.setArguments(args);
                                fragmentManager.beginTransaction().replace(R.id.main_fragment_container, event)
                                        .commit();
                            }
                        });
                    }

                    @Override
                    public void onError(VolleyError error) {
                        //Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                    }

                });
    }

    public static Fragment newInstance() {
        RSVPActivity activeFragment = new RSVPActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }


    private void RsvpEvent() {
        // TODO Auto-generated method stub

        final Dialog rsvpevent = new Dialog(getActivity());
        rsvpevent.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rsvpevent.setContentView(R.layout.rsvp_events);
        rsvpevent.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Appconstant.rsvpDo = null;

        ListView lvAdapter = (ListView) rsvpevent.findViewById(R.id.lvEventList);

        RsvpEventAdapter Rsvpadapter = new RsvpEventAdapter(getActivity(), allEventData);
        lvAdapter.setAdapter(Rsvpadapter);


        lvAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        Button cancel = (Button) rsvpevent.findViewById(R.id.btnCancel);
        Button btnDone = (Button) rsvpevent.findViewById(R.id.btnDone);


        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                rsvpevent.dismiss();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadOnServer();
                rsvpevent.dismiss();

            }
        });

        rsvpevent.show();
    }

    private void uploadOnServer() {
        final Gson gson = new Gson();
        String jsonString = gson.toJson(Appconstant.rsvpDo);
        RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_RSVP, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            RSVPEVENTDO event = gson.fromJson(response, RSVPEVENTDO.class);
                            if (event.getStatus().contains("true")) {
                                onRefresh();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub

                        Toast.makeText(getActivity(), "successOne", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void onRefresh() {
        onResume();
    }

    private void RsvpHelp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rsvp_help_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button cancel = (Button) dialog.findViewById(R.id.btnCancelRvsp);
        Button submit = (Button) dialog.findViewById(R.id.btnRsvpSubmit);
        etPhoneNumber = (EditText) dialog.findViewById(R.id.etPhoneNumber);
        etEmailId = (EditText) dialog.findViewById(R.id.etEmailId);
        etQuestion = (EditText) dialog.findViewById(R.id.etQuestion);

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNo = etPhoneNumber.getText().toString().trim();
                String EmailId = etEmailId.getText().toString().trim();
                String Question = etQuestion.getText().toString().trim();

                RSVP_HELP_DO help_data = new RSVP_HELP_DO();
                help_data.setUserid(String.valueOf(Appconstant.USER_ID));
                help_data.setEmailid(EmailId);
                help_data.setPhonenumber(phoneNo);
                help_data.setQuestion(Question);
                help_data.setWedding_code(Appconstant.WEDDING_CODE);

                final Gson gson = new Gson();
                String jsonString = gson.toJson(help_data);
                RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_RSVP_HELP_DETAILS, jsonString, true,
                        new ResponseListener() {
                            @Override
                            public void onSuccess(String response) {
                                try {
                                    RSVP_HELP_DO help = gson.fromJson(response, RSVP_HELP_DO.class);
                                    if (help.getStatus().contains("Insert successfully")) {
//                                rsvpevent.dismiss();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess_one(JSONObject response) {
                                // TODO Auto-generated method stub
                                Toast.makeText(getActivity(), "successOne", Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.gallery_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.gallery_notification:
                Intent i = new Intent(getActivity(), NotificationActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }

        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRsvpEvent:
                RsvpEvent();
                break;
            case R.id.btnRsvpHelp:
                RsvpHelp();
                break;


        }

    }
}
