package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.LoginDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.UserRegister;
import com.google.gson.Gson;

import org.json.JSONObject;

public class Login_Account extends Fragment {
	
	EditText etUserEmail,etPassword;
	Button btnLogin;
	TextView tvLogin,tvForget;
	String email_id,password;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootview = inflater.inflate(R.layout.login_account_layout, container, false);
		InitializeControls(rootview);
//		startDrawerActivity();
		return rootview;
	}

	private void startDrawerActivity(){
		Intent i = new Intent(getActivity(),NavDrawer.class);
		startActivity(i);
	}
	private void InitializeControls(View rootview) {
		etUserEmail = (EditText)rootview.findViewById(R.id.etUserEmail);
		etPassword = (EditText)rootview.findViewById(R.id.etPassword);
		tvLogin = (TextView)rootview.findViewById(R.id.tvLogin);
		tvForget = (TextView)rootview.findViewById(R.id.tvForget);
		
		tvLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "Login start...", Toast.LENGTH_SHORT).show();
				LoginWithserver();
			}
		});
		
		tvForget.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment forgotAccount=new ForgotPasswordActivity();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		        fragmentTransaction.replace(R.id.main_container, forgotAccount);
		        fragmentTransaction.commit();				
			}
		});
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}
	private void LoginWithserver() {

		email_id = etUserEmail.getText().toString().trim();
		password = etPassword.getText().toString().trim();

		if (email_id.isEmpty())
			Toast.makeText(getActivity(), "Please enter phone number.", Toast.LENGTH_SHORT).show();
		else if (password.isEmpty())
			Toast.makeText(getActivity(), "Please enter password.", Toast.LENGTH_SHORT).show();
		else {
			// hideKeyBoard(this);

			String s = "test";
			LoginDo login = new LoginDo();
			login.emailId = email_id;
			login.password = password;

			final Gson gson = new Gson();
			String jsonString = gson.toJson(login);
			RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_LOGIN_USER, jsonString, true,
					new ResponseListener() {
						@Override
						public void onSuccess(String response) {
							try {
								UserRegister userLogin1 = gson.fromJson(response, UserRegister.class);
								if ("success".equals(userLogin1.getStatus())) {

									Appconstant.USER_ID = userLogin1.getId();
									Appconstant.WEDDING_CODE = userLogin1.getWeddingCode();
									Appconstant.HASHTAG = userLogin1.getHashtag();
									Appconstant.PROFILE_PICS = userLogin1.getImageUrl();
									Toast.makeText(getActivity(), userLogin1.getMsg(), Toast.LENGTH_SHORT).show();
									startDrawerActivity();


								} else {
									Toast.makeText(getActivity(), userLogin1.getMsg(), Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
								e.printStackTrace();
							}
						}

						@Override
						public void onError(VolleyError error) {
							Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onSuccess_one(JSONObject response) {
							// TODO Auto-generated method stub

						}

					});
		}
	}
}
