package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.Comment_cell_Adapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.CommentDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends Activity {
	
	EditText commentText;
	ImageButton back;
	ListView listview;
	ImageView ivBackButton,send;
	private Comment_cell_Adapter adapter;
	private ArrayList<String> arrayList;
	String value;
	ListView lvList;
	
	List<SERVICEDATA.POST> questions= new ArrayList<SERVICEDATA.POST>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_comments);
		lvList = (ListView)findViewById(R.id.lvList);
		
		value = getIntent().getStringExtra("PostId");
		
		try{
			 questions = Appconstant.COMMENTS;
			adapter = new Comment_cell_Adapter(this,questions);
			 lvList.setAdapter(adapter);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		commentText=(EditText)findViewById(R.id.etcommentedit);
        send=(ImageView)findViewById(R.id.ivSend);
        ivBackButton = (ImageView)findViewById(R.id.ivBackButton);
        arrayList = new ArrayList<String>();
        
        ivBackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(CommentActivity.this,NavDrawer.class);
				startActivity(i);
			}
		});
        
        send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
							String message =  commentText.getText().toString().trim();
				arrayList.add(Appconstant.USERNAME + " : "+commentText.getText().toString());
				CommentDo comment = new CommentDo();
				comment.commentatorId= String.valueOf(Appconstant.USER_ID);
				comment.postId = value;
				comment.comment= message;
				final Gson gson = new Gson();
				String jsonString = gson.toJson(comment);
				RequestGenerator.makePostRequest(CommentActivity.this, MadeInHeavenServiceURL.URL_POST_COMMENTS, jsonString, true,
						new ResponseListener() {
							@Override
							public void onSuccess(String response) {
								try {
									CommentDo comment = gson.fromJson(response, CommentDo.class);
									if (comment.getStatus().contains("true")) {
										Toast.makeText(CommentActivity.this, comment.getMsg(), Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									Toast.makeText(CommentActivity.this, "please_try_again", Toast.LENGTH_SHORT).show();
									e.printStackTrace();
								}
							}

							@Override
							public void onError(VolleyError error) {
								Toast.makeText(CommentActivity.this, "please_try_again", Toast.LENGTH_SHORT).show();
							}

							@Override
							public void onSuccess_one(JSONObject response) {
								// TODO Auto-generated method stub

								Toast.makeText(CommentActivity.this, "successOne", Toast.LENGTH_SHORT).show();
							}
						});
				SERVICEDATA.POST post = new SERVICEDATA.POST();
				post.setCommentatorName((Appconstant.USERNAME));
				post.setComments_data(message);
				questions.add(post);
//				 adapter = new Comment_cell_Adapter(this,questions);
//				 lvList.setAdapter(adapter);
	            adapter.notifyDataSetChanged();
	            commentText.setText("");
			}
		});
        
	}
	}
