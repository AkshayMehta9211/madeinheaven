package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.appicmobile.madeinheaven.R;

public class Joinwdding extends Fragment {

	Button joinwedding, createapp;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootview = inflater.inflate(R.layout.joinwedding_frame, container, false);
		setUserVisibleHint(true);

		joinwedding = (Button) rootview.findViewById(R.id.btnJoinWedding);

		createapp = (Button) rootview.findViewById(R.id.btnCreateWedding);

		joinwedding.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Fragment joinwedding=null;
				Fragment joinwedding = new WeddingCodeActivity();

				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.main_container, joinwedding);
				// fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});

		createapp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment createWeddingApp = new CreateWeddingApp();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.main_container, createWeddingApp);
//				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});

		return rootview;

	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}
	public static Fragment newInstance() {
        Joinwdding activeFragment = new Joinwdding();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }
}