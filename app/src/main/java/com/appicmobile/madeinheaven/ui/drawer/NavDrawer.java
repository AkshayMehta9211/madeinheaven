package com.appicmobile.madeinheaven.ui.drawer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramSession;
import com.appicmobile.madeinheaven.ui.Interface.Key;
import com.appicmobile.madeinheaven.ui.activity.HomeActivity;
import com.appicmobile.madeinheaven.ui.activity.Loginframe;
import com.appicmobile.madeinheaven.ui.activity.MainActivity;
import com.appicmobile.madeinheaven.ui.activity.NotificationActivity;
import com.appicmobile.madeinheaven.ui.activity.PollsActivity;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.FirstPopUp;
import com.appicmobile.madeinheaven.ui.rsvpevent.RSVPActivity;
import com.appicmobile.madeinheaven.ui.tabs.ChatActivity;
import com.appicmobile.madeinheaven.ui.tabs.GalleryActivity;
import com.appicmobile.madeinheaven.ui.tabs.UploadActivity;
import com.appicmobile.madeinheaven.ui.utils.AppSharedPreferences;
import com.appicmobile.madeinheaven.ui.utils.Utils;
import com.bumptech.glide.Glide;
import com.facebook.android.Util;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import twitter4j.JSONArray;

public class NavDrawer extends AppCompatActivity implements OnClickListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    String[] titles = {"Manage Wedding", "Refer a Friend", "Logout"};
    private TextView toolbar_title;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar topToolBar;
    private ImageView marriage, gallery, homepage, event, chat;
    public static final int IMAGE_SELECTION_LIMIT = 5;
    public static final int INTENT_REQUEST_GET_IMAGES = 2123;

    ArrayList<SERVICEDATA> AllData = new ArrayList<>();
    ArrayList<SERVICEDATA> FinalData = new ArrayList<>();

    final static String ScreenName = "thehairmonster";
    final static String LOG_TAG = "";
    private static final String PREF_NAME = "LOGIN_DATA";

    ListView lvListView;
    private String mConsumerKey;
    private String mSecretKey;

    SharedPreferences sharedPrefs;
    boolean isPressed = true;
    boolean isDefaultMenu = true;
    private ArrayList<ItemObject> mistViewItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.sildingdrawer_layout);
        // getSupportActionBar().show();

        marriage = (ImageView) findViewById(R.id.ibMarriage);
        gallery = (ImageView) findViewById(R.id.ibGallery);
        homepage = (ImageView) findViewById(R.id.ibHome);
        event = (ImageView) findViewById(R.id.ibEvent);
        chat = (ImageView) findViewById(R.id.ibChat);
        setOnClickListner();
        checkFirstRun();
        mConsumerKey = "Zi1MjXzh7BhBFd88yZA4VRZ2X";
        mSecretKey = "bJzNj5tswL2WCPC77qINNEKWkLJke5DtkscsiCAlIn6pJbOAyh";
        displayView(0, marriage);

        mTitle = Appconstant.HASHTAG;

        String text = Appconstant.HASHTAG;
        mDrawerTitle = Appconstant.HASHTAG;

        lvListView = (ListView) findViewById(R.id.lvListView);
        topToolBar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(topToolBar);
        // topToolBar.setTitleTextColor(0xffffffff);
        // topToolBar.setTitle(mTitle);
        toolbar_title.setText(text == null ? "" : "#" + text.toUpperCase());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        topToolBar.setNavigationIcon(R.drawable.ic_drawer);
        // topToolBar.setLogoDescription(getResources().getString(R.string.logo_desc));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();

        View listHeaderView = inflater.inflate(R.layout.header_view, null, false);
        final ImageView ivProfilePics;
        TextView profile_name;

        ivProfilePics = (ImageView) listHeaderView.findViewById(R.id.ivProfilePics);
        profile_name = (TextView) listHeaderView.findViewById(R.id.profile_name);

        String picsUrl = Appconstant.PROFILE_PICS;

        if (Appconstant.PROFILE_PICS != null) {
            // ivProfilePics.setImageResource(R.drawable.facebook);
            /*Picasso.with(this).load(picsUrl).into(ivProfilePics, new com.squareup.picasso.Callback() {
                @Override
				public void onSuccess() {
				}

				@Override
				public void onError() {
					ivProfilePics.setImageResource(R.drawable.facebook);
				}
			});*/

            Glide.with(this).load(picsUrl).centerCrop().into(ivProfilePics);
        }

        // else{
        // Picasso.with(this).load(Appconstant.PROFILE_PICS).into(ivProfilePics);
        profile_name.setText(Appconstant.USERNAME);
        // }
        mDrawerList.addHeaderView(listHeaderView);

        List<ItemObject> listViewItems = new ArrayList<ItemObject>();

        for (int i = 0; i < titles.length; i++) {
            ItemObject items = new ItemObject(titles[i]);
            listViewItems.add(items);
        }

        mDrawerList.setAdapter(new CustomAdapter(this, listViewItems));

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open_drawer, R.string.close_drawer) {

        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        // Set the drawer toggle as the DrawerListener

        mDrawerToggle.setDrawerIndicatorEnabled(true);

        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (isDefaultMenu) {
                    if (position == 1) {
                        updateMenuItemList();
                    } else if (position == 2) {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Download 'Made in Heaven App' at https://play.google.com/store/apps/details?id=com.appicmobile.madeinheaven");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                        startActivity(Intent.createChooser(sharingIntent, "Share using"));
                    } else if (position == 3) {
                        sharedPrefs = getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        InstagramSession session = new InstagramSession(getApplication());
                        session.reset();
                        Appconstant.WEDDING_CODE = "";
                        Appconstant.LOGIN_USER_ID = 0;
                        Appconstant.USER_ID = 0;
                        Appconstant.WEDDING_CODE = "";
                        Appconstant.HASHTAG = "";
                        Appconstant.PROFILE_PICS = "";
                        Appconstant.USERNAME = "";
                        Editor editor = sharedPrefs.edit();
                        editor.clear();
                        editor.commit();
                        Loginframe logout = new Loginframe();
                        logout.signOutFacebook();
                        Intent i = new Intent(NavDrawer.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                } else {
                    Log.e("position", position + "-" + mDrawerList.getCount());
                    if (position == 1) {
                        updateMenuItemList();
                    } else if (position == mDrawerList.getCount() - 1) {
                        //TODO Join another wedding
                        showNewWeddingDialog();
                        mDrawerLayout.closeDrawers();
                    } else {
                        // get wedding
                        String weddingCode = mistViewItems.get(position - 1).getWeddingCode();
                        String hashTag = mistViewItems.get(position - 1).getName();
                        if (weddingCode != null && !weddingCode.isEmpty()) {
                            Appconstant.WEDDING_CODE = weddingCode;
                            Appconstant.HASHTAG = hashTag;
                            displayView(0, marriage);
                        }
                        mDrawerLayout.closeDrawers();
                    }
                }
            }

        });
    }

    private void showNewWeddingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Join Another Wedding");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setHint("Enter a valid Wedding Code");
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String code = input.getText().toString();
                if (!TextUtils.isEmpty(code)) {
                    addNewWedding(code);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void addNewWedding(final String code) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("weddingCode", code);
            jsonObject.put("userId", Appconstant.USER_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestGenerator.makePostRequest(this, MadeInHeavenServiceURL.URL_ADD_WEDDING, jsonObject.toString(), true,
                new com.appicmobile.madeinheaven.ui.network.ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("response", response);
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            String status = jsonObject1.getString("status");
                            if(status.equals("successoosss")){
                                String hashtag = jsonObject1.getString("hashtag");
                                Utils.showToast(NavDrawer.this,"New Wedding Added Successfully");
                                AppSharedPreferences sharedPreferences = new AppSharedPreferences(NavDrawer.this);
                                JSONObject jsonObject2 = new JSONObject();
                                jsonObject2.put("hashtag",hashtag);
                                jsonObject2.put("weddingCode",code);

                                org.json.JSONArray jsonArray = sharedPreferences.getWeddingList();
                                jsonArray.put(jsonObject2);
                                sharedPreferences.setWeddingList(jsonArray.toString());
                                updateMenuItemList();
                                Appconstant.WEDDING_CODE = code;
                                Appconstant.HASHTAG = hashtag;
                                displayView(0, marriage);
                            }else {

                                new AlertDialog.Builder(NavDrawer.this  )
                                        .setTitle("Join New Wedding")
                                        .setMessage("Wedding code is wrong")
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        })
                                        .show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        Log.e("error", "error");
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        Log.e("JSON response", response.toString());
                    }
                });
    }

    private void updateMenuItemList() {
        if (isDefaultMenu) {
            isDefaultMenu = false;
            AppSharedPreferences sharedPreferences = new AppSharedPreferences(this);
            org.json.JSONArray jsonArray = sharedPreferences.getWeddingList();
            mistViewItems = new ArrayList<ItemObject>();
            Log.e("jsonArray", jsonArray.toString());
            mistViewItems.add(new ItemObject("Main Menu", ""));

            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String weddingName = jsonObject.getString("hashtag");
                        String weddingCode = jsonObject.getString("weddingCode");
                        ItemObject items = new ItemObject(weddingName, weddingCode);
                        mistViewItems.add(items);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            mistViewItems.add(new ItemObject("Join Another Wedding", ""));

            if (mistViewItems.size() > 0) {
                mDrawerList.setAdapter(new CustomAdapter(this, mistViewItems));
            }
        } else {
            isDefaultMenu = true;
            mistViewItems = new ArrayList<ItemObject>();

            for (int i = 0; i < titles.length; i++) {
                ItemObject items = new ItemObject(titles[i]);
                mistViewItems.add(items);
            }

            mDrawerList.setAdapter(new CustomAdapter(this, mistViewItems));
        }
    }

    public void setActionBarTitle(String title) {
        toolbar_title.setText(title);
    }


    private void selectItemFragment(int position) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            default:
            case 1:
                fragment = new DefaultFragment();

                break;
            case 2:
                fragment = new DefaultFragment();

                break;
        }
        fragmentManager.beginTransaction().replace(R.id.main_fragment_container, fragment).commit();

        mDrawerList.setItemChecked(position, true);
        setTitle(titles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // noinspection SimplifiableIfStatement

        if (id == R.id.action_alert) {

            Intent i = new Intent(NavDrawer.this, NotificationActivity.class);
            startActivity(i);

            return true;
        }
        if (id == R.id.action_face) {

            Intent i = new Intent(NavDrawer.this, PollsActivity.class);
            startActivity(i);
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setOnClickListner() {
        // TODO Auto-generated method stub

        marriage.setOnClickListener(this);
        gallery.setOnClickListener(this);
        homepage.setOnClickListener(this);
        event.setOnClickListener(this);
        chat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ibMarriage:
                displayView(0, marriage);
                marriage.setBackgroundResource(R.color.Dark);
                event.setBackgroundResource(R.color.lightDark);
                break;
            case R.id.ibGallery:
//                man.getInstance().dialogDismiss();
                displayView(1, gallery);
                break;
            case R.id.ibHome:
//                man.getInstance().dialogDismiss();
                displayView(2, homepage);
                break;
            case R.id.ibEvent:
//                man.getInstance().dialogDismiss();
                displayView(3, event);
                break;
            case R.id.ibChat:
//                man.getInstance().dialogDismiss();
                displayView(4, chat);
                break;

        }
    }

    private void displayView(int index, ImageView marriage2) {
        Fragment fragment = null;

        switch (index) {
            case 0:
                fragment = NavDrawer.newInstance();
                if (isPressed) {
                    marriage.setBackgroundResource(R.color.Dark);
                    gallery.setBackgroundResource(R.color.lightDark);
                    event.setBackgroundResource(R.color.lightDark);
                    chat.setBackgroundResource(R.color.lightDark);
                    isPressed = !isPressed;
                } else {
                    marriage.setBackgroundResource(R.color.lightDark);
                }

                isPressed = !isPressed; // reverse

                break;
            case 1:
                fragment = GalleryActivity.newInstance();


                if (isPressed) {
                    gallery.setBackgroundResource(R.color.Dark);
                    marriage.setBackgroundResource(R.color.lightDark);
                    event.setBackgroundResource(R.color.lightDark);
                    chat.setBackgroundResource(R.color.lightDark);
                    isPressed = !isPressed;
                } else {
                    gallery.setBackgroundResource(R.color.Dark);
                }

                isPressed = !isPressed; // reverse
                break;
            case 2:
                fragment = UploadActivity.newInstance();
                if (isPressed) {
                    gallery.setBackgroundResource(R.color.lightDark);
                    marriage.setBackgroundResource(R.color.lightDark);
                    event.setBackgroundResource(R.color.lightDark);
                    chat.setBackgroundResource(R.color.lightDark);
                    isPressed = !isPressed;
                } else {
                    // homepage.setBackgroundResource(R.color.lightDark);
                }

                isPressed = !isPressed; // reverse
                break;
            case 3:
                fragment = RSVPActivity.newInstance();
                if (isPressed) {
                    event.setBackgroundResource(R.color.Dark);
                    gallery.setBackgroundResource(R.color.lightDark);
                    marriage.setBackgroundResource(R.color.lightDark);
                    chat.setBackgroundResource(R.color.lightDark);
                    isPressed = !isPressed;
                } else {
                    event.setBackgroundResource(R.color.Dark);
                }

                isPressed = !isPressed; // reverse
                break;
            case 4:
                fragment = ChatActivity.newInstance();
                if (isPressed) {
                    chat.setBackgroundResource(R.color.Dark);
                    event.setBackgroundResource(R.color.lightDark);
                    gallery.setBackgroundResource(R.color.lightDark);
                    marriage.setBackgroundResource(R.color.lightDark);
                    isPressed = !isPressed;
                } else {
                    chat.setBackgroundResource(R.color.Dark);
                }

                isPressed = !isPressed; // reverse
                break;

        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, fragment, fragment.getClass().getSimpleName()).commit();
    }

    @SuppressWarnings("deprecation")
    private void setActiveTab(Button activeTab, Button inactiveTab1, Button inactive3, Button inactive4) {

        activeTab.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.watever));
        inactiveTab1.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.watever));

    }

    public static Fragment newInstance() {
        HomeActivity activeFragment = new HomeActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
//        Intent i = new Intent(this, NavDrawer.class);
//        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }

    private void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        if (frag instanceof UploadActivity) { // custom interface with no signitures
            frag.onActivityResult(requestCode, resultCode, data);
        }
        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }

    public void checkFirstRun() {
        PreferenceUtils preference = new PreferenceUtils(NavDrawer.this);
        boolean isFirstRun = preference.getbooleanFromPreference(preference.IS_FIRST_RUN, true);
        if (isFirstRun) {
            preference.saveBooleanInPreference(preference.IS_FIRST_RUN, false);
            callPopupService();
        }
    }

    private void callPopupService() {
        JSONObject object = new JSONObject();
        try {
            object.put("weddingCode", String.valueOf(Appconstant.WEDDING_CODE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestGenerator.makePostRequest(NavDrawer.this, MadeInHeavenServiceURL.URL_FIRST_POPUP, object.toString(), false,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        Gson gson = new Gson();
                        FirstPopUp popUp = gson.fromJson(response, FirstPopUp.class);
                        if (popUp.getStatus().equals("true")) {
                            startDialogFragment(popUp);
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                    }
                });
    }

    private void startDialogFragment(FirstPopUp popUp) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment fragment = new PopupDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message", popUp.getMessage());
        bundle.putString("imagelink", popUp.getImage());
        fragment.setArguments(bundle);
        fragment.show(ft, PopupDialogFragment.class.getSimpleName());
    }
}
