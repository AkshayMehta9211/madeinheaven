package com.appicmobile.madeinheaven.ui.adapter;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.EVENTDATA;
import com.appicmobile.madeinheaven.ui.pojo.RSVPEVENTDO;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Ashvini on 07-Jan-16.
 */
public class RsvpEventAdapter extends BaseAdapter {

    Context context;
    private Activity act;
    private ArrayList<EVENTDATA> allData;
    ImageLoader imageLoader;
    EVENTDATA result;
    ViewHolderItem viewHolder;
//	 NavDrawer act;

    ArrayList<RSVPEVENTDO.DATA> event = new ArrayList<RSVPEVENTDO.DATA>();

    private static final int SNAP_MIN = 0;
    private static final int SNAP_MIDDLE = 50;
    private static final int SNAP_MAX = 100;

    private static final int LOWER_HALF = (SNAP_MIN + SNAP_MIDDLE) / 2;
    private static final int UPPER_HALF = (SNAP_MIDDLE + SNAP_MAX) / 2;

    int progressChanged =0;

    ArrayList<RSVPEVENTDO> allUpdate = new ArrayList<RSVPEVENTDO>();
    RSVPEVENTDO even = new RSVPEVENTDO();



    public RsvpEventAdapter(Activity act, ArrayList<EVENTDATA> allData) {

        this.act = act;
        this.allData = allData;
    }

    @Override
    public int getCount() {
        if (allData != null && allData.size() >= 0)
            return allData.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//		 ViewHolderItem viewHolder;

        result = allData.get(position);
        Log.i("resultData", result.toString());


        if (convertView == null) {
            convertView = LayoutInflater.from(act).inflate(R.layout.rsvp_event_confirm_cell, null);
            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.tvEventTitle = (TextView) convertView.findViewById(R.id.tvEventTitle);
            viewHolder.tvTimeBetween = (TextView) convertView.findViewById(R.id.tvTimeBetween);
            viewHolder.tvMonthDay = (TextView) convertView.findViewById(R.id.tvMonthDay);
            viewHolder.tvYear = (TextView) convertView.findViewById(R.id.tvYear);
            viewHolder.ivNext = (ImageView) convertView.findViewById(R.id.ivNext);
            viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tvDay);
            viewHolder.snap_bar = (SeekBar)convertView.findViewById(R.id.snap_bar);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.tvEventTitle.setText(allData.get(position).getEvent_tittle());
        viewHolder.tvDay.setText(allData.get(position).getEvent_day());
        viewHolder.tvTimeBetween.setText(allData.get(position).getEvent_start_time() + " to " + result.getEvent_end_time());
        viewHolder.tvMonthDay.setText(allData.get(position).getEvent_months() + allData.get(position).getEvent_date());
        viewHolder.tvYear.setText(allData.get(position).getEvent_year());


        String rsvu_option= result.getRsvp_option();
        switch (rsvu_option){
            case "yes":
                viewHolder.snap_bar.setProgress(18);
                break;
            case "no":
                viewHolder.snap_bar.setProgress(82);
                break;
            case "maybe":
                viewHolder.snap_bar.setProgress(50);
                break;
        }


        viewHolder.snap_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int progressChanged = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                final int duration = 750;
                int progress = seekBar.getProgress();
                if (progress >= SNAP_MIN && progress <= LOWER_HALF) {
                    ValueAnimator animation = ValueAnimator.ofFloat(0f, 1f);
                    animation.setDuration(duration);
                    animation.start();
                    allData.get(position).setRsvp_option("yes");
                    String msg = "yes";
                    confirmRequestupdate(msg,position);
                    Appconstant.rsvpDo = even;
                    seekBar.setProgress(18);
                    Toast.makeText(act, "yes", Toast.LENGTH_SHORT).show();
                }
                if (progress > LOWER_HALF && progress <= UPPER_HALF) {
                    ValueAnimator animation = ValueAnimator.ofFloat(10f, 50f);
                    animation.setDuration(duration);
                    animation.start();
                    String msg = "maybe";
                    confirmRequestupdate(msg, position);
                    seekBar.setProgress(50);
                    Appconstant.rsvpDo = even;
                    allData.get(position).setRsvp_option("maybe");
                    Toast.makeText(act, "maybe", Toast.LENGTH_SHORT).show();
                }
                if (progress > UPPER_HALF && progress <= SNAP_MAX) {

                    ValueAnimator animation = ValueAnimator.ofFloat(60f, 100f);
                    animation.setDuration(duration);
                    animation.start();
                    String msg = "no";
                    confirmRequestupdate(msg, position);
                    seekBar.setProgress(82);
                    Appconstant.rsvpDo = even;
                    allData.get(position).setRsvp_option("no");
                    Toast.makeText(act, "no", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    private void confirmRequestupdate(String msg,int pos) {
        RSVPEVENTDO.DATA confrm = new RSVPEVENTDO.DATA();
        confrm.setRsvp_option(msg);
        confrm.setUserid(String.valueOf(Appconstant.USER_ID));
        confrm.setEvent_id(allData.get(pos).getId());
        event.add(confrm);
        even.setEvent(event);
    }

    public class ViewHolderItem {
        TextView tvEventTitle;
        TextView tvTimeBetween;
        TextView tvMonthDay;
        TextView tvYear;
        TextView tvDay;
        ImageView ivNext;
        SeekBar snap_bar;
    }
}