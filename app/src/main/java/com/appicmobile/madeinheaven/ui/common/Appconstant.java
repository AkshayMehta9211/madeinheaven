package com.appicmobile.madeinheaven.ui.common;

import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.pojo.RSVPEVENTDO;

import java.util.ArrayList;
import java.util.List;


public class Appconstant 
{

	public static final String RECEIVER_ID = "receiver_id";
	public static final String RECEIVER_IMAGE = "reciver_image";
	public static final String RECEIVER_NAME = "reciver_name";
	public static final String GCM_TOKEN ="gcm_token" ;
	public static String CODE = "";
	public static final boolean DEBUG 	= true;
	public static int USER_ID  ;
	public static String WEDDING_CODE;
	public static String HASHTAG;
	public static String PROFILE_PICS;
	public static String USERNAME;
	public static String TOKEN ="token";
	public static String TYPE;

	public static String FLAG ="flag";

	public static String opti;
	public static int ans;
	
	
	public static long LOGIN_USER_ID;
	public static String LOGIN_WEDDING_CODE="wedding";

	public static String LOGIN_HASHTAG = "hashtag";
	public static String LOGIN_PROFILE_PICS="profilepics";
	public static String LOGIN_USERNAME="username";
	public static String LATTITUDE ;
	public static String LONGITUDE;
	public static String ADDRESS;
	public static String INFO;
	
	public static boolean TWITTER;
	public static boolean FACEBOOK;
	
	public static ArrayList<SERVICEDATA.POST> COMMENTS;
	public static List<RSVPEVENTDO> CONFRM_LIST;
	public static RSVPEVENTDO rsvpDo;
	
	public static String EVENT_TITLE ;
	public static String EVENT_ADDRESS ;
	public static String EVENT_INFO;
	public static String EVENT_MONTHS ;
	public static String EVENT_DATE ;
	public static String EVENT_DAY;
	public static String EVENT_YEAR;
	public static String EVENT_MONTHS_NAME;
	public static String EVENT_START_TIME;
	public static String EVENT_END_TIME;

	public static String EVENT_TIME_START;
	public static String EVENT_TIME_END ;
	public static String EVENT_ID;
	
	public static String twitter_consumer_key;
	public static String twitter_secret_key;
	public static String twitterAccessToken;
	public static String twitterAccessTokenSecret;
	
	public static int i;
	public static ArrayList<ImageData> image;
	/**
	 * @return the image
	 */
	public static ArrayList<ImageData> getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public static void setImage(ArrayList<ImageData> image) {
		Appconstant.image = image;
	}
	
	
	
	
	
	
}
