package com.appicmobile.madeinheaven.ui.activity;

import android.R.color;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.PrisImageAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.PostTweetAsync;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.PostImageAsynctask;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.twitter.TestConnect;
import com.appicmobile.madeinheaven.ui.uploadTab.UPLOADIMAGE;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.ShareDialogBuilder;
import com.facebook.widget.WebDialog;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PostActivity extends ActionBarActivity implements OnClickListener {


    private static final String TAG = PostActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private ArrayList<ImageData> mImageDatas = new ArrayList<>();

    private OnClickListener mAddImageListener;
    private PrisImageAdapter mPrisImageAdapter;

    SharedPreferences sPrefs;
    SharedPreferences.Editor sEdit;
    private UiLifecycleHelper uiHelper;
    public ImageView ivBack;
    private LinearLayout llShareFacebook, llShareInstagram, llShareTwitter;
    private TextView tvShareNow, tvTwitterShare, tvFaceBook;
    ShareDialogBuilder shareDialog;
    EditText etCaption;

    String fbPath;
    Uri ImaheURI;
    String token = "";
    private boolean shareOnFb = false;
    private boolean shareOnTwitter = false;
    private boolean postOnFb;
    private String urlPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        Intent i = getIntent();
        mImageDatas = new ArrayList<>(Appconstant.image);

        uiHelper = new UiLifecycleHelper(this, null);
        uiHelper.onCreate(savedInstanceState);

        sPrefs = this.getSharedPreferences("ImageData", Context.MODE_PRIVATE);
        sEdit = sPrefs.edit();
        try {
            setToolBar();
            initLayout();
            setClickListeners();
            updateViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        setToolBar();
        initLayout();
        setClickListeners();
        uiHelper.onResume();
        // updateViews();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        uiHelper.onDestroy();
    }


    private void updateViews() {

    }

    private void initLayout() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        llShareFacebook = (LinearLayout) findViewById(R.id.llShareFacebook);
        llShareInstagram = (LinearLayout) findViewById(R.id.llShareInstagram);
        llShareTwitter = (LinearLayout) findViewById(R.id.llShareTwitter);
        tvTwitterShare = (TextView) findViewById(R.id.tvTwitterShare);
        tvFaceBook = (TextView) findViewById(R.id.tvFaceBook);
        tvShareNow = (TextView) findViewById(R.id.tvShareNow);
        etCaption = (EditText) findViewById(R.id.etCaption);
        etCaption.setHint("Enter a caption here."+"#"+Appconstant.HASHTAG);

        mRecyclerView.setOnClickListener(this);
        llShareFacebook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFacebookView();
                Session session = com.facebook.Session.getActiveSession();
                if (session == null || !session.isOpened()) {
                    loginWithFacebook();
                }
            }
        });
        llShareInstagram.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        llShareTwitter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleTwitterView();
                Intent i = new Intent(PostActivity.this, TestConnect.class);
                startActivityForResult(i, 135);
            }
        });

//		if (Appconstant.FACEBOOK)
//			tvFaceBook.setTextColor(getResources().getColor(R.color.purple));
//
//		if (Appconstant.TWITTER)
//			tvTwitterShare.setTextColor(getResources().getColor(R.color.purple));
//		else {
////			tvTwitterShare.setTextColor(getResources().getColor(R.color.purple));
//		}

        ivBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PostActivity.this);
                alertDialogBuilder.setMessage("Click on OK to lose all images");

                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Appconstant.image.clear();
                        mImageDatas.clear();
                        finish();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        mRecyclerView.setHasFixedSize(true);
        mPrisImageAdapter = new PrisImageAdapter(this, mImageDatas, 8, mAddImageListener);//todo change null

        LinearLayoutManager llm = new GridLayoutManager(this, 4);
        mRecyclerView.setAdapter(mPrisImageAdapter);

        mRecyclerView.setLayoutManager(llm);

        tvShareNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (shareOnTwitter) {
                    new PostTweetAsync().executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR, PostActivity.this, PostTweetAsync.POST_TWEET_WITH_IMAGE, "#" + Appconstant.HASHTAG + " " + getShareMessage(), mImageDatas);
                }
                final ArrayList<UPLOADIMAGE.Image> base64ImageList = new ArrayList<UPLOADIMAGE.Image>();
                new PostImageAsynctask(mImageDatas,PostActivity.this){
                    @Override
                    protected void onPostExecute(ArrayList<UPLOADIMAGE.Image> images) {
                        Log.e(TAG,images.size()+"");
                        super.onPostExecute(images);
                        try {
                            if (progressDialog != null) {
                                if (COUNT <= 1) {
                                    progressDialog.cancel();
                                    progressDialog = null;
                                }
                                COUNT--;
                            }
                        } catch (Exception e) {

                        }
                        base64ImageList.addAll(images);
                        sendPost(base64ImageList);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


//				onCheckinFb(true);
                /*File file = null;
                ArrayList<UPLOADIMAGE.Image> base64ImageList = new ArrayList<UPLOADIMAGE.Image>();
                for (int i = 0; i < mImageDatas.size(); i++) {
                    String path = mImageDatas.get(i).getImagePath().toString();
                    file = new File(path);


                    // Step 1: Converting File to byte[]
                    Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
                    ExifInterface ei = null;
                    try {
                        ei = new ExifInterface(path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//					int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap retVal = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    retVal.compress(Bitmap.CompressFormat.JPEG, 50, baos); // bm is the
                    // bitmap
                    byte[] b = baos.toByteArray();
                    // Step 2 : converting byte[] to Base64 encoded String
                    String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                    UPLOADIMAGE.Image image = new UPLOADIMAGE.Image();
                    image.setFileextension(".jpg");
                    image.setPath(encodedImage);

                    base64ImageList.add(image);

                }*/

                /*UPLOADIMAGE uploadimage = new UPLOADIMAGE();
                uploadimage.setUserId(Appconstant.USER_ID);
                uploadimage.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));
                uploadimage.setImage(base64ImageList);

                // Toast.makeText(PostActivity.this, "Picture Saved",
                // 2000).show();

                final Gson gson = new Gson();
                final String jsonString = gson.toJson(uploadimage);
                //Log.i("uploadimage", jsonString);

                RequestGenerator.makePostRequest(PostActivity.this, MadeInHeavenServiceURL.URL_POST_IMAGE, jsonString,
                        true, new ResponseListener() {
                            @Override
                            public void onSuccess(String response) {
                                try {
                                    UPLOADIMAGE prescription1 = gson.fromJson(response, UPLOADIMAGE.class);

                                    if (prescription1.getStatus() == true) {
                                        JSONArray arrayData;
                                        JSONObject objectData;
                                        try {
                                            objectData = new JSONObject(response);

                                            if (objectData != null) {
                                                arrayData = objectData.getJSONArray("images");
                                                String Urlpath = null;

                                                for (int i = 0; i < arrayData.length(); i++) {

                                                    JSONObject data = arrayData.getJSONObject(i);
                                                    Urlpath = data.getString("postImg");
                                                }
                                                if (shareOnFb) {
                                                    Session session = com.facebook.Session.getActiveSession();
                                                    if (session == null || !session.isOpened()) {
                                                        loginWithFacebook();
                                                        postOnFb = true;
                                                        urlPath = Urlpath;
                                                    } else {
                                                        onCheckinFb(true, Urlpath);
                                                    }

                                                } else {
                                                    startNavDrawerActivity();
                                                }
                                            }
                                            Toast.makeText(PostActivity.this, "uploaded successfull", Toast.LENGTH_SHORT).show();
                                            etCaption.setText("");


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(PostActivity.this, "uploaded not successfull", Toast.LENGTH_SHORT)
                                                .show();
                                        // onBackPressed();
                                    }

                                } catch (Exception e) {
                                    System.out.println("e---->>" + e.toString());
                                    Toast.makeText(PostActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                // TODO Auto-generated method stub
                                Toast.makeText(PostActivity.this, "toast_please_try_again", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess_one(JSONObject response) {
                                // TODO Auto-generated method stub
                                Toast.makeText(PostActivity.this, "JSONObject response_try_again", Toast.LENGTH_SHORT).show();
                            }
                        });*/

            }
        });
    }

public void sendPost(ArrayList<UPLOADIMAGE.Image> base64ImageList){
    UPLOADIMAGE uploadimage = new UPLOADIMAGE();
    uploadimage.setUserId(Appconstant.USER_ID);
    uploadimage.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));
    uploadimage.setImage(base64ImageList);

    showProgressNotification();

    final Gson gson = new Gson();
    final String jsonString = gson.toJson(uploadimage);
    Log.e("uploadimage", jsonString);

    RequestGenerator.makePostRequest(PostActivity.this, MadeInHeavenServiceURL.URL_POST_IMAGE, jsonString,
            true, new ResponseListener() {
                @Override
                public void onSuccess(String response) {
                    try {
                        UPLOADIMAGE prescription1 = gson.fromJson(response, UPLOADIMAGE.class);

                        if (prescription1.getStatus() == true) {
                            JSONArray arrayData;
                            JSONObject objectData;
                            try {
                                objectData = new JSONObject(response);

                                if (objectData != null) {
                                    arrayData = objectData.getJSONArray("images");
                                    String Urlpath = null;

                                    for (int i = 0; i < arrayData.length(); i++) {

                                        JSONObject data = arrayData.getJSONObject(i);
                                        Urlpath = data.getString("postImg");
                                    }
                                    if (shareOnFb) {
                                        Session session = com.facebook.Session.getActiveSession();
                                        if (session == null || !session.isOpened()) {
                                            loginWithFacebook();
                                            postOnFb = true;
                                            urlPath = Urlpath;
                                        } else {
                                            if (mImageDatas.size() < 8) {
                                                ArrayList<String> resultList = new ArrayList<>();
                                                for (ImageData imageData : mImageDatas) {
                                                    resultList.add(imageData.getImagePath());
                                                }
                                                share("com.facebook.katana", resultList, "JuhiRen");
                                            }
                                            //    onCheckinFb(true, Urlpath);
                                        }

                                    } else {
                                        // startNavDrawerActivity();
                                    }
                                }
                                closeNotification();
//                                Toast.makeText(PostActivity.this, "uploaded successfull", Toast.LENGTH_SHORT).show();
                                etCaption.setText("");
                                startNavDrawerActivity();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            closeNotification();
                            startNavDrawerActivity();
                            // onBackPressed();
                        }

                    } catch (Exception e) {
                        closeNotification();
                        startNavDrawerActivity();
                        System.out.println("e---->>" + e.toString());
                    }
                }

                @Override
                public void onError(VolleyError error) {
                    closeNotification();
                    startNavDrawerActivity();
                    // TODO Auto-generated method stub
                }

                @Override
                public void onSuccess_one(JSONObject response) {
                    closeNotification();
                    startNavDrawerActivity();
                    // TODO Auto-generated method stub
                }
            });


}

    private void showProgressNotification() {
        Intent notificationIntent = null;
        long when = System.currentTimeMillis();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 111, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mbuilder = new NotificationCompat.Builder(this);
        mbuilder.setContentText("Creating feed...");
        mbuilder.setContentIntent(pendingIntent);
        mbuilder.setContentTitle("Made in Heaven");
        mbuilder.setSmallIcon(R.mipmap.ic_launcher);
        mbuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        mbuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        Notification mNotification = mbuilder.build();
        mNotification.when = when;
        mNotification.flags |= Notification.FLAG_ONGOING_EVENT;
        mNotificationManager.notify(111, mNotification);
    }

    private void closeNotification(){
//        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.cancel(111);
        Intent notificationIntent = null;
        long when = System.currentTimeMillis();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 111, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mbuilder = new NotificationCompat.Builder(this);
        mbuilder.setContentText("Click here to view your post");
        mbuilder.setContentIntent(pendingIntent);
        mbuilder.setContentTitle("Made in Heaven");
        mbuilder.setSmallIcon(R.mipmap.ic_launcher);
        mbuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        mbuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        Notification mNotification = mbuilder.build();
        mNotification.when = when;
        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(111, mNotification);

    }

    private void startNavDrawerActivity() {
        Intent mainIntent = new Intent(this, NavDrawer.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK        );
        startActivity(mainIntent);
        finish();
    }

    private void toggleFacebookView() {
        shareOnFb = !shareOnFb;
        tvFaceBook.setTextColor(getResources().getColor(shareOnFb ? R.color.purple : R.color.gray));
    }

    private void toggleTwitterView() {
        shareOnTwitter = !shareOnTwitter;
        tvTwitterShare.setTextColor(getResources().getColor(shareOnTwitter ? R.color.purple : R.color.gray));
    }

    private String getShareMessage() {
        if (!etCaption.getText().toString().isEmpty())
            return etCaption.getText().toString();
        else
            return "Made In Heaven";

    }

	/*public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

		return retVal;
	}*/

    private void loginWithFacebook() {
        Session session = Session.getActiveSession();

        if (session == null)
            session = new Session(getApplicationContext());

        if (session != null && !session.isOpened() && !session.isClosed()) {
            OpenRequest op = new Session.OpenRequest(PostActivity.this);
            op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
            op.setCallback(statusCallback);
            op.setPermissions(getPermissions());
            session = new Builder(PostActivity.this).build();
            Session.setActiveSession(session);
            session.openForRead(op);
            token = session.getAccessToken();
        } else {
            Session.openActiveSession(PostActivity.this, true, statusCallback);
        }
    }

    public List<String> getPermissions() {
        List<String> PERMISSIONS = new ArrayList<String>();
//		 PERMISSIONS.add("publish_stream");
//		 PERMISSIONS.add("publish_actions");
        PERMISSIONS.add("email");
        PERMISSIONS.add("public_profile");
        PERMISSIONS.add("user_friends");
        return PERMISSIONS;
    }

    public Session.StatusCallback statusCallback = new SessionStatusCallback();

    public class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            // showLoader("Logging User through Facebook");
            if (session != null && session.isOpened()) {
                if (postOnFb && urlPath != null) {
                    postOnFb = false;
                  //  onCheckinFb(true, urlPath);
                    if (mImageDatas.size() < 8) {
                        ArrayList<String> resultList = new ArrayList<>();
                        for (ImageData imageData : mImageDatas) {
                            resultList.add(imageData.getImagePath());
                        }
                        share("com.facebook.katana",resultList,"JuhiRen");
                    }

                }
//				if (Appconstant.FACEBOOK)
//					session = com.facebook.Session.getActiveSession();
//				if(session !=null)
//					Appconstant.FACEBOOK = true;
//					tvFaceBook.setTextColor(getResources().getColor(R.color.purple));
            } else {
//				loginWithFacebook();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

//		super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		 /*if (resultCode ==RESULT_OK){

		        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		    }else{
		        Session.getActiveSession().closeAndClearTokenInformation();
		    }*/
        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            // Add top margin only for the first item to avoid double space
            // between items
            if (parent.getChildLayoutPosition(view) == 0)
                outRect.top = space;
        }
    }

    public void onCheckinFb(boolean isDefaultMsg, String urlPath) {

        try {

            Bundle params = new Bundle();
            params.putString("name", "Madeinheaven");
            params.putString("description", "post");
            params.putString("picture", urlPath);
            params.putString("link", urlPath);
            params.putString("caption", "#" + Appconstant.HASHTAG + " " + getShareMessage());

            Session session = Session.getActiveSession();
            final String fbtoken = session.getAccessToken();
            if (session.isOpened()) {
                WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(PostActivity.this,
                        session, params)).setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values, FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
							/*final String postId = values.getString("post_id");
							if (postId != null) {
								Toast.makeText(PostActivity.this, "Sucessfully Posted", Toast.LENGTH_SHORT).show();
								startNavDrawerActivity();
							} else {*/
                            // User clicked the Cancel button
                            Toast.makeText(PostActivity.this.getApplicationContext(), "Sucessfully Posted",
                                    Toast.LENGTH_SHORT).show();
                            startNavDrawerActivity();
                            //}
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(PostActivity.this.getApplicationContext(), "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(PostActivity.this.getApplicationContext(), "Error posting story",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }).build();
                feedDialog.show();
            } else {
                Toast.makeText(PostActivity.this, "Please try again", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Toast.makeText(this, "error in publish", Toast.LENGTH_SHORT).show();
        }
    }

    private void setToolBar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        if (getSupportActionBar() != null) {
            setTitle("Post");
            setTitleColor(color.white);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

    }

    private void setClickListeners() {
        mAddImageListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageDatas.size() < 8) {
                    ArrayList<String> resultList = new ArrayList<>();
                    for (ImageData imageData : mImageDatas) {
                        resultList.add(imageData.getImagePath());
                    }

                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (mImageDatas.size() < 8) {
            ArrayList<String> resultList = new ArrayList<>();
            for (ImageData imageData : mImageDatas) {
                resultList.add(imageData.getImagePath());
            }
        }
    }


    protected void share(String nameApp, ArrayList<String> resultList, String text) {
// TODO Auto-generated method stub

        try {
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
            share.setType("image/jpeg");
            List<ResolveInfo> resInfo = getPackageManager()
                    .queryIntentActivities(share, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    Intent targetedShare = new Intent(
                            android.content.Intent.ACTION_SEND_MULTIPLE);
                    targetedShare.setType("image/png"); // put here your mime
                    // type
                    if (info.activityInfo.packageName.toLowerCase().contains(
                            nameApp)
                            || info.activityInfo.name.toLowerCase().contains(
                            nameApp)) {
                        targetedShare.putExtra(Intent.EXTRA_SUBJECT, text);
                        targetedShare.putExtra(Intent.EXTRA_TEXT, text);
                        ArrayList<Uri> files = new ArrayList<Uri>();
                        for (int j = 0; j < resultList.size(); j++) {
                            if (!resultList.get(j).isEmpty()) {
                                File file = new File(resultList.get(j));
                                Uri uri = Uri.fromFile(file);
                                files.add(uri);
                            }
                        }

                        targetedShare.putParcelableArrayListExtra(Intent.EXTRA_STREAM,
                                files);
                        targetedShare.setPackage(info.activityInfo.packageName);
                        targetedShareIntents.add(targetedShare);
                    }
                }
                Intent chooserIntent = Intent.createChooser(
                        targetedShareIntents.remove(0), "Select app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        targetedShareIntents.toArray(new Parcelable[]{}));
                startActivity(chooserIntent);
            }
        } catch (Exception e) {
        }
    }



}
