package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;


public class WeddingCodeActivity extends Fragment{

	ImageButton btncode;
	EditText etWeddingcode;
	TextView tvCancelWeedingcode;
	String code;
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootview=inflater.inflate(R.layout.weedingcode_layout,container,false);
		
//		code = getArguments().getString("code");
//		Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
		
		etWeddingcode = (EditText)rootview.findViewById(R.id.etWeddingcode);
		tvCancelWeedingcode = (TextView)rootview.findViewById(R.id.tvCancelWeedingcode);
		btncode=(ImageButton)rootview.findViewById(R.id.btnJoincode);
		
		btncode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String etcode = etWeddingcode.getText().toString().trim();
				setHideSoftKeyboard(etWeddingcode);
				Appconstant.WEDDING_CODE = etcode;
				Fragment haveaccount=new HaveAccountActivity();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
				Bundle args = new Bundle();
				 args.putString(Appconstant.FLAG, etcode);
				 haveaccount.setArguments(args);
				 
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		        fragmentTransaction.replace(R.id.main_container, haveaccount);
//		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
		});




		tvCancelWeedingcode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment getStarted = new Joinwdding();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		        fragmentTransaction.replace(R.id.main_container, getStarted);
//		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
		});
		return rootview;
	}

	public void setHideSoftKeyboard(EditText et){
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}



	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}
}