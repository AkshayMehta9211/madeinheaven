package com.appicmobile.madeinheaven.ui.pojo;

/**
 * Created by Ashvini on 08-Jan-16.
 */
public class RSVP_HELP_DO {

    public String getWedding_code() {
        return wedding_code;
    }

    public void setWedding_code(String wedding_code) {
        this.wedding_code = wedding_code;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String wedding_code;
    public String userid;
    public String phonenumber;
    public String emailid;
    public String question;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;
}
