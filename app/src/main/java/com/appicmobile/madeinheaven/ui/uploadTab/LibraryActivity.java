package com.appicmobile.madeinheaven.ui.uploadTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.ImageAdapter;
import com.appicmobile.madeinheaven.ui.multipleimages.MultiImageSelectorActivity;


public class LibraryActivity extends Fragment {

	 ImageAdapter myImageAdapter;
	 public static final int IMAGE_SELECTION_LIMIT = 5;
		public static final int INTENT_REQUEST_GET_IMAGES = 2123;
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

//		PhotosActivity act = new PhotosActivity();
//		act.onDestroy();
		View rootview = inflater.inflate(R.layout.activity_library, container, false);
		MultiImageSelectorActivity();
		
		/* GridView gridview = (GridView)rootview. findViewById(R.id.gridview);
	        myImageAdapter = new ImageAdapter(getActivity());
	        gridview.setAdapter(myImageAdapter);
	        
	        Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	        
	        
	        String ExternalStorageDirectoryPath = Environment.getExternalStorageDirectory().getAbsolutePath();
	        
	        String targetPath = ExternalStorageDirectoryPath + "/test/";
	        
	        Toast.makeText(getActivity(), targetPath, Toast.LENGTH_LONG).show();
	        File targetDirector = new File(targetPath);
	        
	        File[] files = targetDirector.listFiles();
	        if(files !=null  && files.length > 0){
	        for (File file : files){
	         myImageAdapter.add(file.getAbsolutePath());
	        } 
	        }*/
		return rootview;
		
	}
	public static Fragment newInstance() {
		LibraryActivity activeFragment = new LibraryActivity();
		Bundle bundle = new Bundle();
		activeFragment.setArguments(bundle);
		return activeFragment;
	}
	
	protected void MultiImageSelectorActivity() {
		
		Intent intent = new Intent(getActivity(), MultiImageSelectorActivity.class);
		intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, IMAGE_SELECTION_LIMIT);
		intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);
		startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
	}
	
}
