package com.appicmobile.madeinheaven.ui.activity;


import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.Instagram;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramSession;
import com.appicmobile.madeinheaven.ui.Interface.Key;
import com.appicmobile.madeinheaven.ui.adapter.FeedAdapter;
import com.appicmobile.madeinheaven.ui.adapter.Home_cell_Adapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.utils.CircularProgressView;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import twitter4j.Twitter;

public class HomeActivity extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private TextView tvUserName, tvLikes, tvComments, tvShare, tvNumber_Of_Likes, tvUploadTime;
    private ImageView ivUserPics;
    private LinearLayout lvImages;
    ListView lvListView;
    JSONArray data = null;
    ArrayList<String> contactList;
    private String mConsumerKey;
    private String mSecretKey;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    Home_cell_Adapter adapter;
    private InstagramSession mInstagramSession;
    private Instagram mInstagram;
    CoordinatorLayout clContent;
    private static final String CLIENT_ID = "09fce637a1cd40239cd9ed79856fc7fa";
    private static final String CLIENT_SECRET = "252691b554684be8848c170fd52581b0";
    private static final String REDIRECT_URI = "http://davaaii.com/made_in_heaven1.1";

    private ArrayList<SERVICEDATA> allData;
    private FeedAdapter.OnFeedItemClickListener onFeedItemClickListener;

    ArrayList<SERVICEDATA> AllData = new ArrayList<SERVICEDATA>();
    ArrayList<SERVICEDATA> FinalData;

    ArrayList<SERVICEDATA.POST> coment = new ArrayList<SERVICEDATA.POST>();

    final static String ScreenName = "thehairmonster";
    final static String LOG_TAG = "MadeinHeaven";
    private SwipeRefreshLayout swipeRefreshLayout;

    TextView toolbar_title;
    MenuItem action_face;
    FeedContextMenuManager man = new FeedContextMenuManager();
    private PreferenceUtils preference;
    private CircularProgressView mCircularProgressView;
    private SimpleDateFormat mSimpleDateFormat;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        FinalData = new ArrayList<SERVICEDATA>();
    }

    @Override
    public void onResume() {
        super.onResume();
        man.getInstance().dialogDismiss();
        FinalData.clear();
        adapter = null;
        mInstagram = new Instagram(getActivity(), CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);
        mInstagramSession = mInstagram.getSession();
        lvListView.setVisibility(View.GONE);
        onRefresh();

    }

    // Date
    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.activity_home, container, false);
        setHasOptionsMenu(true);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        String text = Appconstant.HASHTAG;

        ((NavDrawer) getActivity()).setActionBarTitle("#" + text);

        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        preference=new PreferenceUtils(getActivity());

        mConsumerKey = "Zi1MjXzh7BhBFd88yZA4VRZ2X";
        mSecretKey = "bJzNj5tswL2WCPC77qINNEKWkLJke5DtkscsiCAlIn6pJbOAyh";

        lvListView = (ListView) view.findViewById(R.id.lvListView);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        mCircularProgressView = (CircularProgressView) view.findViewById(R.id.progressBar);
        mCircularProgressView.setVisibility(View.VISIBLE);
        lvListView.setVisibility(View.GONE);
      /* loadDataFromTwitter();
        postDataOnServer();
        loadDataFromServer();
        itemListToJsonConvert(AllData);*/
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private JSONObject itemListToJsonConvert(ArrayList<SERVICEDATA> list) {

        JSONObject jResult = new JSONObject();// main object
        JSONArray jArray = new JSONArray();// /ItemDetail jsonArray
        if(list.size()>0) {
            for (int i = 0; i < list.size(); i++) {
                JSONObject jGroup = new JSONObject();// /sub Object
                JSONObject jUser = new JSONObject();
                JSONArray jImage = new JSONArray();

                try {
                    String userID = list.get(i).getPost_id();

                    String postImage = list.get(i).getImages_standard_resolution();
                    jResult.put("userId", Appconstant.USER_ID);
                    jResult.put("weddingCode", Appconstant.WEDDING_CODE);
                    jGroup.put("postId", list.get(i).getPost_id());

                    jImage.put(list.get(i).getImages_standard_resolution());

                    jGroup.put("userName", list.get(i).getUsers_name());
                    jGroup.put("profilePic", list.get(i).getUsers_profile_pics());
                    jGroup.put("postDate", list.get(i).getCreated_time());
                    jGroup.put("type", list.get(i).getType());
                    jGroup.put("postImg", jImage);
                    jArray.put(jGroup);

                    jResult.put("getallpost", jArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else{

            try {
                jResult.put("userId", Appconstant.USER_ID);
                jResult.put("weddingCode", Appconstant.WEDDING_CODE);
                jResult.put("getallpost",jArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i("JSON RESPONSE", jResult.toString());

        return jResult;
    }

    private void postDataOnServer() {

    }

    private void loadDataFromTwitter() {
        // TODO Auto-generated method stub
        // download twitter timeline after first checking to see if there is a
        // network connection

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadTwitterTask().execute(ScreenName);
        } else {
            Log.v(LOG_TAG, "No network connection available.");
        }
    }


    // Uses an AsyncTask to download a Twitter user's timeline
    private class DownloadTwitterTask extends AsyncTask<String, Void, String> {
        // final static String CONSUMER_KEY = "MY CONSUMER KEY";
        // final static String CONSUMER_SECRET = "MY CONSUMER SECRET";
        final static String TwitterTokenURL = "https://api.twitter.com/oauth2/token";
        final static String TwitterStreamURL = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=";

        @Override
        protected String doInBackground(String... screenNames) {
            String result = null;

            if (screenNames.length > 0) {
                result = getTwitterStream(screenNames[0]);
            }
            return result;
        }

        // onPostExecute convert the JSON results into a Twitter object (which
        // is an Array list of tweets
        @Override
        protected void onPostExecute(String result) {
            // Twitter twits = jsonToTwitter(result);
        }

        // converts a string of JSON data into a Twitter object
        @SuppressWarnings("unchecked")
        private Twitter jsonToTwitter(String result) {
            Twitter twits = null;
            if (result != null && result.length() > 0) {
                try {
                    Gson gson = new Gson();
                    java.lang.reflect.Type collectionType = new TypeToken<List<Twitter>>() {
                    }.getType();
                    List<Twitter> lcs = /* (List<Twitter>) */new Gson().fromJson(result, collectionType);
                    twits = gson.fromJson(result, Twitter.class);
                } catch (IllegalStateException ex) {
                    // just eat the exception
                }
            }
            return twits;
        }

        // convert a JSON authentication object into an Authenticated object
        private Authenticated jsonToAuthenticated(String rawAuthorization) {
            Authenticated auth = null;
            if (rawAuthorization != null && rawAuthorization.length() > 0) {
                try {
                    Gson gson = new Gson();
                    auth = gson.fromJson(rawAuthorization, Authenticated.class);
                } catch (IllegalStateException ex) {
                    // just eat the exception
                }
            }
            return auth;
        }

        private String getResponseBody(HttpRequestBase request) {
            StringBuilder sb = new StringBuilder();
            try {

                DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                String reason = response.getStatusLine().getReasonPhrase();

                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();

                    BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    String line = null;
                    while ((line = bReader.readLine()) != null) {
                        sb.append(line);
                    }
                } else {
                    sb.append(reason);
                }
            } catch (UnsupportedEncodingException ex) {
            } catch (ClientProtocolException ex1) {
            } catch (IOException ex2) {
            }
            return sb.toString();
        }

        private String getTwitterStream(String screenName) {
            String results = null;

            // Step 1: Encode consumer key and secret
            try {
                // URL encode the consumer key and secret
                String urlApiKey = URLEncoder.encode(mConsumerKey, "UTF-8");
                String urlApiSecret = URLEncoder.encode(mSecretKey, "UTF-8");

                // Concatenate the encoded consumer key, a colon character, and
                // the
                // encoded consumer secret
                String combined = urlApiKey + ":" + urlApiSecret;

                // Base64 encode the string
                String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);

                HttpPost httpPost = new HttpPost(TwitterTokenURL);
                httpPost.setHeader("Authorization", "Basic " + base64Encoded);
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
                String rawAuthorization = getResponseBody(httpPost);
                Authenticated auth = jsonToAuthenticated(rawAuthorization);

                // Applications should verify that the value associated with the
                // token_type key of the returned object is bearer
                if (auth != null && auth.token_type.equals("bearer")) {

                    // Step 3: Authenticate API requests with bearer token
                    HttpGet httpGet = new HttpGet(TwitterStreamURL + screenName);

                    // construct a normal HTTPS request and include an
                    // Authorization
                    // header with the value of Bearer <>
                    httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                    httpGet.setHeader("Content-Type", "application/json");
                    // update the results with the body of the response
                    results = getResponseBody(httpGet);

                    TwitterResponse(results);
                    Log.i("JsonResponse-Twitter", results);

                }
            } catch (UnsupportedEncodingException ex) {
            } catch (IllegalStateException ex1) {
            }
            return results;
        }

        private void TwitterResponse(String twitt_resp) {

            try {
                JSONArray arrayData = new JSONArray(twitt_resp);

                if (arrayData != null) {
                    Log.e("twitter arrayData",arrayData.length()+"");
                    for (int i = 0; i < arrayData.length(); i++) {

                        JSONObject data = arrayData.getJSONObject(i);

                        String createdTime = null;
                        String post_id = null;
                        String user_name = null;
                        String postImageUrl = null;
                        String profile_image_url = null;

                        if (data.getJSONObject("entities").has("media")) {
                            Log.e("data",data.toString());
                            createdTime = data.getString("created_at");
                            post_id = data.getString("id_str");

                            JSONObject user = data.getJSONObject("user");

                            if (user.has("name")) {
                                user_name = user.getString("name");
                            }
                            if (user.has("profile_image_url")) {
                                profile_image_url = user.getString("profile_image_url");
                            }

                            JSONObject entities = data.getJSONObject("entities");

                            if (entities.has("media")) {
                                JSONArray media = entities.getJSONArray("media");
                                for (int med = 0; med < media.length(); med++) {
                                    JSONObject indices = media.getJSONObject(med);
                                    if (indices.has("media_url")) {
                                        postImageUrl = indices.getString("media_url");
                                    }
                                }
                            }

                            SERVICEDATA allvalues = new SERVICEDATA();
                            allvalues.setImages_standard_resolution(postImageUrl);
                            ;/* setImages_standard_resolution(postImageUrl); */
                            allvalues.setUsers_name(user_name);
                            allvalues.setUsers_profile_pics(profile_image_url);
                            allvalues.setCreated_time(createdTime);
                            allvalues.setType("twitter");
                            allvalues.setPost_id(post_id);
                            AllData.add(allvalues);
                            Log.e("twitter AllData",AllData.size()+"");
                        }
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    String BaseUrl = "https://api.instagram.com/v1/tags/" + Appconstant.HASHTAG
            + "/media/recent?access_token=237229928.09fce63.5c7ce2a66ddc453b92c93bfeadf55e90";

    private void loadDataFromServer() {
        Log.e("URL",BaseUrl);
        swipeRefreshLayout.setRefreshing(true);
        // String jsonString = gson.toJson("");
        RequestGenerator.makeGetRequest(getActivity(), BaseUrl, false, new ResponseListener() {
            @Override
            public void onSuccess(String response) {
                try {

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess_one(JSONObject response) {
                try {
                    JSONObject jsonResponse = new JSONObject();
                    jsonResponse = response.getJSONObject("pagination");
                    jsonResponse = response.getJSONObject("meta");
                    JSONArray jsonArray = response.getJSONArray("data");

                    if (response != null) {
                        try {

                            if (response.has(Key.TAG_DATA)) {

                                // Getting JSON Array node

                                JSONArray arrData = response.getJSONArray(Key.TAG_DATA);

                                for (int i = 0; i < arrData.length(); i++) {
                                    int comment_count = 0;
                                    String created_time = null;
                                    String post_id = null;

                                    JSONObject data = arrData.getJSONObject(i);

                                    created_time = data.getString(Key.TAG_CREATED_TIME);

                                    post_id = data.getString(Key.TAG_ID);
                                    JSONObject likes = data.getJSONObject(Key.TAG_LIKE);
                                    String likes_count = null;
                                    if (likes.has(Key.TAG_LIKE_COUNT)) {
                                        likes_count = likes.getString(Key.TAG_LIKE_COUNT);
                                    }

                                    JSONObject images = data.getJSONObject(Key.TAG_IMAGES);

                                    String images_standard = null;
                                    if (images.has(Key.TAG_IMAGES_STANDARD_RESOLUTION)) {
                                        images_standard = images.getJSONObject(Key.TAG_IMAGES_STANDARD_RESOLUTION)
                                                .getString(Key.TAG_IMAGES_STANDARD_RESOLUTION_URL);
                                    }

                                    JSONObject user = data.getJSONObject(Key.TAG_USER);

                                    String username = null;
                                    String profilePics = null;
                                    String userFullName = null;
                                    if (user.has(Key.TAG_USER_NAME)) {
                                        username = user.getString(Key.TAG_USER_NAME);
                                    }
                                    if (user.has(Key.TAG_USER_PROFILE_PICS)) {
                                        profilePics = user.getString(Key.TAG_USER_PROFILE_PICS);
                                    }
                                    if (user.has(Key.TAG_USER_FULL_NAME)) {
                                        userFullName = user.getString(Key.TAG_USER_FULL_NAME);
                                    }

                                    SERVICEDATA allvalues = new SERVICEDATA();
                                    allvalues.setImages_standard_resolution(images_standard);
                                    allvalues.setUsers_name(userFullName);
                                    allvalues.setUsers_profile_pics(profilePics);
                                    allvalues.setCreated_time(created_time);
                                    allvalues.setType("instagram");
                                    allvalues.setPost_id(post_id);
                                    allvalues.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));

                                    AllData.add(allvalues);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                swipeRefreshLayout.setRefreshing(false);
                Log.i("AllData", AllData.size()+"");
                JSONObject obj = itemListToJsonConvert(AllData);

                final Gson gson = new Gson();

                String jsonString = gson.toJson(obj.toString());

                RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_MEDIA, obj.toString(), false,
                        new com.appicmobile.madeinheaven.ui.network.ResponseListener() {
                            @Override
                            public void onSuccess(String response) {
                                try {
                                    getResponseFromServer(response);
                                } catch (Exception e) {
                                    System.out.println("e---->>" + e.toString());
                                    Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
                                }
                            }

                            private void getResponseFromServer(String serverResponse) {
                                Log.i("serverResponse", serverResponse);

                                FinalData.clear();
                                JSONArray arrayData;
                                JSONObject objectData;
                                try {
                                    objectData = new JSONObject(serverResponse);

                                    if (objectData != null) {

                                        // String = null;
                                        // userid = objectData.getString("userId");

                                        arrayData = objectData.getJSONArray("getallpost");

                                        for (int i = 0; i < arrayData.length(); i++) {

                                            JSONObject data = arrayData.getJSONObject(i);

                                            String postDate = null;
                                            String post_id = null;
                                            String userName = null;
//									String postImage = null;
                                            String profilePic = null;
                                            String type = null;
                                            String userhaslike = null;
                                            String totalLike = null;
                                            String postType = null;
                                            String comment_one = null;
                                            String commentatorName = null;
                                            String commentCounts = "0";
                                            String postImg = null;
                                            String question = null;
                                            String option = null;
                                            String optiontype = null;
                                            String vote = null;
                                            String answer = null;
                                            String questionId = null;
                                            String videoLink = null;
                                            String postDatestring = null;


                                            post_id = data.getString("postId");
                                            profilePic = data.getString("profilePic");
                                            userName = data.getString("userName");
                                            videoLink = data.getString("videolink");
                                            List<String> postImage = new ArrayList<String>();
                                            JSONArray objVal = data.getJSONArray("postImg");
                                            for (int j = 0; j < objVal.length(); j++) {

                                                String im = objVal.getString(j).toString();
                                                postImage.add(im);
                                            }


                                            userhaslike = data.getString("userhaslike");
                                            totalLike = data.getString("totalLike");
                                            postType = data.getString("posttype");
                                            postDate = data.getString("postDate");
                                            postDatestring = data.getString("postDatestring");


                                            type = data.getString("type");

                                            if (data.has("answer")) {
//                                                if (!"".equals(data.getString("answer"))) {
                                                answer = data.getString("answer");
//                                                }
                                            }

                                            ArrayList<SERVICEDATA.POST> val = new ArrayList<SERVICEDATA.POST>();

                                            JSONArray comment_two = data.getJSONArray("comments");
                                            commentCounts = String.valueOf(comment_two.length());
                                            JsonObject jObj = new JsonObject();
                                            JsonArray cArray = new JsonArray();
                                            for (int j = 0; j < comment_two.length(); j++) {
                                                JSONObject comm = comment_two.getJSONObject(j);
                                                SERVICEDATA.POST comments = new SERVICEDATA.POST();

                                                comment_one = comm.getString("comments");
                                                commentatorName = comm.getString("commentatorName");

                                                comments.setCommentatorName(commentatorName);
                                                comments.setComments_data(comment_one);
                                                val.add(comments);
                                            }

                                            ArrayList<SERVICEDATA.POLLS> polll = new ArrayList<SERVICEDATA.POLLS>();
                                            if (data.has("question")) {
                                                question = data.getString("question");
                                                questionId = data.getString("questionId");
                                                JSONArray poll = data.getJSONArray("polls");

                                                for (int j = 0; j < poll.length(); j++) {
                                                    JSONObject pol = poll.getJSONObject(j);
                                                    SERVICEDATA.POLLS polls = new SERVICEDATA.POLLS();

                                                    option = pol.getString("option");
                                                    optiontype = pol.getString("optiontype");
                                                    vote = pol.getString("vote");

                                                    polls.setOption1(option);
                                                    polls.setOptiontype(optiontype);
                                                    polls.setVote_a(vote);
                                                    polll.add(polls);
                                                }
                                            }
                                            SERVICEDATA values = new SERVICEDATA();
//									values.setImages_standard_resolution(postImage);
                                            values.setVideolink(videoLink);
                                            values.setPostImage(postImage);
                                            values.setUsers_name(userName);
                                            values.setUsers_profile_pics(profilePic);
                                            values.setCreated_time(postDate);
                                            values.setPostDatestring(postDatestring);
                                            values.setType(type);
                                            values.setPost_id(post_id);
                                            values.setLikes(userhaslike);
                                            values.setPosttype(postType);
                                            values.setLikes_count(totalLike);
                                            values.setComments(val);
                                            values.setQuestion(question);
                                            values.setQuestionId(questionId);
                                            values.setPolls(polll);
                                            values.setAnswer(answer);
                                            values.setOptionTypeClicked("");
                                            values.setComments_count(Integer.parseInt(commentCounts));
                                            FinalData.add(values);
                                        }
                                    }

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                setListviewAdapter();
                            }

                            @Override
                            public void onError(VolleyError error) {
                                mCircularProgressView.setVisibility(View.GONE);
                                lvListView.setVisibility(View.VISIBLE);
                                // TODO Auto-generated method stub
                                System.out.println("e---->>" + "Error");
                                Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess_one(JSONObject response) {
                                mCircularProgressView.setVisibility(View.GONE);
                                lvListView.setVisibility(View.VISIBLE);
                                // TODO Auto-generated method stub
                                Toast.makeText(getActivity(), "onSuces_one" + response.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });
    }


    public void setListviewAdapter() {
        mCircularProgressView.setVisibility(View.GONE);
        lvListView.setVisibility(View.VISIBLE);
        Log.e("FinalData",FinalData.size()+"");
        if (!FinalData.isEmpty()) {
            if (adapter == null) {
                adapter = new Home_cell_Adapter(getActivity(), FinalData, this);
                lvListView.setAdapter(adapter);

                lvListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                        man.getInstance().dialogDismiss();
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });
                lvListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        man.getInstance().dialogDismiss();
                        updateItemAtPosition(position);
                        lvListView.smoothScrollToPosition(position);
                    }
                });
                adapter.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void updateItemAtPosition(int position) {
        int visiblePosition = lvListView.getFirstVisiblePosition();
        View view = lvListView.getChildAt(position - visiblePosition);
        lvListView.getAdapter().getView(position, view, lvListView);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        mCircularProgressView.setVisibility(View.VISIBLE);
        AllData.clear();
        FinalData.clear();
        loadDataFromTwitter();
        postDataOnServer();
        loadDataFromServer();
        itemListToJsonConvert(AllData);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void showLikedSnackbar() {
        Snackbar.make(clContent, "Liked!", Snackbar.LENGTH_SHORT).show();
    }



}
