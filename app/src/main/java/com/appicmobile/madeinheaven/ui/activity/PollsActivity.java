package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.Home_cell_Adapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.utils.CircularProgressDrawable;
import com.appicmobile.madeinheaven.ui.utils.CircularProgressView;
import com.facebook.internal.Utility;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;


public class PollsActivity extends Activity{

	
	TextView tvBackButton;
	private CircularProgressView mCircularProgressView;
	ListView lvListView;
	Home_cell_Adapter adapter;
	ArrayList<SERVICEDATA> FinalData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_polls);
		
		tvBackButton = (TextView)findViewById(R.id.tvBackButton);
		tvBackButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		getWidgetReferences();
		initialization();

		JSONObject jResult = new JSONObject();

		try {
			jResult.put("userId", Appconstant.USER_ID);
			jResult.put("weddingCode", Appconstant.WEDDING_CODE);
			jResult.put("getallpost",new JSONArray());
		} catch (JSONException e) {
			e.printStackTrace();
		}


		fetchAllPollsData(jResult);
	}

	private void fetchAllPollsData(JSONObject jResult) {
		Log.i("jResult", jResult.toString());
		RequestGenerator.makePostRequest(this, MadeInHeavenServiceURL.URL_MEDIA, jResult.toString(), false,
				new com.appicmobile.madeinheaven.ui.network.ResponseListener() {
					@Override
					public void onSuccess(String response) {
						try {
							getResponseFromServer(response);
						} catch (Exception e) {
							System.out.println("e---->>" + e.toString());
							Toast.makeText(PollsActivity.this, "toast_please_try_again", Toast.LENGTH_SHORT).show();
						}
					}

					private void getResponseFromServer(String serverResponse) {
						Log.i("serverResponse", serverResponse);

						FinalData.clear();
						org.json.JSONArray arrayData;
						JSONObject objectData;
						try {
							objectData = new JSONObject(serverResponse);

							if (objectData != null) {

								// String = null;
								// userid = objectData.getString("userId");

								arrayData = objectData.getJSONArray("getallpost");

								for (int i = 0; i < arrayData.length(); i++) {

									JSONObject data = arrayData.getJSONObject(i);

									String postDate = null;
									String post_id = null;
									String userName = null;
//									String postImage = null;
									String profilePic = null;
									String type = null;
									String userhaslike = null;
									String totalLike = null;
									String postType = null;
									String comment_one = null;
									String commentatorName = null;
									String commentCounts = "0";
									String postImg = null;
									String question = null;
									String option = null;
									String optiontype = null;
									String vote = null;
									String answer = null;
									String questionId = null;
									String videoLink = null;
									String postDatestring = null;


									post_id = data.getString("postId");
									profilePic = data.getString("profilePic");
									userName = data.getString("userName");
									videoLink = data.getString("videolink");
									List<String> postImage = new ArrayList<String>();
									org.json.JSONArray objVal = data.getJSONArray("postImg");
									for (int j = 0; j < objVal.length(); j++) {

										String im = objVal.getString(j).toString();
										postImage.add(im);
									}


									userhaslike = data.getString("userhaslike");
									totalLike = data.getString("totalLike");
									postType = data.getString("posttype");
									postDate = data.getString("postDate");
									postDatestring = data.getString("postDatestring");


									type = data.getString("type");

									if (data.has("answer")) {
//                                                if (!"".equals(data.getString("answer"))) {
										answer = data.getString("answer");
//                                                }
									}

									ArrayList<SERVICEDATA.POST> val = new ArrayList<SERVICEDATA.POST>();

									org.json.JSONArray comment_two = data.getJSONArray("comments");
									commentCounts = String.valueOf(comment_two.length());
									JsonObject jObj = new JsonObject();
									JsonArray cArray = new JsonArray();
									for (int j = 0; j < comment_two.length(); j++) {
										JSONObject comm = comment_two.getJSONObject(j);
										SERVICEDATA.POST comments = new SERVICEDATA.POST();

										comment_one = comm.getString("comments");
										commentatorName = comm.getString("commentatorName");

										comments.setCommentatorName(commentatorName);
										comments.setComments_data(comment_one);
										val.add(comments);
									}

									ArrayList<SERVICEDATA.POLLS> polll = new ArrayList<SERVICEDATA.POLLS>();
									if (data.has("question")) {
										question = data.getString("question");
										questionId = data.getString("questionId");
										org.json.JSONArray poll = data.getJSONArray("polls");

										for (int j = 0; j < poll.length(); j++) {
											JSONObject pol = poll.getJSONObject(j);
											SERVICEDATA.POLLS polls = new SERVICEDATA.POLLS();

											option = pol.getString("option");
											optiontype = pol.getString("optiontype");
											vote = pol.getString("vote");

											polls.setOption1(option);
											polls.setOptiontype(optiontype);
											polls.setVote_a(vote);
											polll.add(polls);
										}
									}
									SERVICEDATA values = new SERVICEDATA();
//									values.setImages_standard_resolution(postImage);
									values.setVideolink(videoLink);
									values.setPostImage(postImage);
									values.setUsers_name(userName);
									values.setUsers_profile_pics(profilePic);
									values.setCreated_time(postDate);
									values.setPostDatestring(postDatestring);
									values.setType(type);
									values.setPost_id(post_id);
									values.setLikes(userhaslike);
									values.setPosttype(postType);
									values.setLikes_count(totalLike);
									values.setComments(val);
									values.setQuestion(question);
									values.setQuestionId(questionId);
									values.setPolls(polll);
									values.setAnswer(answer);
									values.setOptionTypeClicked("");
									values.setComments_count(Integer.parseInt(commentCounts));

									if(data.has("question")){
										FinalData.add(values);
									}

								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						setListviewAdapter();
					}

					@Override
					public void onError(VolleyError error) {
						mCircularProgressView.setVisibility(View.GONE);
						lvListView.setVisibility(View.VISIBLE);
						// TODO Auto-generated method stub
						System.out.println("e---->>" + "Error");
						Toast.makeText(PollsActivity.this, "toast_please_try_again", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onSuccess_one(JSONObject response) {
						mCircularProgressView.setVisibility(View.GONE);
						lvListView.setVisibility(View.VISIBLE);
						// TODO Auto-generated method stub
						Toast.makeText(PollsActivity.this, "toast_please_try_again", Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void initialization() {
		FinalData = new ArrayList<SERVICEDATA>();
	}

	private void getWidgetReferences() {
		mCircularProgressView = (CircularProgressView) findViewById(R.id.progressBar);
		lvListView = (ListView) findViewById(R.id.lvListView);
	}


	public void setListviewAdapter() {
		mCircularProgressView.setVisibility(View.GONE);
		lvListView.setVisibility(View.VISIBLE);
		Log.e("FinalData",FinalData.size()+"");
		if (!FinalData.isEmpty()) {
			if (adapter == null) {
				adapter = new Home_cell_Adapter(PollsActivity.this, FinalData, null);
				lvListView.setAdapter(adapter);
			} else {
				adapter.notifyDataSetChanged();
			}
		}
	}


}
