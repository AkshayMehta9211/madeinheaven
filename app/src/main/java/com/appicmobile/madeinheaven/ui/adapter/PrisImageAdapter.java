package com.appicmobile.madeinheaven.ui.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.media.ExifInterface;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.CustomDrawable;
import com.appicmobile.madeinheaven.ui.common.DimensionUtils;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.tabs.BitmapUtil;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit on 9/10/2015.
 */
public class PrisImageAdapter extends RecyclerView.Adapter<PrisImageAdapter.ViewHolder> {

	private List<ImageData> imageDatas;
	private Activity mContext;
	private final int mMaxImageCount;
	private OnClickListener mClickListener;
	private List<ImageData> image = new ArrayList<ImageData>();
	private final int VIEW_ITEM = 1;
	private final int VIEW_ADD = 0;
	public PrisImageAdapter(Activity context, List<ImageData> imageDatas, int maxImageCount,
			OnClickListener onClickListener) {
		this.imageDatas = imageDatas;
		this.mContext = context;
		this.mMaxImageCount = maxImageCount;
		this.mClickListener = onClickListener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prescription_image, parent, false);
		v.setBackgroundDrawable(new CustomDrawable(mContext).setColor(R.color.white)
				.setRadius((int) mContext.getResources().getDimension(R.dimen.default_radius)).build());
		/*R.dimen.default_radius*/
		ViewHolder pvh = new ViewHolder(v);
		return pvh;
	}

	@Override
	public void onBindViewHolder(final ViewHolder ViewHolder, int i) {
		if (i == 0 && imageDatas.size() < mMaxImageCount) {
			ViewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_plus));
			Bitmap original = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_plus);
			int size = DimensionUtils.dpToPx(mContext, 40);
			Bitmap b = Bitmap.createScaledBitmap(original, size, size, false);
			Drawable d = new BitmapDrawable(mContext.getResources(), b);
            d.setColorFilter(ContextCompat.getColor(mContext, R.color.primary), PorterDuff.Mode.SRC_IN);
			ViewHolder.imageView.setImageDrawable(d);
			ViewHolder.imageButton.setVisibility(View.INVISIBLE);
			ViewHolder.imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mContext.finish();
				}
			});

			ViewHolder.progressBar.setVisibility(View.INVISIBLE);
		} else {
			final int position = imageDatas.size() < mMaxImageCount ? i - 1 : i;
			final ImageData imageData = imageDatas.get(position);
			ViewHolder.imageView.setTag(imageData.getImagePath());
			ViewHolder.itemView.setTag(imageData.getImagePath());
			ViewHolder.progressBar.setVisibility(View.VISIBLE);
			ViewHolder.imageView.setImageBitmap(null);

			image = Appconstant.image;

			if (imageData.getBitmap() != null) {

				ViewHolder.imageView.setImageBitmap(rotateBitmap(imageData));

            } else {

				new AsyncTask<Void, Void, Bitmap>() {
					@Override
					protected Bitmap doInBackground(Void... params) {
						return BitmapUtil.getScaledBitmap(imageData.getImagePath());
					}

					@Override
					protected void onPostExecute(Bitmap bitmap) {
						super.onPostExecute(bitmap);
						imageData.setBitmap(bitmap);
						if (ViewHolder.imageView.getTag().equals(imageData.getImagePath())) {
							ViewHolder.progressBar.setVisibility(View.INVISIBLE);
							ViewHolder.imageView.setImageBitmap(rotateBitmap(imageData));

                        }
					}
				}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}

//			ViewHolder.imageButton
//					.setBackgroundDrawable(new CustomDrawable(mContext).setColor(R.color.primary).setRadius(0).build());
            ViewHolder.imageButton.setImageResource(R.drawable.ic_remove);
			ViewHolder.imageButton.setVisibility(View.VISIBLE);
			// ViewHolder.imageView.setOnClickListener(null);
			ViewHolder.imageButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					imageDatas.remove(position);
					ArrayList<ImageData> val = new ArrayList<ImageData>();
					image.remove(position);
					val.addAll(image);
					if (val.size() != 0)
						Appconstant.image = val;
					else {
						Appconstant.image.clear();
						imageDatas.clear();
					}
					notifyDataSetChanged();
				}
			});
		}
	}

    private Bitmap rotateBitmap2(String imagePath) {
        Bitmap bitmap = null;
        try {
            File f = new File(imagePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                    null, options);
            bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                    outstudentstreamOutputStream);
        } catch (IOException e) {
            Log.w("TAG", "-- Error in setting image");
        } catch (OutOfMemoryError oom) {
            Log.w("TAG", "-- OOM Error in setting image");
        }
        return bitmap;
    }

    private Bitmap rotateBitmap(ImageData imageData) {
		if(imageData==null||imageData.getBitmap()==null){
			return null;
		}
		if(imageData.getRotation()==null){
			return imageData.getBitmap();
		}
		Matrix mat = new Matrix();
		mat.postRotate(imageData.getRotation());
		if(imageData.isFlip()){
			mat.preScale(1.0f, -1.0f);
		}
		Bitmap bMapRotate = Bitmap.createBitmap(imageData.getBitmap(), 0, 0, imageData.getBitmap().getWidth(), imageData.getBitmap().getHeight(), mat, true);

		return bMapRotate;
	}

	@Override
	public int getItemCount() {
		int size = imageDatas.size();
		return size < mMaxImageCount ? size + 1 : size;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		ImageView imageView;
		ImageButton imageButton;
		ProgressBar progressBar;

		ViewHolder(View itemView) {
			super(itemView);
			imageView = (ImageView) itemView.findViewById(R.id.imageView);
			imageButton = (ImageButton) itemView.findViewById(R.id.button);
			progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
		}
	}

}