package com.appicmobile.madeinheaven.ui.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashwini on 2/13/2016.
 */
public class Notification {

    @SerializedName("notiarray")
    @Expose
    private List<Notiarray> notiarray = new ArrayList<Notiarray>();

    /**
     *
     * @return
     * The notiarray
     */
    public List<Notiarray> getNotiarray() {
        return notiarray;
    }

    /**
     *
     * @param notiarray
     * The notiarray
     */
    public void setNotiarray(List<Notiarray> notiarray) {
        this.notiarray = notiarray;
    }


}
