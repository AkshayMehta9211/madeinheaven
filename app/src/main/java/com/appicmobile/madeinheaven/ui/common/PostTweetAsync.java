package com.appicmobile.madeinheaven.ui.common;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.appicmobile.madeinheaven.ui.activity.PostActivity;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.twitter.TwitterSession;

import java.io.File;
import java.util.ArrayList;

import twitter4j.PagableResponseList;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;


public class PostTweetAsync extends AsyncTask<Object, Void, Boolean> {

    @Override
    protected void onPostExecute(Boolean result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (result) {
            switch (option) {
                case 0:
                    Toast.makeText(mainActivity, "tweet_successful",
                            Toast.LENGTH_SHORT).show();
                    startNavDrawerActivity();
                    break;
                case 1:
                    Toast.makeText(mainActivity, "tweet_successful",
                            Toast.LENGTH_SHORT).show();
                    startNavDrawerActivity();
                    break;
                case 2:
                    Toast.makeText(mainActivity, "Message sent",
                            Toast.LENGTH_SHORT).show();
                    break;
                default:

                    break;
            }

        } else {
            Toast.makeText(mainActivity, "Tweet failed",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private PostActivity mainActivity;
    private int option;
    public static final int POST_TWEET = 0;
    public static final int POST_TWEET_WITH_IMAGE = 1;
    public static final int SEND_DIRECT_MESSAGE = 2;


    ArrayList<ImageData> Url = new ArrayList<ImageData>();

    @Override
    protected Boolean doInBackground(Object... params) {
        // TODO Auto-generated method stub
        try {
            mainActivity = (PostActivity) params[0];
            option = (int) params[1];
            String message = (String) params[2];

            // SharedPreferences sharedPreferences =
            // mainActivity.getSharedPreferences(ApplicationData.PREF_SOCIAL_NETWORK,
            // Context.MODE_PRIVATE);
            String Token = Appconstant.twitterAccessToken;
            String TokenSecret = Appconstant.twitterAccessTokenSecret;
            if (!(Token == null || Token.equals("") || TokenSecret == null || TokenSecret.equals(""))) {
                // Populate token and token_secret in consumer
                // provider.retrieveAccessToken(consumer, verifier);
                // TODO: you might want to store token and token_secret in you
                // app settings!


                TwitterSession session = new TwitterSession(mainActivity);

                AccessToken a = session.getAccessToken();

                // Initialize Twitter4J
                ConfigurationBuilder confbuilder = new ConfigurationBuilder();
                confbuilder.setOAuthAccessToken(a.getToken()).setOAuthAccessTokenSecret(a.getTokenSecret())
                        .setOAuthConsumerKey("Zi1MjXzh7BhBFd88yZA4VRZ2X")
                        .setOAuthConsumerSecret("bJzNj5tswL2WCPC77qINNEKWkLJke5DtkscsiCAlIn6pJbOAyh");
                final Twitter twitter = new TwitterFactory(confbuilder.build()).getInstance();
                StatusUpdate ad;
                switch (option) {
                    case POST_TWEET:
                        ad = new StatusUpdate(message);
                        twitter.updateStatus(ad);
                        return true;

                    case POST_TWEET_WITH_IMAGE:

                        Url = (ArrayList<ImageData>) params[3];

                        if (Url.size() > 0) {
                            for (int i = 0; i < Url.size(); i++) {
                                String image = Url.get(i).getImagePath();
                                ;

                                File file = new File(image);
                                ad = new StatusUpdate(message);
                                if (file != null) {
                                    if (file.exists()) {
                                        ad.setMedia(file);
                                    }
                                }
                                twitter.updateStatus(ad);
                            }
                        }


                        return true;

                    case SEND_DIRECT_MESSAGE:
                        ArrayList<User> followers = new ArrayList<User>();
                        long nextCursor = -1;
                        do {
                            PagableResponseList<User> usersResponse = twitter.getFollowersList(twitter.getScreenName(),
                                    nextCursor);
                            System.out.println("size() of first iteration:" + usersResponse.size());
                            nextCursor = usersResponse.getNextCursor();
                            followers.addAll(usersResponse);
                        } while (nextCursor > 0);

                        for (User user : followers) {
                            twitter.sendDirectMessage(user.getId(), message);
                        }
                        return true;

                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    private void startNavDrawerActivity() {
        Intent mainIntent = new Intent(mainActivity, NavDrawer.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mainActivity.startActivity(mainIntent);
        mainActivity.finish();
    }
}
