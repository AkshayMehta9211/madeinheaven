package com.appicmobile.madeinheaven.ui.network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.HomeActivity;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.appicmobile.madeinheaven.ui.uploadTab.UPLOADIMAGE;
import com.appicmobile.madeinheaven.ui.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ashwini on 3/1/2016.
 */
public class PostImageAsynctask extends AsyncTask<Void,Integer,ArrayList<UPLOADIMAGE.Image>> {


    private static final String TAG = PostImageAsynctask.class.getSimpleName();
    ArrayList<UPLOADIMAGE.Image> base64ImageList = new ArrayList<UPLOADIMAGE.Image>();
    private ArrayList<ImageData> mImageDatas = new ArrayList<>();
    private Context context;
    protected ProgressDialog progressDialog;
    protected static int COUNT;



    public PostImageAsynctask(ArrayList<ImageData> mImageDatas, Context context) {
        this.mImageDatas = mImageDatas;
        this.context = context;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        COUNT++;
    }

    @Override
    protected ArrayList<UPLOADIMAGE.Image> doInBackground(Void... params) {
        File file = null;

        for (int i = 0; i < mImageDatas.size(); i++) {
            String imagePath = mImageDatas.get(i).getImagePath().toString();
            String path = Utils.compressImage(imagePath, context);
            file = new File(path);

            // Step 1: Converting File to byte[]
            Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
//					int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            if(mImageDatas.get(i).getRotation()!=null)
                matrix.postRotate(90);
            Bitmap retVal = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            retVal.compress(Bitmap.CompressFormat.JPEG, 50, baos); // bm is the
            // bitmap
            byte[] b = baos.toByteArray();
            // Step 2 : converting byte[] to Base64 encoded String
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            UPLOADIMAGE.Image image = new UPLOADIMAGE.Image();
            image.setFileextension(".jpg");
            image.setPath(encodedImage);

            base64ImageList.add(image);
        }
        return base64ImageList;
      //  return null;
    }


    /**
     * @param context
     * @param title
     * @param message
     */
    private void generateNotification(Context context, String title, String message) {
//        Intent notificationIntent = null;
//        long when = System.currentTimeMillis();
//        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationIntent = new Intent(context, HomeActivity.class);
//        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 111, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        mbuilder = new NotificationCompat.Builder(context);
//        mbuilder.setContentText(message);
//        mbuilder.setContentIntent(pendingIntent);
//        mbuilder.setContentTitle(title);
//        mbuilder.setSmallIcon(R.mipmap.ic_launcher);
//        mbuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
//        mbuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        mNotification = mbuilder.build();
//        mNotification.when = when;
//        mNotification.flags |= Notification.FLAG_ONGOING_EVENT;
//        mNotificationManager.notify(111, mNotification);


    }


}
