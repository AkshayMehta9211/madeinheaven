package com.appicmobile.madeinheaven.ui.common;

import java.io.Serializable;

public class CommentsDo implements Serializable {
	public String comm_Name;
	public String comm_data;

	/**
	 * @return the comm_Name
	 */
	public String getComm_Name() {
		return comm_Name;
	}

	/**
	 * @param comm_Name
	 *            the comm_Name to set
	 */
	public void setComm_Name(String comm_Name) {
		this.comm_Name = comm_Name;
	}

	/**
	 * @return the comm_data
	 */
	public String getComm_data() {
		return comm_data;
	}

	/**
	 * @param comm_data
	 *            the comm_data to set
	 */
	public void setComm_data(String comm_data) {
		this.comm_data = comm_data;
	}
}
