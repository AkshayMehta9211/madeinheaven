package com.appicmobile.madeinheaven.ui.pojo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.appicmobile.madeinheaven.ui.activity.Loginframe;

/**
 * Created by Ankit on 9/10/2015.
 */
public class UserRegister {

    private int id;
    private boolean islogin;
    private String email_id;
    private String status;
    private String msg;
    private String weddingCode;
    private String hashtag;
    private String imageUrl;
    private String userName;
    private String userType;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the hashtag
     */
    public String getHashtag() {
        return hashtag;
    }

    /**
     * @param hashtag the hashtag to set
     */
    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the weddingCode
     */
    public String getWeddingCode() {
        return weddingCode;
    }

    /**
     * @param weddingCode the weddingCode to set
     */
    public void setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the islogin
     */
    public boolean isIslogin() {
        return islogin;
    }

    /**
     * @param islogin the islogin to set
     */
    public void setIslogin(boolean islogin) {
        this.islogin = islogin;
    }

    /**
     * @return the email_id
     */
    public String getEmail_id() {
        return email_id;
    }

    /**
     * @param email_id the email_id to set
     */
    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }


    private static SharedPreferences getSharedPreferences(Context context) {
        return context == null ? null : context.getSharedPreferences("UserRegister", Context.MODE_PRIVATE);
    }

    public static UserRegister getUser(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if (sharedPreferences == null) {
            return null;
        }

        UserRegister userRegister = new UserRegister();
        userRegister.setId(sharedPreferences.getInt("userRegister.id", 0));
        userRegister.setIslogin(sharedPreferences.getBoolean("userRegister.islogin", false));
        userRegister.setStatus(sharedPreferences.getString("userRegister.status", ""));
        userRegister.setEmail_id(sharedPreferences.getString("userRegister.email_id", ""));

        return userRegister;
    }

    public static boolean saveUser(Context context, UserRegister userRegister) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if (sharedPreferences == null) {
            return false;
        }

        return sharedPreferences.edit()
                .putInt("userRegister.id", userRegister.getId())
                .putString("userRegister.email_id", userRegister.getEmail_id())
                .putString("userRegister.status", userRegister.getStatus())
                .putBoolean("userRegister.islogin", userRegister.isIslogin())
                .commit();
    }

    public static boolean clear(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if (sharedPreferences == null) {
            return false;
        }
        return sharedPreferences.edit().clear().commit();
    }

    public static boolean isValidUser(UserRegister userRegister) {
        return !(userRegister == null || userRegister.getId() <= 0 || TextUtils.isEmpty(userRegister.getEmail_id()));
    }

    public static void signOut(Activity activity) {
        UserRegister.clear(activity);
        Intent intent = new Intent(activity, Loginframe.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

//	    public static boolean saveAddress(Activity activity, ArrayList<Address> addressArrayList) {
//	        SharedPreferences sharedPreferences = getSharedPreferences(activity);
//	        if (addressArrayList != null && sharedPreferences != null) {
//	            Gson gson = new Gson();
//	            return sharedPreferences.edit()
//	                    .putString(ChangeAddressActivity.TAG_ADDRESS, gson.toJson(addressArrayList))
//	                    .commit();
//	        }
//	        return false;
//	    }

//	    public static ArrayList<Address> getAddress(Activity activity) {
//	        SharedPreferences sharedPreferences = getSharedPreferences(activity);
//	        if (sharedPreferences != null) {
//	            String addressString = sharedPreferences.getString(ChangeAddressActivity.TAG_ADDRESS, null);
//	            if (addressString != null) {
//	                try {
//	                    Gson gson = new Gson();
//	                    JSONArray jsonArray = new JSONArray(addressString);
//	                    ArrayList<Address> addressArrayList = new ArrayList<>(jsonArray.length());
//	                    for (int i = 0; i < jsonArray.length(); i++) {
//	                        String jsonString = jsonArray.get(i).toString();
//	                        Address address = gson.fromJson(jsonString, Address.class);
//	                        addressArrayList.add(address);
//	                    }
//	                    return addressArrayList;
//	                } catch (JSONException e) {
//	                }
//	            }
//	        }
//	        return new ArrayList<>();
//	    }

	  /*  public static boolean saveRecentAddress(Activity activity, Address address) {
            SharedPreferences sharedPreferences = getSharedPreferences(activity);
	        if (address != null && sharedPreferences != null) {
	            Gson gson = new Gson();
	            return sharedPreferences.edit()
	                    .putString(ChangeAddressActivity.TAG_RECENT_ADDRESS, gson.toJson(address))
	                    .commit();
	        }
	        return false;
	    }

	    public static Address getRecentAddress(Context context) {
	        SharedPreferences sharedPreferences = getSharedPreferences(context);
	        if (sharedPreferences != null) {
	            String addressString = sharedPreferences.getString(ChangeAddressActivity.TAG_RECENT_ADDRESS, null);
	            
	            try{
	            	
	            	if (addressString != null) {
	            		try {
	            			Gson gson = new Gson();
	            			return gson.fromJson(addressString,Address.class);
	            		} catch (Exception e) {
	            			e.printStackTrace();
	            		}
	            	}
	            
	            }
	            catch(Exception e){
	            	e.printStackTrace();
	            }
	        }
	        return null;
	    }

	}*/


}
