package com.appicmobile.madeinheaven.ui.tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.NotificationActivity;
import com.appicmobile.madeinheaven.ui.adapter.ChatDataAdpater;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.CHATDATADO;
import com.appicmobile.madeinheaven.ui.pojo.CHATDO;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends Fragment {
    Toolbar toolbar;
    TextView toolbar_title;
    ArrayList<CHATDATADO.DATA> event = new ArrayList<CHATDATADO.DATA>();

    List<CHATDATADO> chat = new ArrayList<CHATDATADO>();
    ListView lvChatList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.chat_layout, container, false);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ((NavDrawer) getActivity()).setActionBarTitle("CHAT");

        chatUserListfromSerive();
        FeedContextMenuManager man = new FeedContextMenuManager();
        man.getInstance().dialogDismiss();

        lvChatList = (ListView) rootview.findViewById(R.id.lvChatList);
//		toolbar = (Toolbar)rootview.findViewById(R.id.toolbar);
        /*toolbar_title = (TextView)rootview.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Chat");*/
//    	toolbar.setBackgroundColor(R.color.purple);
       /* activity.getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_drawer);*/
        // Set the drawer toggle as the DrawerListener

//		mDrawerToggle.setDrawerIndicatorEnabled(true);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //for crate home button

//        activity.setSupportActionBar(toolbar);
      /*  activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        return rootview;
    }

    private void chatUserListfromSerive() {
        CHATDO chatdo = new CHATDO();
        chatdo.setWeddingCode(Appconstant.WEDDING_CODE);
        chatdo.setUserId(String.valueOf(Appconstant.USER_ID));
        final Gson gson = new Gson();
        String jsonString = gson.toJson(chatdo);
        RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_CHAT_USER_LIST, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {

                        try {
                            JSONObject objectData = new JSONObject(response);

                            if (objectData != null) {
                                JSONObject jsonResponse = new JSONObject();
                                JSONArray arrData = objectData.getJSONArray("list");
                                for (int i = 0; i < arrData.length(); i++) {

                                    String userId = null;
                                    String userName = null;
                                    String imageUrl = null;
                                    String manager = null;

                                    JSONObject data = arrData.getJSONObject(i);
                                    userId = data.getString("userId");
                                    userName = data.getString("userName");
                                    imageUrl = data.getString("imageUrl");
                                    manager = data.getString("manager");

                                    CHATDATADO list = new CHATDATADO();

                                    CHATDATADO.DATA chatdatado = new CHATDATADO.DATA();
                                    chatdatado.setUserId(userId);
                                    chatdatado.setImageUrl(imageUrl);
                                    chatdatado.setManager(manager);
                                    chatdatado.setUserName(userName);
                                    event.add(chatdatado);
                                    list.setList(event);
                                    chat.add(list);
                                    setAdapterData();
                                }
                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
//                        Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub

                        Toast.makeText(getActivity(), "successOne", Toast.LENGTH_SHORT).show();
                        try {


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


    }

    private void setAdapterData() {
        ChatDataAdpater chatAdapter = new ChatDataAdpater(getActivity(), chat);
        lvChatList.setAdapter(chatAdapter);

   /*     lvChatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CHATDATADO chatdatado = (CHATDATADO) parent.getItemAtPosition(position);
                Intent i = new Intent(getActivity(), UserChatMessage.class);
                i.putExtra(Appconstant.RECEIVER_ID, chatdatado.getList().get(position).getUserId());
                startActivity(i);
            }
        });*/
    }


    public static Fragment newInstance() {
        ChatActivity activeFragment = new ChatActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.gallery_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.gallery_notification:
                Intent i = new Intent(getActivity(), NotificationActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }

        return false;
    }
}
