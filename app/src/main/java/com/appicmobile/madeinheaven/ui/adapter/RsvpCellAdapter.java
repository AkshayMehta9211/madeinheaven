package com.appicmobile.madeinheaven.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.EVENTDATA;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class RsvpCellAdapter extends BaseAdapter{

	Context context;
	private Activity act;
	private  ArrayList<EVENTDATA> allData;
	ImageLoader imageLoader;
	EVENTDATA result;
	 ViewHolderItem viewHolder;
//	 NavDrawer act;




	public RsvpCellAdapter( Activity act, ArrayList<EVENTDATA> allData) {

		this.act= act;
		this.allData = allData;
	}

	@Override
	public int getCount() {
		if(allData != null && allData.size()>=0)
			return allData.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return allData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
//		 ViewHolderItem viewHolder;
		
		 result =allData.get(position);
		 Log.i("resultData", result.toString());
		
		 
	    if(convertView==null){
	    	convertView = LayoutInflater.from(act).inflate(R.layout.rsvp_events_layout, null);
	        // well set up the ViewHolder
	        viewHolder = new ViewHolderItem();
	        viewHolder.tvEventTitle 	= (TextView)convertView.findViewById(R.id.tvEventTitle);
	        viewHolder.tvTimeBetween 	= (TextView)convertView.findViewById(R.id.tvTimeBetween);
	        viewHolder.tvMonthDay	= (TextView)convertView.findViewById(R.id.tvMonthDay);
	        viewHolder.tvYear  	= (TextView)convertView.findViewById(R.id.tvYear);
	        viewHolder.ivNext = (ImageView)convertView.findViewById(R.id.ivNext);
	        viewHolder.tvDay = (TextView)convertView.findViewById(R.id.tvDay);
			viewHolder.ivCloseEvent =(ImageView)convertView.findViewById(R.id.ivCloseEvent);
			viewHolder.ivCheckEvent =(ImageView)convertView.findViewById(R.id.ivCheckEvent);
			viewHolder.ivDependEvent =(ImageView)convertView.findViewById(R.id.ivDependEvent);
	        convertView.setTag(viewHolder);
	         
	    }else{
	        viewHolder = (ViewHolderItem) convertView.getTag();
	    }
	        viewHolder.tvEventTitle.setText(allData.get(position).getEvent_tittle());
	        viewHolder.tvDay.setText(allData.get(position).getEvent_day());
	        viewHolder.tvTimeBetween.setText(allData.get(position).getEvent_start_time()+" to " + result.getEvent_end_time());
	        viewHolder.tvMonthDay.setText(allData.get(position).getEvent_months()+ allData.get(position).getEvent_date());
	        viewHolder.tvYear.setText(allData.get(position).getEvent_year());
	        viewHolder.ivNext.setTag(position);

			String rsvp_option = result.getRsvp_option();

		switch (rsvp_option){
			case "yes":
				viewHolder.ivCheckEvent.setVisibility(View.VISIBLE);
				viewHolder.ivDependEvent.setVisibility(View.GONE);
				viewHolder.ivCloseEvent.setVisibility(View.GONE);
				break;
			case "no":
				viewHolder.ivCloseEvent.setVisibility(View.VISIBLE);
				viewHolder.ivCheckEvent.setVisibility(View.GONE);
				viewHolder.ivDependEvent.setVisibility(View.GONE);
				break;
			case "maybe":
				viewHolder.ivDependEvent.setVisibility(View.VISIBLE);
				viewHolder.ivCloseEvent.setVisibility(View.GONE);
				viewHolder.ivCheckEvent.setVisibility(View.GONE);
				break;

			default:
				break;
		}



	    return convertView;
	}
	
	public class ViewHolderItem {
		TextView tvEventTitle;
		TextView tvTimeBetween;
		TextView tvMonthDay;
		TextView tvYear;
		TextView tvDay;
		ImageView ivNext;
		ImageView ivCloseEvent;
		ImageView ivCheckEvent;
		ImageView ivDependEvent;
		
	}

	



}
