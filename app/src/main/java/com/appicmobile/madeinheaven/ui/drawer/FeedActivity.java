package com.appicmobile.madeinheaven.ui.drawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.appicmobile.madeinheaven.R;

public class FeedActivity extends Fragment{
	
	private ListView lvListView;
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootview = inflater.inflate(R.layout.activity_home, container, false);
		
		lvListView = (ListView) rootview.findViewById(R.id.lvListView);
		
		
//		GalleryGridViewAdapter gridadapter = new GalleryGridViewAdapter(getActivity(), imageId);
//		gridview.setAdapter(gridadapter);

		/*
		 * Intent cameraIntent = new
		 * Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		 * startActivityForResult(cameraIntent, CAMERA_REQUEST);
		 */
		return rootview;
	}
	
	public static Fragment newInstance() {
		FeedActivity activeFragment = new FeedActivity();
		Bundle bundle = new Bundle();
		activeFragment.setArguments(bundle);
		return activeFragment;
	}

}
