package com.appicmobile.madeinheaven.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.util.StringUtil;
import com.appicmobile.madeinheaven.ui.pojo.Message;
import com.appicmobile.madeinheaven.ui.utils.DateTimeUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by himanshusoni on 06/09/15.
 */
public class ChatMessageAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int MY_MESSAGE = 0, OTHER_MESSAGE = 1, HEADER = 2;
    Context context;
    private ArrayList<Message> messages = new ArrayList<>();

    public ChatMessageAdapter(Context context, ArrayList<Message> data) {
        this.context = context;
        this.messages = getFormattedList(data);
    }

   /* @Override
    public int getViewTypeCount() {
        // my message, other message, my image, other image
        return 3;
    }*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case HEADER:
                return new ViewHolderHeader(LayoutInflater.from(context).inflate(R.layout.list_item_chat_header, viewGroup, false));
            case MY_MESSAGE:
                return new ViewHolderItem(LayoutInflater.from(context).inflate(R.layout.item_mine_message, viewGroup, false));
            case OTHER_MESSAGE:
                return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_other_message, viewGroup, false));

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message item = messages.get(position);
        if (holder.getItemViewType() == MY_MESSAGE) {
            ViewHolderItem mholder = (ViewHolderItem) holder;
            mholder.msg.setText(item.getMessage());
            mholder.textTime.setText(DateTimeUtils.formateDate(item.getDate()));
            if(!item.getReceiver().getImageUrl().isEmpty()){
                Picasso.with(context).load(item.getReceiver().getImageUrl()).into(mholder.imageView);
            }

        } else if (holder.getItemViewType() == OTHER_MESSAGE) {
            ViewHolder mholder = (ViewHolder) holder;
            mholder.msg.setText(item.getMessage());
            mholder.textTime.setText(DateTimeUtils.formateDate(item.getDate()));
            if(!item.getReceiver().getImageUrl().isEmpty()){
                Picasso.with(context).load(item.getReceiver().getImageUrl()).into(mholder.imageView);
            }

        } else if (holder.getItemViewType() == HEADER) {
            ViewHolderHeader mholder = (ViewHolderHeader) holder;
            mholder.msg.setText(item.getDate());
        }
    }

    @Override
    public int getItemViewType(int position) {
        Message item = messages.get(position);
        if (item.getReceiverId() == null) {
            return HEADER;
        } else {
            String receiverType = item.getReceiver().getType();
            switch (receiverType) {
                case "receiver":
                    return MY_MESSAGE;
                case "sender":
                    return OTHER_MESSAGE;
                default:
                    return 0;
            }
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }


    private ArrayList<Message> getFormattedList(ArrayList<Message> dataSet) {
        Date prevDate = null;
        ArrayList<Message> messages = new ArrayList<>();
        for (Message message : dataSet) {
            Date currentDate = DateTimeUtils.getDateFromString(message.getDate(), "dd/MM/yyyy");
            if (prevDate == null || !currentDate.equals(prevDate)) {
                Message message1 = new Message();
                message1.setDate(DateTimeUtils.getStringFromDate(currentDate, "dd MMM,yyyy"));
                messages.add(message1);
            }
            prevDate = currentDate;
            messages.add(message);
        }
        return messages;
    }

    public void addItemsToList(ArrayList<Message> message) {
        messages.clear();
        messages.addAll(getFormattedList(message));
    }

    public static class ViewHolderItem extends RecyclerView.ViewHolder {
        TextView msg;
        ImageView imageView;
        TextView textTime;

        public ViewHolderItem(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
            imageView = (ImageView) v.findViewById(R.id.ivUserPics);
            textTime = (TextView) v.findViewById(R.id.textTime);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView msg;
        ImageView imageView;
        TextView textTime;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
            imageView = (ImageView) v.findViewById(R.id.ivUserPics);
            textTime = (TextView) v.findViewById(R.id.textTime);
        }


    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView msg;


        public ViewHolderHeader(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            msg = (TextView) v.findViewById(R.id.text);
        }
    }
}
