package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.ForgotDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ForgotPasswordActivity extends Fragment implements OnClickListener{
	
	private EditText etForgetPassword;
	private TextView tvCancel,tvErrorMessage,tvMessage;
	private Button btnSubmit;
	private String forgetPassword;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootview=inflater.inflate(R.layout.forgot_password_layout,container,false);
		
		etForgetPassword = (EditText)rootview.findViewById(R.id.etFogotPassword);
		tvCancel = (TextView)rootview.findViewById(R.id.tvCancel);
		btnSubmit = (Button)rootview.findViewById(R.id.btnSubmit);
		tvErrorMessage= (TextView)rootview.findViewById(R.id.tvErrorMessage);
		tvMessage= (TextView)rootview.findViewById(R.id.tvMessage);
		tvCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		
		
		return rootview;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSubmit:
			
			goToSubmit();
			break;
		case R.id.tvCancel:
			goToNext();
			break;

		default:
			break;
		}
		
	}

	private void goToSubmit() {
		
		tvErrorMessage.setVisibility(View.GONE);
		tvMessage.setVisibility(View.GONE);

		forgetPassword = etForgetPassword.getText().toString().trim();

			if (forgetPassword.isEmpty())
				Toast.makeText(getActivity(), "Please enter phone number.", Toast.LENGTH_SHORT).show();
			else {
				// hideKeyBoard(this);
				ForgotDo forgot = new ForgotDo();
				forgot.email_id = forgetPassword;
				final Gson gson = new Gson();
				String jsonString = gson.toJson(forgot);
				RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_USER_POST, jsonString, true,
						new ResponseListener() {
							@Override
							public void onSuccess(String response) {
								try {
									ForgotUser forgetPassword = gson.fromJson(response, ForgotUser.class);
									if (forgetPassword.getStatus().contains("true")) {
										tvErrorMessage.setVisibility(View.GONE);
										tvMessage.setVisibility(View.VISIBLE);
										Toast.makeText(getActivity(), forgetPassword.getMsg(), Toast.LENGTH_SHORT).show();

									} else {
										tvMessage.setVisibility(View.GONE);
										tvErrorMessage.setVisibility(View.VISIBLE);
										Toast.makeText(getActivity(), forgetPassword.getMsg(), Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
									e.printStackTrace();
								}
							}

							@Override
							public void onError(VolleyError error) {
								Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
							}

							@Override
							public void onSuccess_one(JSONObject response) {
								// TODO Auto-generated method stub
							}

						});
			}
		}
		
	

	private void goToNext() {
		Fragment loginframe=new Loginframe();
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, loginframe);
        fragmentTransaction.commit();
	}
}
