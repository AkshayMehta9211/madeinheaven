package com.appicmobile.madeinheaven.ui.tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.CustomVideoActivity;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.multipleimages.MultiImageSelectorActivity;
import com.appicmobile.madeinheaven.ui.rsvpevent.RSVPActivity;
import com.appicmobile.madeinheaven.ui.uploadTab.PhotosActivity;
import com.appicmobile.madeinheaven.ui.uploadTab.SurveyActivity;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;

import java.util.List;

public class UploadActivity extends Fragment implements OnClickListener {

	private Button marriage, gallery, homepage, event, chat;
	private TextView photo, video, library, survey;
	public FragmentTransaction ft;
	public Fragment tabfragment;
	private FragmentTabHost mTabHost;

	TextView toolbar_title;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		final View rootview = inflater.inflate(R.layout.upload_layout, container,false);
		AppCompatActivity activity = (AppCompatActivity) getActivity();

		((NavDrawer)getActivity()).setActionBarTitle("Library");

		FeedContextMenuManager man = new FeedContextMenuManager();
		man.getInstance().dialogDismiss();
//		toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
		/*toolbar_title = (TextView) rootview.findViewById(R.id.toolbar_title);
		toolbar_title.setText("Library");*/
		// toolbar.setBackgroundColor(R.color.purple);
		activity.getSupportActionBar().setHomeButtonEnabled(true);
//		toolbar.setNavigationIcon(R.drawable.ic_drawer);

		// Set the drawer toggle as the DrawerListener

		// mDrawerToggle.setDrawerIndicatorEnabled(true);
		// getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// for crate home button
//		activity.setSupportActionBar(toolbar);
		/*activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

		mTabHost = (FragmentTabHost)rootview.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.container);
		mTabHost.addTab(mTabHost.newTabSpec("PHOTO").setIndicator("PHOTO"), PhotosActivity.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("Video").setIndicator("VIDEO"), CustomVideoActivity.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("LIBRARY").setIndicator("LIBRARY"), MultiImageSelectorActivity.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("POLLS").setIndicator("POLLS"), SurveyActivity.class, null);
        mTabHost.setCurrentTab(2);

		/*mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.color.purple);
		mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.color.purple);
		mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.color.purple);
		mTabHost.getTabWidget().getChildAt(3).setBackgroundResource(R.color.purple);*/
		for (int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
        {
            TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#ffffff"));
			//tv.setTextSize(getResources().getDimension(R.dimen.text_size_small));
			mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.purple_dark));
        }
		mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				.setBackgroundColor(getResources().getColor(R.color.purple_dark));
		marriage = (Button) rootview.findViewById(R.id.ibMarriage);
		gallery = (Button) rootview.findViewById(R.id.ibGallery);
		homepage = (Button) rootview.findViewById(R.id.ibHome);
		event = (Button) rootview.findViewById(R.id.ibEvent);
		chat = (Button) rootview.findViewById(R.id.ibChat);
		setOnClickListner();
		return rootview;
	}
    
    private void setOnClickListner() {
		// TODO Auto-generated method stub
	}
    
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            Activity a = getActivity();
            if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ibMarriage:
			displayView(0, marriage);
			break;
		case R.id.ibGallery:
			displayView(1, gallery);
			break;
		case R.id.ibHome:
			displayView(2, homepage);
			break;
		case R.id.ibEvent:
			displayView(3, event);
			break;
		case R.id.ibChat:
			displayView(4, chat);
			break;
		}
	}

	private void displayView(int index, Button marriage2) {
		Fragment fragment = null;
		switch (index) {
		case 0:
			fragment = NavDrawer.newInstance();
			setActiveTab(marriage2, marriage);
			break;
		case 1:
			/*fragment = GalleryActivity.newInstance();
			setActiveTab(marriage2, gallery);
			break;*/
		case 2:
			fragment = UploadActivity.newInstance();
			setActiveTab(marriage2, homepage);
			break;
		case 3:
			fragment = RSVPActivity.newInstance();
			setActiveTab(marriage2, event);
			break;
		case 4:
			fragment = ChatActivity.newInstance();
			setActiveTab(marriage2, chat);
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void setActiveTab(Button activeTab, Button inactiveTab1) {
		// TODO Auto-generated method stub

		activeTab.setBackgroundDrawable(ContextCompat.getDrawable(
				getActivity(), R.drawable.bottomtab_press));
		inactiveTab1.setBackgroundDrawable(ContextCompat.getDrawable(
				getActivity(), R.drawable.bottom_tab));

	}

	public static Fragment newInstance() {
		UploadActivity activeFragment = new UploadActivity();
		Bundle bundle = new Bundle();
		activeFragment.setArguments(bundle);
		return activeFragment;
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


}
