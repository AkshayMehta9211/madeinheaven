package com.appicmobile.madeinheaven.ui.drawer;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Ashwini on 2/11/2016.
 */
public class PopupDialogFragment extends DialogFragment implements View.OnClickListener {

    private TextView tvMessage;
    private ImageView ivMessage;
    private Button btnClose;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dialog.getWindow().setAttributes(lp);
        setCancelable(false);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.first_visit_popup, container, false);
        tvMessage = (TextView) rootView.findViewById(R.id.tvMessage);
        ivMessage = (ImageView) rootView.findViewById(R.id.ivMessage);
        btnClose = (Button) rootView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        String message = getArguments().getString("message");
        String imageLink = getArguments().getString("imagelink");
        tvMessage.setText(message);
        if (!TextUtils.isEmpty(imageLink)) {
            Picasso.with(getActivity()).load(imageLink).into(ivMessage);
        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
