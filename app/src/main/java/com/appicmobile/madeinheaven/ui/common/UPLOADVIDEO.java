package com.appicmobile.madeinheaven.ui.common;

public class UPLOADVIDEO {
	
	private long userId;
	private Boolean status;
	private String weddingCode;
	private String fileextension;
	private String path;
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	/**
	 * @return the weddingCode
	 */
	public String getWeddingCode() {
		return weddingCode;
	}
	/**
	 * @param weddingCode the weddingCode to set
	 */
	public void setWeddingCode(String weddingCode) {
		this.weddingCode = weddingCode;
	}
	/**
	 * @return the fileextension
	 */
	public String getFileextension() {
		return fileextension;
	}
	/**
	 * @param fileextension the fileextension to set
	 */
	public void setFileextension(String fileextension) {
		this.fileextension = fileextension;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
