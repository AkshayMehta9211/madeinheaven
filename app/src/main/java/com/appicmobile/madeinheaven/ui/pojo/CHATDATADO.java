package com.appicmobile.madeinheaven.ui.pojo;

import java.util.List;

/**
 * Created by Ashvini on 09-Jan-16.
 */
public class CHATDATADO {
    public List<DATA> list;


    public List<DATA> getList() {
        return list;
    }

    public void setList(List<DATA> list) {
        this.list = list;
    }

    public static class DATA {
        public String userId;
        public String userName ;
        public String imageUrl ;
        public String manager;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getManager() {
            return manager;
        }

        public void setManager(String manager) {
            this.manager = manager;
        }
    }
}
