package com.appicmobile.madeinheaven.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.UPLOADVIDEO;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.uploadTab.UPLOADIMAGE;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * Created by Ashwini on 2/10/2016.
 */
public class CustomVideoActivity extends Fragment {

    private final static int CAMERA_RQ = 6969;
    private final static int PERMISSION_RQ = 84;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.activity_custom_video, container, false);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Request permission to save videos in external storage
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_RQ);
        }
        File saveDir = null;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // Only use external storage directory if permission is granted, otherwise cache directory is used by default
            saveDir = new File(Environment.getExternalStorageDirectory(), "MadeInHeaven");
            saveDir.mkdirs();
        }

        new MaterialCamera(getActivity())
                .saveDir(saveDir)
                .showPortraitWarning(false)
                .allowRetry(true)
                .countdownSeconds(15f)
                .defaultToFrontFacing(false)
                .start(CAMERA_RQ);
        return rootview;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private String readableFileSize(long size) {
        if (size <= 0) return size + " B";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private String fileSize(File file) {
        return readableFileSize(file.length());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Received recording or error from MaterialCamera
        if (requestCode == CAMERA_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                final File file = new File(data.getData().getPath());
                String Base64File=encodeVideoFile(file);
                String fileExt=getFileExt(file.getAbsolutePath());
                uploadImageToServer(Base64File,fileExt);
                Toast.makeText(getActivity(), String.format("Saved to: %s, size: %s",
                        file.getAbsolutePath(), fileSize(file)), Toast.LENGTH_LONG).show();
            } else if (data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                if (e != null) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            // Sample was denied WRITE_EXTERNAL_STORAGE permission
            Toast.makeText(getActivity(), "Videos will be saved in a cache directory instead of an external storage directory since permission was denied.", Toast.LENGTH_LONG).show();
        }
    }

    public String encodeVideoFile(File file){
        //File tempFile = new File(path);
        String encodedString = null;

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (Exception e) {
            // TODO: handle exception
        }
        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bytes = output.toByteArray();
        encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
        return encodedString;
    }




    public static String getFileExt(String fileName) {
        return fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
    }


    private void uploadImageToServer(String encodedImage,String ext) {
        UPLOADVIDEO uploadVideo = new UPLOADVIDEO();
        uploadVideo.setUserId(Appconstant.USER_ID);
        uploadVideo.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));
        uploadVideo.setFileextension("."+ext);
        uploadVideo.setPath(encodedImage);

        final Gson gson = new Gson();
        final String jsonString = gson.toJson(uploadVideo);

        RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_POST_VIDEO, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            UPLOADIMAGE prescription1 = gson.fromJson(response, UPLOADIMAGE.class);
                            if (prescription1.getStatus() == true) {
                                Toast.makeText(getActivity(), "uploaded successfull", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(getActivity(), "uploaded not successfull", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getActivity(), "JSONObject response_try_again", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void onBackPressed() {
        Intent mainIntent = new Intent(getActivity(),NavDrawer.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        /*Intent i = new Intent(getActivity(), NavDrawer.class);
        startActivity(i);*/
    }


}