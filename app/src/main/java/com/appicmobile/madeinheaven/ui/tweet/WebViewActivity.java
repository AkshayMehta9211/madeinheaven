package com.appicmobile.madeinheaven.ui.tweet;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.appicmobile.madeinheaven.R;

public class WebViewActivity extends Activity {
	
	private WebView webView;
	
	public static String EXTRA_URL = "https://api.twitter.com/oauth2/token";

	private String consumerKey = "Zi1MjXzh7BhBFd88yZA4VRZ2X";
	private String consumerSecret = "bJzNj5tswL2WCPC77qINNEKWkLJke5DtkscsiCAlIn6pJbOAyh";
	private String callbackUrl = "http://appicmobile.com";
	private String oAuthVerifier = "https://api.twitter.com/oauth2/token";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_webview);
		
		setTitle("Login");

		final String url = this.getIntent().getStringExtra(EXTRA_URL);
		if (null == url) {
			Log.e("Twitter", "URL cannot be null");
			finish();
		}

		webView = (WebView) findViewById(R.id.webView);
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl(url);
	}


	class MyWebViewClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			if (url.contains(callbackUrl)) {
				Uri uri = Uri.parse(url);
//				http://appicmobile.com/?oauth_token=nIr-tgAAAAAAiwf1AAABUZESpNA&oauth_verifier=tcIs0Lf6d7CZ4rcrsIjfCoiyQMrbpvJX
				/* Sending results back */
				String verifier = uri.getQueryParameter(oAuthVerifier);
				Intent resultIntent = new Intent();
				resultIntent.putExtra(oAuthVerifier, verifier);
				setResult(RESULT_OK, resultIntent);
				
				/* closing webview */
				finish();
				return true;
			}
			return false;
		}
	}

}
