package com.appicmobile.madeinheaven.ui.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashwini on 2/8/2016.
 */
public class ChatMessage {

    @SerializedName("weddingCode")
    @Expose
    private int weddingCode;
    @SerializedName("senderId")
    @Expose
    private String senderId;
    @SerializedName("receiverId")
    @Expose
    private String receiverId;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     *
     * @return
     * The weddingCode
     */
    public int getWeddingCode() {
        return weddingCode;
    }

    /**
     *
     * @param weddingCode
     * The weddingCode
     */
    public void setWeddingCode(int weddingCode) {
        this.weddingCode = weddingCode;
    }

    /**
     *
     * @return
     * The senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     *
     * @param senderId
     * The senderId
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     *
     * @return
     * The receiverId
     */
    public String getReceiverId() {
        return receiverId;
    }

    /**
     *
     * @param receiverId
     * The receiverId
     */
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}