package com.appicmobile.madeinheaven.ui.pojo;

import android.graphics.Bitmap;

/**
 * Created by Ankit on 9/11/2015.
 */
public class ImageData {
    private String imagePath;
    private Bitmap bitmap;
    private String extension;
    private Integer rotation;
    private boolean flip;

    public boolean isFlip() {
        return flip;
    }

    public void setFlip(boolean flip) {
        this.flip = flip;
    }

    public Integer getRotation() {
        return rotation;
    }

    public void setRotation(Integer rotation) {
        this.rotation = rotation;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
