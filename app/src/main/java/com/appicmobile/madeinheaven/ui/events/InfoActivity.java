package com.appicmobile.madeinheaven.ui.events;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;

public class InfoActivity extends Fragment{
	
	TextView tvInfo;
	String info;
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootview=inflater.inflate(R.layout.info_layout,container,false);
		tvInfo = (TextView)rootview.findViewById(R.id.tvInfo);
		info = Appconstant.INFO;
		tvInfo.setText(info);
		tvInfo.setTextColor(Color.parseColor("#444444"));
		return rootview;
	}
	
	public static Fragment newInstance() {
		InfoActivity activeFragment = new InfoActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}
}
