package com.appicmobile.madeinheaven.ui.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ashvini on 8/25/2015.
 */
public class Images implements Parcelable {

    String base64Value;

    public String getContentValue() {
        return contentValue;
    }

    public void setContentValue(String contentValue) {
        this.contentValue = contentValue;
    }

    String contentValue;
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    String imageName;

    public Images() {
        super();
    }

    public Images(String base64Value, String imageName,String contentValue) {
        this.base64Value = base64Value;
        this.imageName = imageName;
        this.contentValue=contentValue;
    }

    public String getBase64Value() {
        return base64Value;
    }

    public void setBase64Value(String base64Value) {
        this.base64Value = base64Value;
    }

    protected Images(Parcel in) {
        base64Value = in.readString();
        imageName = in.readString();
        contentValue=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(base64Value);
        dest.writeString(imageName);
        dest.writeString(contentValue);
    }

    @SuppressWarnings("unused")
    public static final Creator<Images> CREATOR = new Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel in) {
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
