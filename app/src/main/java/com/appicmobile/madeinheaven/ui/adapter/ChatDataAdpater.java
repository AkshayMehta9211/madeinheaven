package com.appicmobile.madeinheaven.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.UserChatMessage;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.pojo.CHATDATADO;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Ashvini on 09-Jan-16.
 */
public class ChatDataAdpater extends BaseAdapter {


    Context context;
    private Activity act;
    List<CHATDATADO> chat;


    public ChatDataAdpater(Activity act, List<CHATDATADO> chat) {
        this.act = act;
        this.chat = chat;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {

        if (chat != null && chat.size() > 0)
            return chat.size();
        return 0;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;

        final CHATDATADO data = chat.get(position);
        final ViewHolderItem viewHolder;

        if (convertView == null) {

            if (position % 2 == 0)
                convertView = LayoutInflater.from(act).inflate(R.layout.activity_chat_cell, null);
            else
                convertView = LayoutInflater.from(act).inflate(R.layout.activity_chat_cell_gray, null);
            viewHolder = new ViewHolderItem();
            viewHolder.ivUserPics = (ImageView) convertView.findViewById(R.id.ivUserPics);
            viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        String ImageUrl = data.getList().get(position).getImageUrl();
        if (ImageUrl != null) {
            Glide.with(act).load(ImageUrl).error(R.drawable.profile_image).into(viewHolder.ivUserPics);
        }
        viewHolder.tvUserName.setText(data.getList().get(position).getUserName());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(act, UserChatMessage.class);
                i.putExtra(Appconstant.RECEIVER_ID, data.getList().get(position).getUserId());
                i.putExtra(Appconstant.RECEIVER_IMAGE, data.getList().get(position).getImageUrl());
                i.putExtra(Appconstant.RECEIVER_NAME, data.getList().get(position).getUserName());
                act.startActivity(i);
            }
        });
        return convertView;
    }

    private class ViewHolderItem {

        ImageView ivUserPics;
        TextView tvUserName;
    }
}
