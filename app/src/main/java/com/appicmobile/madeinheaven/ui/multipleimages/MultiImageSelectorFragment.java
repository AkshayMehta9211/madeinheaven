package com.appicmobile.madeinheaven.ui.multipleimages;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.ListPopupWindow;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.activity.PostActivity;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.pojo.ImageData;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;



/**
 * 图片选择Fragment Created by Nereo on 2015/4/7.
 */
public class MultiImageSelectorFragment extends Fragment {

	private static final String TAG = "MultiImageSelector";

	/** 最大图片选择次数，int类型 */
	public static final String EXTRA_SELECT_COUNT = "max_select_count";
	/** 图片选择模�?，int类型 */
	public static final String EXTRA_SELECT_MODE = "select_count_mode";
	/** 是�?�显示相机，boolean类型 */
	public static final String EXTRA_SHOW_CAMERA = "show_camera";
	/** 默认选择的数�?�集 */
	public static final String EXTRA_DEFAULT_SELECTED_LIST = "default_result";
	/** �?�选 */
	public static final int MODE_SINGLE = 0;
	/** 多选 */
	public static final int MODE_MULTI = 1;
	// �?�?�loader定义
	private static final int LOADER_ALL = 0;
	private static final int LOADER_CATEGORY = 1;
	// 请求加载系统照相机
	private static final int REQUEST_CAMERA = 100;

	

	// 结果数�?�
	private ArrayList<String> resultList = new ArrayList<String>();
	// 文件夹数�?�
	private ArrayList<Folder> mResultFolder = new ArrayList<Folder>();
	private List<Image> mImages = new ArrayList<Image>();


	// 图片Grid
	private GridView mGridView;
	private ImageView mImageView;
	private Callback mCallback;

	private ImageGridAdapter mImageAdapter;
	private FolderAdapter mFolderAdapter;

	private ListPopupWindow mFolderPopupWindow;

	// 时间线
	private TextView mTimeLineText;
	// 类别
	private TextView mCategoryText;
	// 预览按钮
	private Button mPreviewBtn;
	// 底部View
	private View mPopupAnchorView;

	private int mDesireImageCount;

	private boolean hasFolderGened = false;
	private boolean mIsShowCamera = false;

	private int mGridWidth, mGridHeight;

	private File mTmpFile;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}





	/*
	 * @Override public void onAttach(Context activity) {
	 * super.onAttach(activity); try { mCallback = (Callback) activity; }catch
	 * (ClassCastException e){ throw new ClassCastException(
	 * "The Activity must implement MultiImageSelectorFragment.Callback interface..."
	 * ); } }
	 */

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_multi_image, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// 选择图片数�?
		mDesireImageCount = getArguments().getInt(EXTRA_SELECT_COUNT);

		// 图片选择模�?
		final int mode = getArguments().getInt(EXTRA_SELECT_MODE);

		// 默认选择
		if (mode == MODE_MULTI) {
			ArrayList<String> tmp = getArguments().getStringArrayList(EXTRA_DEFAULT_SELECTED_LIST);
			if (tmp != null && tmp.size() > 0) {
				resultList = tmp;
			}
		}

		// 是�?�显示照相机
		mIsShowCamera = getArguments().getBoolean(EXTRA_SHOW_CAMERA, true);
		mImageAdapter = new ImageGridAdapter(getActivity(), mIsShowCamera);
		// 是�?�显示选择指示器
		mImageAdapter.showSelectIndicator(mode == MODE_MULTI);

		mPopupAnchorView = view.findViewById(R.id.footer);

		mTimeLineText = (TextView) view.findViewById(R.id.timeline_area);
		// �?始化，先�?�?当�?timeline
		mTimeLineText.setVisibility(View.GONE);

		mCategoryText = (TextView) view.findViewById(R.id.category_btn);
		// �?始化，加载所有图片
		mCategoryText.setText(R.string.folder_all);
		mCategoryText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (mFolderPopupWindow == null) {
					createPopupFolderList(mGridWidth, mGridHeight);
				}

				if (mFolderPopupWindow.isShowing()) {
					mFolderPopupWindow.dismiss();
				} else {
					mFolderPopupWindow.show();
					int index = mFolderAdapter.getSelectIndex();
					index = index == 0 ? index : index - 1;
					mFolderPopupWindow.getListView().setSelection(index);
				}
			}
		});

		mPreviewBtn = (Button) view.findViewById(R.id.select);
		// �?始化，按钮状�?�?始化
		if (resultList == null || resultList.size() <= 0) {
			mPreviewBtn.setText(R.string.select);
			mPreviewBtn.setEnabled(true);
		}
		mPreviewBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ArrayList<ImageData> imageUrl = new ArrayList<>();
				String url = null;
				if (resultList != null && resultList.size() > 0) {
					for (int i = 0; i < resultList.size(); i++) {
						url = resultList.get(i).toString();
						ImageData imageData = new ImageData();
						imageData.setImagePath(url);
						imageUrl.add(imageData);
					}
				}

				Intent i = new Intent(getActivity(), PostActivity.class);
				Appconstant.image = imageUrl;
				getActivity().startActivity(i);

			}
		});

		mGridView = (GridView) view.findViewById(R.id.grid);
		mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView absListView, int state) {

				final Picasso picasso = Picasso.with(getActivity());
				if (state == SCROLL_STATE_IDLE || state == SCROLL_STATE_TOUCH_SCROLL) {
					// picasso.resumeTag(getActivity());
				} else {
					// picasso.pauseTag(getActivity());
				}

				if (state == SCROLL_STATE_IDLE) {
					// �?�止滑动，日期指示器消失
					mTimeLineText.setVisibility(View.GONE);
				} else if (state == SCROLL_STATE_FLING) {
					mTimeLineText.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (mTimeLineText.getVisibility() == View.VISIBLE) {
					int index = firstVisibleItem + 1 == view.getAdapter().getCount() ? view.getAdapter().getCount() - 1
							: firstVisibleItem + 1;
					Image image = (Image) view.getAdapter().getItem(index);
					if (image != null) {
						mTimeLineText.setText(TimeUtils.formatPhotoDate(image.path));
					}
				}
			}
		});
		mGridView.setAdapter(mImageAdapter);

		mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			public void onGlobalLayout() {

				final int width = mGridView.getWidth();
				final int height = mGridView.getHeight();

				mGridWidth = width;
				mGridHeight = height;

				final int desireSize = getResources().getDimensionPixelOffset(R.dimen.image_size);
				/*R.dimen.image_size*/
				final int numCount = width / desireSize;
				final int columnSpace = getResources().getDimensionPixelOffset(R.dimen.space_size);
				/*R.dimen.space_size*/
				int columnWidth = (width - columnSpace * (numCount - 1)) / numCount;
				mImageAdapter.setItemSize(columnWidth);

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				} else {
					mGridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
			}
		});
		mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				if (mImageAdapter.isShowCamera()) {
					// 如果显示照相机，则第一个Grid显示为照相机，处�?�特殊逻辑
					/*
					 * if(i == 0){ showCameraAction(); }else{
					 */
					// 正常�?作
					Image image = (Image) adapterView.getAdapter().getItem(i);
					selectImageFromGrid(image, mode);

					// mImageView.setImageResource();
					// }
				} else {
					// 正常�?作
					Image image = (Image) adapterView.getAdapter().getItem(i);
					selectImageFromGrid(image, mode);
				}
			}
		});

		mFolderAdapter = new FolderAdapter(getActivity());
	}

	/**
	 * 创建弹出的ListView
	 */
	private void createPopupFolderList(int width, int height) {
		mFolderPopupWindow = new ListPopupWindow(getActivity());
		mFolderPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mFolderPopupWindow.setAdapter(mFolderAdapter);
		mFolderPopupWindow.setContentWidth(width);
		mFolderPopupWindow.setWidth(width);
		mFolderPopupWindow.setHeight(height * 5 / 8);
		mFolderPopupWindow.setAnchorView(mPopupAnchorView);
		mFolderPopupWindow.setModal(true);
		mFolderPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

				mFolderAdapter.setSelectIndex(i);

				final int index = i;
				final AdapterView v = adapterView;

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mFolderPopupWindow.dismiss();

						if (index == 0) {/*
											 * getActivity().
											 * getSupportLoaderManager().
											 * restartLoader(LOADER_ALL, null,
											 * mLoaderCallback);
											 * mCategoryText.setText(R.string.
											 * folder_all); if (mIsShowCamera) {
											 * mImageAdapter.setShowCamera(true)
											 * ; } else {
											 * mImageAdapter.setShowCamera(false
											 * ); }
											 */
						} else {
							Folder folder = (Folder) v.getAdapter().getItem(index);
							if (null != folder) {
								mImageAdapter.setData(folder.images);
								mCategoryText.setText(folder.name);
								// 设定默认选择
								if (resultList != null && resultList.size() > 0) {
									mImageAdapter.setDefaultSelected(resultList);
								}
							}
							mImageAdapter.setShowCamera(false);
						}
						// 滑动到最�?始�?置
						mGridView.smoothScrollToPosition(0);
					}
				}, 100);

			}
		});
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// 首次加载所有图片
		// new LoadImageTask().execute();
		getActivity().getSupportLoaderManager().initLoader(LOADER_ALL, null, mLoaderCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 相机�?照完�?�?�，返回图片路径
		if (requestCode == REQUEST_CAMERA) {
			if (resultCode == Activity.RESULT_OK) {
				if (mTmpFile != null) {
					if (mCallback != null) {
						mCallback.onCameraShot(mTmpFile);
					}
				}
			} else {
				if (mTmpFile != null && mTmpFile.exists()) {
					mTmpFile.delete();
				}
			}
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.d(TAG, "on change");

		if (mFolderPopupWindow != null) {
			if (mFolderPopupWindow.isShowing()) {
				mFolderPopupWindow.dismiss();
			}
		}

		mGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			public void onGlobalLayout() {

				final int height = mGridView.getHeight();

				final int desireSize = getResources().getDimensionPixelOffset(R.dimen.image_size);
				/*R.dimen.image_size*/
				Log.d(TAG, "Desire Size = " + desireSize);
				final int numCount = mGridView.getWidth() / desireSize;
				Log.d(TAG, "Grid Size = " + mGridView.getWidth());
				Log.d(TAG, "num count = " + numCount);
				final int columnSpace = getResources().getDimensionPixelOffset(R.dimen.space_size);
				/*R.dimen.space_size*/
				int columnWidth = (mGridView.getWidth() - columnSpace * (numCount - 1)) / numCount;
				mImageAdapter.setItemSize(columnWidth);

				if (mFolderPopupWindow != null) {
					mFolderPopupWindow.setHeight(height * 5 / 8);
				}
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				} else {
					mGridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
			}
		});

		super.onConfigurationChanged(newConfig);

	}

	/**
	 * 选择相机
	 */
	private void showCameraAction() {
		// 跳转到系统照相机
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// 设置系统相机�?照�?�的输出路径
			// 创建临时文件
			mTmpFile = FileUtils.createTmpFile(getActivity());
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTmpFile));
			startActivityForResult(cameraIntent, REQUEST_CAMERA);
		} else {
			Toast.makeText(getActivity(), R.string.msg_no_camera, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 选择图片�?作
	 * 
	 * @param image
	 */
	private void selectImageFromGrid(Image image, int mode) {
		if (image != null) {
			// 多选模�?
			if (mode == MODE_MULTI) {
				if (resultList.contains(image.path)) {
					resultList.remove(image.path);
					if (resultList.size() != 0) {
						mPreviewBtn.setEnabled(true);
						mPreviewBtn.setText(getResources().getString(R.string.select) + "(" + resultList.size() + ")");
					} else {
						mPreviewBtn.setEnabled(false);
						mPreviewBtn.setText(R.string.select);
					}
					if (mCallback != null) {
						mCallback.onImageUnselected(image.path);
					}


//					Intent i = new Intent(getActivity(), PostActivity.class);

//					getActivity().startActivity(i);



				} else {
					// 判断选择数�?问题
					if (mDesireImageCount == resultList.size()) {
						Toast.makeText(getActivity(), R.string.msg_amount_limit, Toast.LENGTH_SHORT).show();
						return;
					}
					resultList.add(image.path);
					ArrayList<ImageData> imageUrl = new ArrayList<>();
					String url = null;
					if (resultList != null && resultList.size() > 0) {
						for (int i = 0; i < resultList.size(); i++) {
							url = resultList.get(i).toString();
							ImageData imageData = new ImageData();
							imageData.setImagePath(url);
							imageUrl.add(imageData);
						}
						Appconstant.image = imageUrl;
					}



					mPreviewBtn.setEnabled(true);
					mPreviewBtn.setText(getResources().getString(R.string.selected) + "(" + resultList.size() + ")");
					if (mCallback != null) {
						mCallback.onImageSelected(image.path);
					}
				}
				mImageAdapter.select(image);
			} else if (mode == MODE_SINGLE) {
				// �?�选模�?
				if (mCallback != null) {
					mCallback.onSingleImageSelected(image.path);
				}
			}
		}
	}

	private LoaderManager.LoaderCallbacks<Cursor> mLoaderCallback = new LoaderManager.LoaderCallbacks<Cursor>() {

		private final String[] IMAGE_PROJECTION = { MediaStore.Images.Media.DATA, MediaStore.Images.Media.DISPLAY_NAME,
				MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.Media._ID };

		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle args) {
			if (id == LOADER_ALL) {
				CursorLoader cursorLoader = new CursorLoader(getActivity(),
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_PROJECTION, null, null,
						IMAGE_PROJECTION[2] + " DESC");
				return cursorLoader;
			} else if (id == LOADER_CATEGORY) {
				CursorLoader cursorLoader = new CursorLoader(getActivity(),
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_PROJECTION,
						IMAGE_PROJECTION[0] + " like '%" + args.getString("path") + "%'", null,
						IMAGE_PROJECTION[2] + " DESC");
				return cursorLoader;
			}
			return null;
		}

		@Override
		public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
			if (data != null) {
				List<Image> images = new ArrayList<Image>();
				int count = data.getCount();
				if (count > 0) {
					data.moveToFirst();
					do {
						String path = data.getString(data.getColumnIndexOrThrow(IMAGE_PROJECTION[0]));
						String name = data.getString(data.getColumnIndexOrThrow(IMAGE_PROJECTION[1]));
						long dateTime = data.getLong(data.getColumnIndexOrThrow(IMAGE_PROJECTION[2]));
						Image image = new Image(path, name, dateTime);
						images.add(image);
						if (!hasFolderGened) {
							// 获�?�文件夹�??称
							File imageFile = new File(path);
							File folderFile = imageFile.getParentFile();
							Folder folder = new Folder();
							folder.name = folderFile.getName();
							folder.path = folderFile.getAbsolutePath();
							folder.cover = image;
							if (!mResultFolder.contains(folder)) {
								List<Image> imageList = new ArrayList<Image>();
								imageList.add(image);
								folder.images = imageList;
								mResultFolder.add(folder);
							} else {
								// 更新
								Folder f = mResultFolder.get(mResultFolder.indexOf(folder));
								f.images.add(image);
							}
						}

					} while (data.moveToNext());
					mImageAdapter.setData(images);

					// 设定默认选择
					if (resultList != null && resultList.size() > 0) {
						mImageAdapter.setDefaultSelected(resultList);
					}
					mFolderAdapter.setData(mResultFolder);
					hasFolderGened = true;
				}
			}
		}

		@Override
		public void onLoaderReset(Loader<Cursor> loader) {

		}
	};

	/**
	 * 回调接�?�
	 */
	public interface Callback {
		void onSingleImageSelected(String path);

		void onImageSelected(String path);

		void onImageUnselected(String path);

		void onCameraShot(File imageFile);
	}
}
