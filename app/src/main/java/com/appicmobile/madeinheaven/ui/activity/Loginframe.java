package com.appicmobile.madeinheaven.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.Instagram.InstagramMainActivity;
import com.appicmobile.madeinheaven.ui.InstagramSupport.Instagram;
import com.appicmobile.madeinheaven.ui.InstagramSupport.InstagramSession;
import com.appicmobile.madeinheaven.ui.Interface.Key;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.InstagramDo;
import com.appicmobile.madeinheaven.ui.common.LoginDo;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.PreferenceUtils;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.UserRegister;
import com.appicmobile.madeinheaven.ui.utils.AppSharedPreferences;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class Loginframe extends Fragment implements OnClickListener {

	private Button facebook, instagram, email;

	private InstagramSession mInstagramSession;
	private Instagram mInstagram;

	private ProgressBar mLoadingPb;
	private GridView mGridView;

	private String mConsumerKey;
	private String mSecretKey;
	public PreferenceUtils preference;

	private static final String CLIENT_ID = "68b3591697504c509c42618dd92782d6";
	private static final String CLIENT_SECRET = "96eaf0f89cd041f28e23d85b1e8d46ef";
	private static final String REDIRECT_URI = "http://www.appicmobile.com";
	public static final String CALLBACK_URL = "http://appicmobile.com";

	final static String TwitterTokenURL = "https://api.twitter.com/oauth2/token";
	final static String TwitterStreamURL = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=";

	final static String ScreenName = "therockncoder";
	final static String LOG_TAG = "Made In Heaven";

	String userName;
	String userProfilePicUrl;
	String emailId;
	String userId;
	String token="";


	String text1;
	String text2;

	TextView terms;
	ArrayList<SERVICEDATA> AllData = new ArrayList<>();

	int PRIVATE_MODE = 0;
	SharedPreferences spref;

	// Editor for Shared preferences
	Editor editor;

	private static final String PREF_NAME = "LOGIN_DATA";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	// User name (make variable public to access from outside)
	public static final String USER_ID = "userId";

	// Email address (make variable public to access from outside)
	public static final String LOGIN_WEDDING_CODE = "wedding";

	public static final String LOGIN_HASHTAG = "hashtag";
	public static final String LOGIN_PROFILE_PICS = "profilepics";
	public static final String LOGIN_USERNAME = "username";
	public static final String LOGIN_TYPE = "userType" ;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		preference=new PreferenceUtils(getActivity());
		spref = this.getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		editor = spref.edit();
		View rootview = inflater.inflate(R.layout.loginframe, container, false);

		mConsumerKey = "Zi1MjXzh7BhBFd88yZA4VRZ2X";
		mSecretKey = "bJzNj5tswL2WCPC77qINNEKWkLJke5DtkscsiCAlIn6pJbOAyh";

//		loadDataFromServer();
		// generateFacebookKeyHash();

		int all = AllData.size();
//		Toast.makeText(getActivity(), "id" + all, Toast.LENGTH_LONG).show();

		facebook = (Button) rootview.findViewById(R.id.btnFacebookLogin);
		instagram = (Button) rootview.findViewById(R.id.btnInstagramLogin);

		terms = (TextView)rootview.findViewById(R.id.terms);

		/*String text1 = "By joining Made in Heaven, you agree to <a style=\"color: #CC0000\" href=\"http://theappmadeinheaven.com/index.php/website/terms\">Terms of Use</a>" ;
		String text2= "and acknowledge <a style=\"color: #CC0000\" href = \"http://theappmadeinheaven.com/index.php/website/privacyPolicy\">Privacy Policy</a>";

		terms.setText( Html.fromHtml(text1)+""+Html.fromHtml(text2));
		text1.setMovementMethod(LinkMovementMethod.getInstance());*/

//		email = (Button) rootview.findViewById(R.id.btnEmailLogin);
		instagram.setOnClickListener(this);
		facebook.setOnClickListener(this);
//		email.setOnClickListener(this);
		return rootview;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if(isVisibleToUser) {
	        Activity a = getActivity();
	        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	}

	public void generateFacebookKeyHash() {
		// Add code to print out the key hash
		try {
			PackageInfo info = getActivity().getPackageManager().getPackageInfo("com.appicmobile.madeinheaven",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String BaseUrl = "https://api.instagram.com/v1/tags/" + Appconstant.HASHTAG
			+ "/media/recent?access_token=2270773782.68b3591.96e66744dd0b448d8274af5d83ad7a16";

	private void loadDataFromServer() {

		// String jsonString = gson.toJson("");
		RequestGenerator.makeGetRequest(getActivity(), BaseUrl, true, new ResponseListener() {
			@Override
			public void onSuccess(String response) {
				try {

				} catch (Exception e) {
					Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
			}

			@Override
			public void onError(VolleyError error) {
				Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess_one(JSONObject response) {
				try {
					JSONObject jsonResponse = new JSONObject();
					jsonResponse = response.getJSONObject("pagination");
					jsonResponse = response.getJSONObject("meta");
					JSONArray jsonArray = response.getJSONArray("data");

					if (response != null) {
						try {
							if (response.has(Key.TAG_DATA)) {

								// Getting JSON Array node

								JSONArray arrData = response.getJSONArray(Key.TAG_DATA);

								for (int i = 0; i < arrData.length(); i++) {

									int comment_count = 0;
									long created_time = 0;
									String post_id = null;

									JSONObject data = arrData.getJSONObject(i);
									// comment_count =
									// data.getInt(Key.TAG_COMMENT_COUNT);

									created_time = data.getLong(Key.TAG_CREATED_TIME);

									post_id = data.getString(Key.TAG_ID);
									JSONObject likes = data.getJSONObject(Key.TAG_LIKE);
									String likes_count = null;
									if (likes.has(Key.TAG_LIKE_COUNT)) {
										likes_count = likes.getString(Key.TAG_LIKE_COUNT);
									}

									JSONObject images = data.getJSONObject(Key.TAG_IMAGES);

									String images_standard = null;
									if (images.has(Key.TAG_IMAGES_STANDARD_RESOLUTION)) {
										images_standard = images.getJSONObject(Key.TAG_IMAGES_STANDARD_RESOLUTION)
												.getString(Key.TAG_IMAGES_STANDARD_RESOLUTION_URL);
									}

									JSONObject user = data.getJSONObject(Key.TAG_USER);

									String username = null;
									String profilePics = null;
									String userFullName = null;
									if (user.has(Key.TAG_USER_NAME)) {
										username = user.getString(Key.TAG_USER_NAME);
									}
									if (user.has(Key.TAG_USER_PROFILE_PICS)) {
										profilePics = user.getString(Key.TAG_USER_PROFILE_PICS);
									}
									if (user.has(Key.TAG_USER_FULL_NAME)) {
										userFullName = user.getString(Key.TAG_USER_FULL_NAME);
									}

									SERVICEDATA allvalues = new SERVICEDATA();
									// allvalues.setComments_count(comment_count);
									allvalues.setImages_standard_resolution(images_standard);
									// allvalues.setCreated_time(created_time);
									// allvalues.setLikes_count(likes_count);
									allvalues.setUsers_name(username);
									allvalues.setUsers_profile_pics(profilePics);
									// allvalues.setUsers_fullname(userFullName);
									allvalues.setType("instagran");
									allvalues.setPost_id("post_id");
									AllData.add(allvalues);
								}

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				// lvListView = (ListView) findViewById(R.id.lvListView);
				// Home_cell_Adapter adapter = new
				// Home_cell_Adapter(HomeActivity.this, AllData);
				// lvListView.setAdapter(adapter);
			}
		});

	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnFacebookLogin:
			loginWithFacebook();
			break;
		case R.id.btnInstagramLogin:
			loginWithInstagram();
			break;
		/*case R.id.btnEmailLogin:
			Fragment haveaccount = new Login_Account();
			FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.main_container, haveaccount);
			fragmentTransaction.commit();*/

		default:
			break;
		}

	}

	private void loginWithInstagram() {
		
		//Toast.makeText(getActivity().getApplicationContext(), "Instagram UnderConstruction", Toast.LENGTH_SHORT).show();
		Intent i = new Intent(getActivity(), InstagramMainActivity.class);
		getActivity().startActivity(i);

	}

	public void loginWithFacebook() {
		Session session = Session.getActiveSession();

		if (session == null)
			session = new Session(getActivity().getApplicationContext());

		if (session != null && !session.isOpened() && !session.isClosed()) {
			Session.OpenRequest op = new Session.OpenRequest(Loginframe.this);
			op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
			op.setCallback(statusCallback);
			op.setPermissions(getPermissions());
			session = new Session.Builder(getActivity()).build();
			Session.setActiveSession(session);
			session.openForRead(op);
			token = session.getAccessToken();
//			 RequestData();
		} else {
			Session.openActiveSession(getActivity(), true, statusCallback);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		 if (resultCode == getActivity().RESULT_OK)
		    {
		        com.facebook.Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
		    }
		    else
		    {
		        com.facebook.Session.getActiveSession().closeAndClearTokenInformation();
		    }
		}
	public List<String> getPermissions() {
		List<String> PERMISSIONS = new ArrayList<String>();
		PERMISSIONS.add("email");
		PERMISSIONS.add("public_profile");
		PERMISSIONS.add("user_friends");
		return PERMISSIONS;
	}

	public Session.StatusCallback statusCallback = new SessionStatusCallback();

	public class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (session != null && session.isOpened()) {
				getFacebookUserInfo();
			} else {
//				loginWithFacebook();
			}
		}
	}

	private void startDrawerActivity() {
		Intent i = new Intent(getActivity(), NavDrawer.class);
		startActivity(i);
		getActivity().finish();
	}
	
	public void signOutFacebook()
	{
		Session currentSession = Session.getActiveSession();
		if(currentSession != null && currentSession.isOpened())
		{
			currentSession.closeAndClearTokenInformation();
			
		}
	}

	private void getFacebookUserInfo() {
		if (Session.getActiveSession() != null && Session.getActiveSession().isOpened()) {
			Request request = Request.newMeRequest(Session.getActiveSession(), new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					userId = user.getId();
					userName = user.getName();
					final String firstName = user.getFirstName();
					final String lastName = user.getLastName();
					userProfilePicUrl = "http://graph.facebook.com/" + user.getId() + "/picture?type=large";
					final String emailId = user.getProperty("email") != null ? user.getProperty("email").toString()
							: "NA";
					final String genderString = user.getProperty("gender") != null
							? user.getProperty("gender").toString() : "NA";
					final int gender = !genderString.equalsIgnoreCase("male")
							? (!genderString.equalsIgnoreCase("female") ? -1 : 0) : 1;
							
							Appconstant.LOGIN_USER_ID =Long.parseLong(user.getId());

							validateLogin2Server();
				}

				private void validateLogin2Server() {
					if (!Appconstant.WEDDING_CODE.equals("")) {
						InstagramDo login = new InstagramDo();
						login.weddingCode = String.valueOf(Appconstant.WEDDING_CODE);
						login.emailId = userId;
						login.loginType = "facebook";
						login.userName = userName;
						login.profilePic = userProfilePicUrl;
						login.type = "user";
						login.deviceType="android";
						login.deviceToken=preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");
						login.token = preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");

						final Gson gson = new Gson();
						String jsonString = gson.toJson(login);
						RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_REGISTER_USER,
								jsonString, true, new ResponseListener() {
							@Override
							public void onSuccess(String response) {
								try {
									JSONObject jsonObject = new JSONObject(response);
									UserRegister userLogin1 = gson.fromJson(response, UserRegister.class);
									Log.e("status",userLogin1.getStatus());
									if ("successoosss".equals(userLogin1.getStatus())) {
										editor.putInt(USER_ID, userLogin1.getId());
										editor.putString(LOGIN_WEDDING_CODE, userLogin1.getWeddingCode());
										editor.putString(LOGIN_HASHTAG, userLogin1.getHashtag());
										editor.putString(LOGIN_PROFILE_PICS, userLogin1.getImageUrl());
										editor.putString(LOGIN_USERNAME, userLogin1.getUserName());
										editor.putString(LOGIN_TYPE,userLogin1.getUserType());
										editor.commit();
										Appconstant.USER_ID = userLogin1.getId();
										Appconstant.WEDDING_CODE = userLogin1.getWeddingCode();
										Appconstant.HASHTAG = userLogin1.getHashtag();
										Appconstant.PROFILE_PICS = userLogin1.getImageUrl();
										Appconstant.USERNAME = userLogin1.getUserName();
										Appconstant.TYPE = userLogin1.getUserType();

										if(jsonObject.has("weddingList")){
											JSONArray jsonArray = jsonObject.getJSONArray("weddingList");
											new AppSharedPreferences(getActivity()).setWeddingList(jsonArray.toString());
										}
										startDrawerActivity();

									} else {
										Toast.makeText(getActivity(), userLogin1.getMsg(), Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
									e.printStackTrace();
								}
							}

							@Override
							public void onError(VolleyError error) {
								Toast.makeText(getActivity(),error.toString(),Toast.LENGTH_LONG).show();
								// TODO Auto-generated method stub
							}

							@Override
							public void onSuccess_one(JSONObject response) {
								Toast.makeText(getActivity(),response.toString(),Toast.LENGTH_LONG).show();
								// TODO Auto-generated method stub
							}

						});

					} else {
						LoginDo login = new LoginDo();
						login.emailId = String.valueOf(Appconstant.LOGIN_USER_ID);
						login.deviceType="android";
						login.deviceToken=preference.getStringFromPreference(Appconstant.GCM_TOKEN,"");
						final Gson gson = new Gson();
						String jsonString = gson.toJson(login);
						RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_LOGIN_USER_2,
								jsonString, true, new ResponseListener() {
							@Override
							public void onSuccess(String response) {
								try {
									UserRegister userLogin1 = gson.fromJson(response, UserRegister.class);
									if ("false".equals(userLogin1.getStatus())) {
										new AlertDialog.Builder(getActivity())
									    .setMessage("Please Join or create a wedding")
									    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
									        public void onClick(DialogInterface dialog, int which) { 
									        	
									        }
									     })
									    
									    .setIcon(android.R.drawable.ic_dialog_alert)
									     .show();

									} else {
										editor.putInt(USER_ID, userLogin1.getId());
										editor.putString(LOGIN_WEDDING_CODE, userLogin1.getWeddingCode());
										editor.putString(LOGIN_HASHTAG, userLogin1.getHashtag());
										editor.putString(LOGIN_PROFILE_PICS, userLogin1.getImageUrl());
										editor.putString(LOGIN_USERNAME, userLogin1.getUserName());
										editor.putString(LOGIN_TYPE,userLogin1.getUserType());
										editor.commit();
										Appconstant.FACEBOOK = true;
										Appconstant.USER_ID = userLogin1.getId();
										Appconstant.WEDDING_CODE = userLogin1.getWeddingCode();
										Appconstant.HASHTAG = userLogin1.getHashtag();
										Appconstant.PROFILE_PICS = userLogin1.getImageUrl();
										Appconstant.USERNAME = userLogin1.getUserName();
										Appconstant.TYPE = userLogin1.getUserType();
										startDrawerActivity();
									}
								} catch (Exception e) {
									Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
									e.printStackTrace();
								}
							}

							@Override
							public void onError(VolleyError error) {
								Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
							}

							@Override
							public void onSuccess_one(JSONObject response) {
								// TODO Auto-generated method stub
							}

						});
					}
				}
			});
			request.getParameters().putString("fields", "id,name,picture,first_name,last_name,gender,email,birthday");
			request.executeAsync();
		} else
			loginWithFacebook();
	}

	public static Fragment newInstance() {
		Loginframe activeFragment = new Loginframe();
		Bundle bundle = new Bundle();
		activeFragment.setArguments(bundle);
		return activeFragment;
	}
}
// }
