package com.appicmobile.madeinheaven.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.adapter.ChatMessageAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.pojo.ChatMessage;
import com.appicmobile.madeinheaven.ui.pojo.ChatMessageResponse;
import com.appicmobile.madeinheaven.ui.pojo.Message;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ashvini on 09-Jan-16.
 */
public class UserChatMessage extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mListView;
    private Button mButtonSend;
    private EditText mEditTextMessage;
    private String receiverId;
    private ChatMessageAdapter mAdapter;
    private ArrayList<Message> messages = new ArrayList<>();
    private ImageView ivUserPic;
    private TextView tvUserName;
    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_side_chat);

        mListView = (RecyclerView) findViewById(R.id.listView);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);
        ivUserPic = (ImageView) findViewById(R.id.ivProfile);
        tvUserName = (TextView) findViewById(R.id.tvName);
        findViewById(R.id.ivBack).setOnClickListener(this);
        ivUserPic.setOnClickListener(this);
        receiverId = getIntent().getStringExtra(Appconstant.RECEIVER_ID);
        String imageUrl = getIntent().getStringExtra(Appconstant.RECEIVER_IMAGE);
        String Name = getIntent().getStringExtra(Appconstant.RECEIVER_NAME);

        if (!TextUtils.isEmpty(imageUrl)) {
            Glide.with(UserChatMessage.this).load(imageUrl).error(R.drawable.profile_image).into(ivUserPic);
        }
        if (!TextUtils.isEmpty(Name)) {
            tvUserName.setText(Name);
        }
        LinearLayoutManager manager = new LinearLayoutManager(UserChatMessage.this);
        mListView.setLayoutManager(manager);
        hideKeyboard();
        displayChatMessage(false);
        myHandler.postDelayed(UpdateData, 2000);
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mEditTextMessage.getText().toString();
                if (TextUtils.isEmpty(message)) {
                    return;
                }
                SendMessage(message);
                mEditTextMessage.setText("");
            }
        });

    }

    public Runnable UpdateData = new Runnable() {
        public void run() {
            try {
                displayChatMessage(true);
                myHandler.postDelayed(this, 2000);
            } catch (Exception e) {
            }
        }
    };


    private void displayChatMessage(final boolean isRefresh) {
        final ChatMessage message = new ChatMessage();
        message.setWeddingCode(Integer.parseInt(Appconstant.WEDDING_CODE));
        message.setSenderId(String.valueOf(Appconstant.USER_ID));
        message.setReceiverId(receiverId);
        final Gson gson = new Gson();
        String jsonString = gson.toJson(message);
        RequestGenerator.makePostRequest(UserChatMessage.this, MadeInHeavenServiceURL.URL_CHAT_MESSAGE, jsonString, false,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (!TextUtils.isEmpty(response)) {
                            ChatMessageResponse messageResponse = gson.fromJson(response, ChatMessageResponse.class);
                            if (messageResponse != null) {
                                if (isRefresh) {
                                    //messages.addAll(messageResponse.getMessage());
                                    if(mAdapter!=null){
                                        mAdapter.addItemsToList(messageResponse.getMessage());
                                        mAdapter.notifyDataSetChanged();
                                        mListView.scrollToPosition(mAdapter.getItemCount() - 1);
                                    }
                                } else {
                                    messages.addAll(messageResponse.getMessage());
                                    if (messages.size() > 0) {
                                        mAdapter = new ChatMessageAdapter(UserChatMessage.this, messages);
                                        mListView.setAdapter(mAdapter);
                                        mListView.scrollToPosition(mAdapter.getItemCount() - 1);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        //Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(getActivity(), "successOne", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void SendMessage(String text) {
        final ChatMessage message = new ChatMessage();
        message.setWeddingCode(Integer.parseInt(Appconstant.WEDDING_CODE));
        message.setSenderId(String.valueOf(Appconstant.USER_ID));
        message.setReceiverId(receiverId);
        message.setMessage(text);
        final Gson gson = new Gson();
        String jsonString = gson.toJson(message);
        RequestGenerator.makePostRequest(UserChatMessage.this, MadeInHeavenServiceURL.URL_SEND_MESSAGE, jsonString, false,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (!TextUtils.isEmpty(response)) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("true")) {
//                                    RefreshActivity();
                                    displayChatMessage(true);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        //Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(getActivity(), "successOne", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void RefreshActivity() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void onClick(View v) {
        finish();
    }
}