package com.appicmobile.madeinheaven.ui.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 07-Jan-16.
 */
public class RSVPEVENTDO {


    public List<DATA> getEvent() {
        return event;
    }

    public void setEvent(List<DATA> event) {
        this.event = event;
    }

    public List<DATA> event = new ArrayList<DATA>();

    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public static class DATA {
        public String  event_id;
        public String rsvp_option;
        public String userid;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getEvent_id() {
            return event_id;
        }

        public void setEvent_id(String event_id) {
            this.event_id = event_id;
        }

        public String getRsvp_option() {
            return rsvp_option;
        }

        public void setRsvp_option(String rsvp_option) {
            this.rsvp_option = rsvp_option;
        }

    }
}
