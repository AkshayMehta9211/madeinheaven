package com.appicmobile.madeinheaven.ui.common;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;


public class MadeInHeavenWebService {
	public StringBuffer url = new StringBuffer();
	public JSONObject jsonObject;

	public JSONObject callServicePost(String serviceName, JSONObject jsonObject) {

//		url.append(DavaaiiServiceURL.URL_LOGIN_USER);
		url.append(serviceName);
		this.jsonObject = jsonObject;
		String response = null;
		

		try {

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(url.toString());
			post.setHeader("content-type", "application/json");
			System.out.println("url->" + url.toString());
			System.out.println("request->" + jsonObject);
			StringEntity entity = new StringEntity(jsonObject.toString());
			post.setEntity(entity);
			HttpResponse resp = httpClient.execute(post);
			response = EntityUtils.toString(resp.getEntity(), "UTF_8");
			if (response != null && response.contains("status")) {
				JSONObject response_jsonObject = new JSONObject(response);
				System.out.println("response->" + response_jsonObject);
				return response_jsonObject;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public JSONObject callServicePut(String serviceName, JSONObject jsonObject) {

//		url.append(DavaaiiServiceURL.URL_LOGIN_USER);
		url.append(serviceName);
		this.jsonObject = jsonObject;
		String response = null;

		try {
			
			

			HttpClient httpClient = new DefaultHttpClient();
			HttpPut put = new HttpPut(url.toString());
			put.setHeader("content-type", "application/json");
			System.out.println("url->" + url.toString());
			System.out.println("request->" + jsonObject);
			StringEntity entity = new StringEntity(jsonObject.toString());
			put.setEntity(entity);
			HttpResponse resp = httpClient.execute(put);
			response = EntityUtils.toString(resp.getEntity(), "UTF_8");
			System.out.println("response->" + response);
			if (response != null ) {
				JSONObject response_jsonObject = new JSONObject(response);
				System.out.println("response->" + response_jsonObject);
				return response_jsonObject;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public JSONObject callServiceGet(String serviceName) {

//		url.append(DavaaiiServiceURL.URL_LOGIN_USER);
		url.append(serviceName);
		System.out.println("req url->"+url.toString());
		String response = null;

		try {

			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse resp = httpClient.execute(new HttpGet(url.toString()));
			response = EntityUtils.toString(resp.getEntity(), "UTF_8");
			System.out.println("response->" + response);
			if (response != null && response.contains("message")) {
				JSONObject response_jsonObject = new JSONObject(response);
				System.out.println("response_jsonObject->" + response_jsonObject);
				return response_jsonObject;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
