package com.appicmobile.madeinheaven.ui.tabs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.InstagramSupport.util.StringUtil;
import com.appicmobile.madeinheaven.ui.activity.FullImageActivity;
import com.appicmobile.madeinheaven.ui.activity.NotificationActivity;
import com.appicmobile.madeinheaven.ui.activity.VideoViewActivity;
import com.appicmobile.madeinheaven.ui.adapter.GalleryGridViewAdapter;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.EventList;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.USERPOSTDO;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.appicmobile.madeinheaven.ui.utils.Utils;
import com.appicmobile.madeinheaven.ui.view.FeedContextMenuManager;
import com.facebook.android.Util;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class GalleryActivity extends Fragment {

    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    GridView gridview;
    ArrayList<USERPOSTDO> allPostData = new ArrayList();
    ArrayList<USERPOSTDO> tempAllPostData = new ArrayList();

    Toolbar toolbar;
    TextView toolbar_title;
    ImageView filterImageView;
    RelativeLayout filterLayout;

    boolean isAllPost = true;

    RadioButton radioMy,radioAll;
    TextView fromTextView,toTextView,applyTextView,cancelTextView,resetTextView;
    private GalleryGridViewAdapter adapter;

    private int year, month, day;
    String mFromDate="",mToDate="";


    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.gallery_layout, container, false);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ((NavDrawer) getActivity()).setActionBarTitle("WEDDING GALLERY");
        FeedContextMenuManager man = new FeedContextMenuManager();
        man.getInstance().dialogDismiss();


        gridview = (GridView) rootview.findViewById(R.id.gridview);
        filterImageView = (ImageView) rootview.findViewById(R.id.filterImageView);
        filterLayout = (RelativeLayout) rootview.findViewById(R.id.filterLayout);

        radioMy = (RadioButton) rootview.findViewById(R.id.radioMy);
        radioAll = (RadioButton) rootview.findViewById(R.id.radioAll);

        fromTextView = (TextView) rootview.findViewById(R.id.fromTextView);
        toTextView = (TextView) rootview.findViewById(R.id.toTextView);

        applyTextView = (TextView) rootview.findViewById(R.id.applyTextView);

        cancelTextView = (TextView) rootview.findViewById(R.id.cancelTextView);
        resetTextView = (TextView) rootview.findViewById(R.id.resetTextView);
        getImagesFromServer();


        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridview.setVisibility(View.VISIBLE);
                filterLayout.setVisibility(View.GONE);
            }
        });

        resetTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFromDate="";
                mToDate="";
                fromTextView.setText("From");
                toTextView.setText("To");
                filterImageView.setImageResource(R.drawable.ic_filter);
                getImagesFromServer();
            }
        });

        applyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radioMy.isChecked()){
                    isAllPost = false;
                }else{
                    isAllPost = true;
                }
                filterImageView.setImageResource(R.drawable.ic_filter_applied);

                filterResults();

            }
        });



        filterImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gridview.getVisibility()==View.VISIBLE){
                    gridview.setVisibility(View.GONE);
                    filterLayout.setVisibility(View.VISIBLE);
                    if(isAllPost){
                        radioAll.setChecked(true);
                    }
                }else{
                    gridview.setVisibility(View.VISIBLE);
                    filterLayout.setVisibility(View.GONE);
                }
            }
        });




        fromTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                fromTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                mFromDate = fromTextView.getText().toString();
                            }
                        }, year, month, day);
                dpd.show();
            }
        });


        toTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                toTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                mToDate = toTextView.getText().toString();
                            }
                        }, year, month, day);
                dpd.show();
            }
        });


        return rootview;
    }


    private void filterResults() {
        ArrayList<USERPOSTDO> filteredAllPostData = new ArrayList();
        if(!isAllPost){
            for(int i=0;i<allPostData.size();i++){
                USERPOSTDO item = allPostData.get(i);
                Log.e("IDS",item.getPostId()+"-"+Appconstant.USER_ID);
                if(Integer.parseInt(item.getPostId()) == Appconstant.USER_ID){
                    filteredAllPostData.add(allPostData.get(i));
                }
            }
        }else{
            filteredAllPostData.addAll(tempAllPostData);
        }

        if(!mFromDate.isEmpty() && !mToDate.isEmpty()){
            ArrayList<USERPOSTDO> filteredDateAllPostData = new ArrayList();
            for(int i=0;i<filteredAllPostData.size();i++){
                USERPOSTDO item = filteredAllPostData.get(i);
                Log.e("IDS",item.getPostId()+"-"+Appconstant.USER_ID);
                if(Utils.isPostDateValid(mFromDate,mToDate,item.postDate)){
                    filteredDateAllPostData.add(filteredAllPostData.get(i));
                }
            }
            setFilteredData(filteredDateAllPostData);
        }else{
            setFilteredData(filteredAllPostData);
        }


    }

    private void setFilteredData(ArrayList<USERPOSTDO> filteredAllPostData) {
        if(filteredAllPostData.size()>0) {
            filterLayout.setVisibility(View.GONE);
            gridview.setVisibility(View.VISIBLE);
            allPostData.clear();
            allPostData.addAll(filteredAllPostData);
            if (adapter == null) {
                adapter = new GalleryGridViewAdapter(getActivity(), allPostData);
                gridview.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }
        }else{
            Utils.showToast(getActivity(),"No Filter Result Found");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Activity a = getActivity();
            if (a != null)
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private void getImagesFromServer() {
        allPostData.clear();
        gridview.setVisibility(View.VISIBLE);
        filterLayout.setVisibility(View.GONE);
        isAllPost = true;
        String s = "test";
        EventList event = new EventList();
        // event.userId = Appconstant.USER_ID;
        event.weddingCode = String.valueOf(Appconstant.WEDDING_CODE);

        final Gson gson = new Gson();
        String jsonString = gson.toJson(event);
        Log.e("jsonString",jsonString);
        RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_USER_POST, jsonString, true,
                new ResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Log.e("response",response);
                            JSONObject object = new JSONObject(response);
                            JSONArray Jarray = object.getJSONArray("getallpost");
                            for (int i = 0; i < Jarray.length(); i++) {
                                JSONObject Jasonobject = Jarray.getJSONObject(i);
                                String postImg = null;
                                String postImageUrl = null;
                                String posttype = null;
                                postImageUrl = Jasonobject.getString("postImg");
                                posttype = Jasonobject.getString("posttype");
                                String userId = Jasonobject.getString("userId");
                                String date = Jasonobject.getString("postDate");

                                USERPOSTDO eventValue = new USERPOSTDO();
                                eventValue.setPostImg(postImageUrl);
                                eventValue.setType(posttype);
                                eventValue.setPostId(userId);
                                eventValue.postDate = date.substring(0,date.indexOf(" "));
                                allPostData.add(eventValue);
                            }
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        tempAllPostData.addAll(allPostData);
                        adapter = new GalleryGridViewAdapter(getActivity(), allPostData);
                        gridview.setAdapter(adapter);
                        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (allPostData.get(position).getType().equalsIgnoreCase("Image")) {
                                    ArrayList<String> imageList = getAllImages(allPostData);
                                    Intent i = new Intent(getActivity(), FullImageActivity.class);
                                    i.putStringArrayListExtra("images",imageList);
                                    i.putExtra("position", position);
                                    startActivity(i);
                                } else {
                                    String ImageUrl = allPostData.get(position).getPostImg();
                                    Intent i = new Intent(getActivity(), VideoViewActivity.class);
                                    i.putExtra("VideoUrl", ImageUrl);
                                    startActivity(i);
                                }

                            }
                        });

                    }

                    @Override
                    public void onError(VolleyError error) {
                        Toast.makeText(getActivity(), "please_try_again", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess_one(JSONObject response) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getActivity(), "JsonResponse", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private ArrayList<String> getAllImages(ArrayList<USERPOSTDO> allPostData) {
        ArrayList<String> imageList = new ArrayList<String>();
        for(int i=0;i<allPostData.size();i++){
            imageList.add(allPostData.get(i).getPostImg());
        }
        return imageList;
    }

    public static Fragment newInstance() {
        GalleryActivity activeFragment = new GalleryActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.gallery_menu, menu);
        menu.findItem(R.id.gallery_notification).setVisible(true).setEnabled(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        inflater.inflate(R.menu.gallery_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.gallery_notification:
                Intent i = new Intent(getActivity(), NotificationActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }

        return false;
    }
}
