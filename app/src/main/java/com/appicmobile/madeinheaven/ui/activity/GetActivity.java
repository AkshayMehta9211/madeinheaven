package com.appicmobile.madeinheaven.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;

public class GetActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private Button btnGatStarted;
    String message;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        message=getIntent().getStringExtra(Appconstant.FLAG);
     /*   Bundle bundle = getIntent().getExtras();
        message = bundle.getString("flag");*/
       // setToolBar();
        initView();
        setOnClickListner();
        
//        int fragmentindex = getIntent().getIntExtra(ApplicationData.FRAGMENT_INDEX, 0);
//        if (fragmentindex == 0) {
//            displayView(0, btnLogin);
//        } else {
//            String activeTAB = getIntent().getStringExtra(ApplicationData.ACTIVE_TAB);
//            if (!TextUtils.isEmpty(activeTAB)) {
//                if (activeTAB.equals(ApplicationData.COMPLETED)) {
//                    displayView(fragmentindex, mCompleted);
//                } else {
//                    displayView(fragmentindex, btnGatStarted);
//                }
//
//            }
//
//        }
        displayView(1, btnGatStarted);
    }
    

//    private void setToolBar() {
//        setSupportActionBar((Toolbar) findViewById(R.id.catalog_toolbar));
//        if (getSupportActionBar() != null) {
//            setTitle(getString(R.string.app_name));
//            getSupportActionBar().setDisplayShowTitleEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        }
//    }

    private void setOnClickListner() {
        btnLogin.setOnClickListener(this);
        btnGatStarted.setOnClickListener(this);
        
    }

    private void initView() {
        btnLogin = (Button) findViewById(R.id.btnActive);
        btnGatStarted = (Button) findViewById(R.id.btnAccepted);
    }

    private void setActiveTab(Button activeTab, Button inactiveTab1) {
        activeTab.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.tab_white));
        inactiveTab1.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.tab_button));
        
    }

    private void displayView(int index, Button button) {
        Fragment fragment = null;
        switch (index) {
            case 0:
                fragment = Loginframe.newInstance();
                setActiveTab(button, btnGatStarted);
                FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                ft1.replace(R.id.main_container, fragment, fragment.getClass().getSimpleName()).commit();
                break;
            case 1:
              //  fragment =Joinwdding.newInstance();
                fragment = new WeddingCodeActivity();
                setActiveTab(button, btnLogin);

                if(message.equals("1")){
                    message = "0";
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
                    ft.replace(R.id.main_container, fragment, fragment.getClass().getSimpleName()).commit();
                }
                else{
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_container, fragment, fragment.getClass().getSimpleName()).commit();
                }
                break;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_container, fragment, fragment.getClass().getSimpleName()).commit();
    }
    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_alert) {
//            Intent i=new Intent(MainActivity.this,EditProfileActivity.class);
//            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnActive:
                displayView(0, btnLogin);
                break;
            case R.id.btnAccepted:
                displayView(1, btnGatStarted);
                break;
            
        }
    }


}