package com.appicmobile.madeinheaven.ui.common;


import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;

import com.appicmobile.madeinheaven.R;


/**
 * Created by Ankit on 9/7/2015.
 */
public class CustomDrawable {
    private int radius;
    private int colorId;
    private int shape;
    private Context context;

    /**
     * adds default radius,color,shape
     */
    public CustomDrawable(Context context) {
        this.context = context;
        //set default value at time of creation
        radius = Math.round(context.getResources().getDimension(R.dimen.default_radius));
        /*R.dimen.default_radius*/
        colorId = R.color.white;
        shape = GradientDrawable.RECTANGLE;
    }

    public CustomDrawable setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    public CustomDrawable setColor(int colorId) {
        this.colorId = colorId;
        return this;
    }

    /**
     * @param shape GradientDrawable
     * @return
     */
    public CustomDrawable setShape(int shape) {
        this.shape = shape;
        return this;
    }

    public GradientDrawable build() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(radius);
        gradientDrawable.setColor(ContextCompat.getColor(context, colorId));
        gradientDrawable.setShape(shape);
        return gradientDrawable;
    }

}
