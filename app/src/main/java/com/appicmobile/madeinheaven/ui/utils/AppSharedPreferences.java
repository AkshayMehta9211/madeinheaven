package com.appicmobile.madeinheaven.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by akshay on 28/6/16.
 */
public class AppSharedPreferences {

    private static final String TAG = AppSharedPreferences.class.getSimpleName();
    private static android.content.SharedPreferences mSharedPreferences;

    public AppSharedPreferences(Context context) {
        mSharedPreferences = context.getSharedPreferences("MyPreference", Context.MODE_PRIVATE);
    }

    public void setWeddingList(String weddinglist) {
        mSharedPreferences.edit().putString("weddinglist",weddinglist).commit();
    }


    public JSONArray getWeddingList(){
        JSONArray jsonArray = null;
        String weddlist = mSharedPreferences.getString("weddinglist","");
        if(weddlist!=null && !weddlist.isEmpty()){
            try {
                jsonArray = new JSONArray(weddlist);
            } catch (JSONException e) {
                e.printStackTrace();
                new JSONArray();
            }
        }
        return jsonArray;
    }
}
