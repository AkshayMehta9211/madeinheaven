package com.appicmobile.madeinheaven.ui.network;

import com.android.volley.VolleyError;
import com.google.gson.JsonObject;

import org.json.JSONObject;

/**
 * Created by Ankit on 9/11/2015.
 */
public interface ResponseListener {
    void onError(VolleyError error);
	void onSuccess(String string);
	void onSuccess_one(JSONObject response);
}
