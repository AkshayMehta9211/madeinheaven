package com.appicmobile.madeinheaven.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;

public class HaveAccountActivity extends Fragment {

	Button btnLoginHere, btnCreateAccount;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootview = inflater.inflate(R.layout.haveaccount_layout, container, false);

		final String code = getArguments().getString(Appconstant.FLAG);
	//	Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();

		btnLoginHere = (Button) rootview.findViewById(R.id.btnLoginHere);
		btnCreateAccount = (Button) rootview.findViewById(R.id.btnCreateAccount);

		btnLoginHere.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				Intent i=new Intent(getActivity(),LoginActivity.class);
				i.putExtra(Appconstant.FLAG, code);
				startActivity(i);

				/*Fragment login = new Login_Account();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

				Bundle args = new Bundle();
				args.putString("HaveCode", code);
				login.setArguments(args);
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.main_container, login);
				// fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();*/
			}
		});

		btnCreateAccount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment newAccount = new CreateNewAccount();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

				Bundle args = new Bundle();
				args.putString(Appconstant.FLAG, code);
				newAccount.setArguments(args);
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.main_container, newAccount);
				// fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});

		return rootview;
	}

}
