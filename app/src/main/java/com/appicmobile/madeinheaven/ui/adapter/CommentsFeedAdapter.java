package com.appicmobile.madeinheaven.ui.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;

import java.util.List;

/**
 * Created by Ashvini on 07-Jan-16.
 */
public class CommentsFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ListAdapter {

        private Context context;
        private int itemsCount = 0;
        private int lastAnimatedPosition = -1;
        private int avatarSize;

        private boolean animationsLocked = false;
        private boolean delayEnterAnimation = true;
        List<SERVICEDATA.POST> questions;

        public CommentsFeedAdapter(Context context, List<SERVICEDATA.POST> questions) {
            this.context = context;
            this.questions = questions;
        }



        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);


            return new CommentViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            runEnterAnimation(viewHolder.itemView, position);
            CommentViewHolder holder = (CommentViewHolder) viewHolder;

            SERVICEDATA.POST value = questions.get(position);
            holder.tvComment.setText(value.getComments_data());
            holder.tvCommentratorName.setText(value.getCommentatorName());

        }

        private void runEnterAnimation(View view, int position) {
            if (animationsLocked) return;

            if (position > lastAnimatedPosition) {
                lastAnimatedPosition = position;
                view.setTranslationY(100);
                view.setAlpha(0.f);
                view.animate()
                        .translationY(0).alpha(1.f)
                        .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                        .setInterpolator(new DecelerateInterpolator(2.f))
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                animationsLocked = true;
                            }
                        })
                        .start();
            }
        }

        @Override
        public int getItemCount() {
            return questions.size();
        }

        public void updateItems() {
            itemsCount = 10;
            notifyDataSetChanged();
        }

        public void addItem() {
            itemsCount++;
            notifyItemInserted(itemsCount - 1);
        }

        public void setAnimationsLocked(boolean animationsLocked) {
            this.animationsLocked = animationsLocked;
        }

        public void setDelayEnterAnimation(boolean delayEnterAnimation) {
            this.delayEnterAnimation = delayEnterAnimation;
        }

    /**
     * Indicates whether all the items in this adapter are enabled. If the
     * value returned by this method changes over time, there is no guarantee
     * it will take effect.  If true, it means all items are selectable and
     * clickable (there is no separator.)
     *
     * @return True if all items are enabled, false otherwise.
     * @see #isEnabled(int)
     */
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    /**
     * Returns true if the item at the specified position is not a separator.
     * (A separator is a non-selectable, non-clickable item).
     * <p/>
     * The result is unspecified if position is invalid. An {@link ArrayIndexOutOfBoundsException}
     * should be thrown in that case for fast failure.
     *
     * @param position Index of the item
     * @return True if the item is not a separator
     * @see #areAllItemsEnabled()
     */
    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    /**
     * Register an observer that is called when changes happen to the data used by this adapter.
     *
     * @param observer the object that gets notified when the data set changes.
     */
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    /**
     * Unregister an observer that has previously been registered with this
     * adapter via {@link #registerDataSetObserver}.
     *
     * @param observer the object to unregister.
     */
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return 0;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    /**
     * <p>
     * Returns the number of types of Views that will be created by
     * {@link #getView}. Each type represents a set of views that can be
     * converted in {@link #getView}. If the adapter always returns the same
     * type of View for all items, this method should return 1.
     * </p>
     * <p>
     * This method will only be called when when the adapter is set on the
     * the { AdapterView}.
     * </p>
     *
     * @return The number of types of Views that will be created by this adapter
     */
    @Override
    public int getViewTypeCount() {
        return 0;
    }

    /**
     * @return true if this adapter doesn't contain any data.  This is used to determine
     * whether the empty view should be displayed.  A typical implementation will return
     * getCount() == 0 but since getCount() includes the headers and footers, specialized
     * adapters might want a different behavior.
     */
    @Override
    public boolean isEmpty() {
        return false;
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {
            protected TextView tvComment;
            protected TextView tvCommentratorName;

            public CommentViewHolder(View v) {
                super(v);
                tvCommentratorName = (TextView)v.findViewById(R.id.tvCommentratorName);
                tvComment = (TextView)v.findViewById(R.id.tvComment);
            }
        }

    }

