package com.appicmobile.madeinheaven.ui.pojo;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ashwini on 2/8/2016.
 */
public class ChatMessageResponse {

    @SerializedName("message")
    @Expose
    private ArrayList<Message> message = new ArrayList<Message>();

    /**
     *
     * @return
     * The message
     */
    public ArrayList<Message> getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }

}
