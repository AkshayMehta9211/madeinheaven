package com.appicmobile.madeinheaven.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.SERVICEDATA;

import java.util.List;

public class Comment_cell_Adapter extends BaseAdapter {

	private Context context;
	List<SERVICEDATA.POST> data;

	public Comment_cell_Adapter(Context context, List<SERVICEDATA.POST> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		
		if(data !=null && data.size()>0)
			return data.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {

		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;

		
		SERVICEDATA.POST value = data.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.comment_layout, null);
			// well set up the ViewHolder
			viewHolder = new ViewHolderItem();
			viewHolder.commentratorName = (TextView) convertView.findViewById(R.id.commentratorName);
//			viewHolder.commentData = (TextView) convertView.findViewById(R.id.commentData);
			
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolderItem) convertView.getTag();
		}
		
		viewHolder.commentratorName.setText(value.getCommentatorName() +":"+value.getComments_data()) ;
//		viewHolder.commentData.setText(value.getComments_data());
		
		return convertView;
	}
	
	public class ViewHolderItem {
		TextView commentratorName;
//		TextView commentData;
	}
}
