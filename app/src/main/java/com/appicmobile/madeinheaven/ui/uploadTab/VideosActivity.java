package com.appicmobile.madeinheaven.ui.uploadTab;

import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.appicmobile.madeinheaven.R;
import com.appicmobile.madeinheaven.ui.common.Appconstant;
import com.appicmobile.madeinheaven.ui.common.MadeInHeavenServiceURL;
import com.appicmobile.madeinheaven.ui.common.UPLOADVIDEO;
import com.appicmobile.madeinheaven.ui.drawer.NavDrawer;
import com.appicmobile.madeinheaven.ui.network.RequestGenerator;
import com.appicmobile.madeinheaven.ui.network.ResponseListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

public class VideosActivity extends Fragment {
    private Uri fileUri;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    public static FragmentActivity ActivityContext = null;
    public static TextView output;
    Intent data = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.blank, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.activity_video_frag, container, false);
        output = (TextView) rootview.findViewById(R.id.output);
        ActivityContext = getActivity();

        ((NavDrawer) getActivity()).setActionBarTitle("VIDEO");

		Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
        // set video quality
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 14);
		
		// name
		getParentFragment().startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

		ImageView buttonRecording = (ImageView) rootview.findViewById(R.id.recording);


        return rootview;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // getActivity();
        // After camera screen this code will excuted

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {

            if (resultCode == ActivityContext.RESULT_OK) {

                Uri selectedImage = data.getData();

                try {
                    byte[] v = readBytes(selectedImage);

                    String encodedImage = Base64.encodeToString(v, Base64.DEFAULT);

                    UPLOADVIDEO uploadVideo = new UPLOADVIDEO();
                    uploadVideo.setUserId(Appconstant.USER_ID);
                    uploadVideo.setWeddingCode(String.valueOf(Appconstant.WEDDING_CODE));
                    uploadVideo.setFileextension(".MPEG-4");
                    uploadVideo.setPath(encodedImage);

                    Toast.makeText(getActivity(), "Picture Saved", Toast.LENGTH_SHORT).show();

                    final Gson gson = new Gson();
                    final String jsonString = gson.toJson(uploadVideo);
                    Log.i("uploadimage", jsonString);

                    RequestGenerator.makePostRequest(getActivity(), MadeInHeavenServiceURL.URL_POST_VIDEO, jsonString, true,
                            new ResponseListener() {
                                @Override
                                public void onSuccess(String response) {
                                    try {
                                        UPLOADIMAGE prescription1 = gson.fromJson(response, UPLOADIMAGE.class);

                                        if (prescription1.getStatus() == true) {

                                            Toast.makeText(getActivity(), "uploaded successfull", Toast.LENGTH_SHORT).show();

                                        } else {
                                            Toast.makeText(getActivity(), "uploaded not successfull", Toast.LENGTH_SHORT).show();
                                            // onBackPressed();
                                        }

                                    } catch (Exception e) {
                                        System.out.println("e---->>" + e.toString());
                                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(VolleyError error) {
                                    // TODO Auto-generated method stub
                                    Toast.makeText(getActivity(), "toast_please_try_again", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onSuccess_one(JSONObject response) {
                                    // TODO Auto-generated method stub
                                    Toast.makeText(getActivity(), "JSONObject response_try_again", Toast.LENGTH_SHORT).show();
                                }
                            });


                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                String filepath = data.toString();
                File imagefile = new File(filepath);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(imagefile);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Bitmap bm = BitmapFactory.decodeStream(fis);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                Toast.makeText(getActivity(), "Video saved to:" + data.getData(), Toast.LENGTH_LONG).show();

            } else if (resultCode == ActivityContext.RESULT_CANCELED) {

                output.setText("User cancelled the video capture.");
                getActivity().finish();

                // User cancelled the video capture
                Toast.makeText(ActivityContext, "User cancelled the video capture.", Toast.LENGTH_LONG).show();

            } else {

                output.setText("Video capture failed.");

                // Video capture failed, advise user
                Toast.makeText(ActivityContext, "Video capture failed.", Toast.LENGTH_LONG).show();
            }
        }
        //getActivity().finish();
    }

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraVideo");
        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                output.setText("Failed to create directory MyCameraVideo.");

                Toast.makeText(ActivityContext, "Failed to create directory MyCameraVideo.", Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }

        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());

        File mediaFile;

        if (type == MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file
            // name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
            String fileMedia = mediaFile.toString();


            try {


                File sdCard = Environment.getExternalStorageDirectory();
                File file = new File(fileMedia);
                mediaStorageDir.mkdirs();
                file.createNewFile();

                if (!mediaFile.exists()) {
                    mediaFile.mkdirs();
                    System.out.println("Making dirs");
                }
                File myFile = new File(mediaFile.getAbsolutePath());
                myFile.createNewFile();
                FileOutputStream out = new FileOutputStream(myFile);
                FileInputStream objFileIS = null;

                try {
                    System.out.println("file = >>>> <<<<<" + mediaFile);
                    objFileIS = new FileInputStream(mediaFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
                byte[] byteBufferString = new byte[1024];
                try {
                    for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1; ) {
                        objByteArrayOS.write(byteBufferString, 0, readNum);
                        System.out.println("read " + readNum + " bytes,");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String videodata = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);

                Uri myUri = Uri.fromFile(myFile);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                FileInputStream fis = new FileInputStream(new File(fileMedia));

                byte[] buf = new byte[1024];
                int n;
                while (-1 != (n = fis.read(buf)))
                    baos.write(buf, 0, n);

                byte[] videoBytes = baos.toByteArray();


//			        myBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        } else {
            return null;
        }

        return mediaFile;
    }

    public byte[] readBytes(Uri uri) throws IOException {
        // this dynamically extends to take the bytes you read
        InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        byte[] b = byteBuffer.toByteArray();
        // Step 2 : converting byte[] to Base64 encoded String

        // we need to know how may bytes were read to write them to the
        // byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        Log.i("byteArray", byteBuffer.toByteArray().toString());


        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public static Fragment newInstance() {
        VideosActivity activeFragment = new VideosActivity();
        Bundle bundle = new Bundle();
        activeFragment.setArguments(bundle);
        return activeFragment;
    }

    OnBackStackChangedListener listener = new OnBackStackChangedListener() {

        @Override
        public void onBackStackChanged() {
            getActivity().finish();
        }
    };

}
