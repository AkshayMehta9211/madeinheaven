package com.appicmobile.madeinheaven.ui.utils;

import android.content.Context;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by akshay on 1/3/16.
 */
public class DateUtil {

    private static final String ABR_SECONDS_AGO = "s";
    private static final String ABR_MINUTES_AGO = "m";
    private static final String ABR_HOURS_AGO = "h";
    private static final String ABR_DAYS_AGO = "d";
    public static final String SIMPLE_DATE_FORMAT = "dd-MM-yyyy";
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Returns a string describing 'time' as a time relative to 'now'.
     * <p/>
     * Time spans in the past are formatted like "42 minutes ago". Time spans in
     * the future are formatted like "in 42 minutes".
     * <p/>
     * Can use FORMAT_ABBREV_RELATIVE flag to use abbreviated relative
     * times, like "42 mins ago".
     *
     * @param time          the time to describe, in milliseconds
     * @param now           the current time in milliseconds
     * @param minResolution the minimum timespan to report. For example, a time
     *                      3 seconds in the past will be reported as "0 minutes ago" if
     *                      this is set to MINUTE_IN_MILLIS. Pass one of 0,
     *                      MINUTE_IN_MILLIS, HOUR_IN_MILLIS, DAY_IN_MILLIS,
     *                      WEEK_IN_MILLIS
     * @param flags         a bit mask of formatting options, such as
     *                      FORMAT_NUMERIC_DATE or
     *                      FORMAT_ABBREV_RELATIVE
     */
    public static CharSequence getPastRelativeTimeSpanString(long time, long now, long minResolution, int flags) {
        long duration = Math.abs(now - time);
        String timeStr;
        long count;
        if (duration < DateUtils.MINUTE_IN_MILLIS && minResolution < DateUtils.MINUTE_IN_MILLIS) {
            count = duration / DateUtils.SECOND_IN_MILLIS;
            timeStr = ABR_SECONDS_AGO;
        } else if (duration < DateUtils.HOUR_IN_MILLIS && minResolution < DateUtils.HOUR_IN_MILLIS) {
            count = duration / DateUtils.MINUTE_IN_MILLIS;
            timeStr = ABR_MINUTES_AGO;
        } else if (duration < DateUtils.DAY_IN_MILLIS && minResolution < DateUtils.DAY_IN_MILLIS) {
            count = duration / DateUtils.HOUR_IN_MILLIS;
            timeStr = ABR_HOURS_AGO;
        } else if (duration < DateUtils.WEEK_IN_MILLIS && minResolution < DateUtils.WEEK_IN_MILLIS) {
            return getRelativeDayString(time, now);
        } else {
            // We know that we won't be showing the time, so it is safe to pass in a null context.
            return DateUtils.formatDateRange(null, time, time, flags);
        }
        return count + timeStr;
    }

    /**
     * Returns a string describing a day relative to the current day. For example if the day is
     * today this function returns "Today", if the day was a week ago it returns "7 days ago", and
     * if the day is in 2 weeks it returns "in 14 days".
     *
     * @param day   the relative day to describe in UTC milliseconds
     * @param today the current time in UTC milliseconds
     */
    private static final String getRelativeDayString(long day, long today) {
        Time startTime = new Time();
        startTime.set(day);
        int startDay = Time.getJulianDay(day, startTime.gmtoff);
        Time currentTime = new Time();
        currentTime.set(today);
        return Math.abs(Time.getJulianDay(today, currentTime.gmtoff) - startDay) + ABR_DAYS_AGO;
    }


    public static long getDateInMillis(String srcDate) {
        String dates = srcDate.substring(0, 19);
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                DATE_FORMAT, Locale.getDefault());
        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(dates);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (Exception e) {
            Log.d("Exception", "" + e.getMessage());
            e.printStackTrace();
        }

        return 0;
    }



    public static String dateDiff(Date date) {
        long oldMillis = date.getTime();
        long currMillis = System.currentTimeMillis();
        String msg = null;
        long diff = java.lang.Math.abs(currMillis - oldMillis);
        long temp = diff / 60000;
        if (temp >= 60) {
            temp = temp / 60;
            if (temp >= 24) {
                temp = temp / 24;
                if (temp > 30) {
                    temp = temp / 30;
                    if (temp > 12) {
                        temp = temp / 12;

                        if (temp == 1) {
                            msg = temp + "y";
                        } else {
                            msg = temp + "y";
                        }
                    } else {

                        if (temp == 1) {
                            msg = temp + "m";
                        } else {
                            msg = temp + "m";
                        }
                    }
                } else {

                    if (temp == 1) {
                        msg = temp + "d";
                    } else {
                        msg = temp + "d";
                    }
                }
            } else {

                if (temp == 1) {
                    msg = temp + "h";
                } else {
                    msg = temp + "h";
                }
            }
        } else {

            if (temp == 1) {
                msg = temp + "m";
            } else {
                msg = temp + "m";
            }
        }
        return msg;
    }

    public static Date getDate(String date) {

        String trimdate = date.substring(0, 19);
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date parsed = null;
        try {
            parsed = inputFormat.parse(trimdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        outputFormat.format(parsed);
        return parsed;
    }

}
